//
//  ListingImage.swift
//  Shoote
//
//  Created by Muhammad Ansaf on 29/10/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import Foundation
import SwiftyJSON

class ListingImage {
	
	var id : Int = kInt0
	var carListingId : Int = kInt0
	var carListing : CarListing = CarListing()
	var imagePath : String = kBlankString
	var createdAt : String = kBlankString
	
	init(fromJson json: JSON) {
		createdAt = json["CreatedAt"].string ?? kBlankString
		id = json["Id"].int ?? kInt0
		imagePath = json["ImagePath"].string ?? kBlankString
		carListing = CarListing(fromJson: json["CarListing"])
		carListingId = json["CarListingId"].int ?? kInt0
	}
	
}
