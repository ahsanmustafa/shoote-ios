//
//  CarBooking.swift
//  Shoote
//
//  Created by BrainX IOS 3 on 03/09/2021.
//  Copyright © 2021 Digi Dev. All rights reserved.
//

import Foundation
import SwiftyJSON

class CarBooking {
	
	var id: Int = kInt0
	var ownerId: Int = kInt0
	var bookingUserId: Int = kInt0
	var carListingId: Int = kInt0
	var carListing: CarListing = CarListing()
	var startDate: String = kBlankString
	var endDate: String = kBlankString
	var startDateUserTimeZome: String = kBlankString
	var endDateUserTimeZone: String = kBlankString
	var email: String = kBlankString
	var phoneNo: String = kBlankString
	var typeOfBooking: String = kBlankString
	var userDrive: Int = kInt0
	var noOfPassengers: Int = kInt0
	var description: String = kBlankString
	var locationType: String = kBlankString
	var location: PhysicalAddres = PhysicalAddres()
	var isApproved: Bool = false
	var bookingStatus: String = kBlankString
	var createdAt: String = kBlankString
	var additionalFees: [AdditionalFee] = []
	var reviews: [Review] = []
	var hourlyTotal: Double = kDouble0
	var processingFee: Double = kDouble0
	var totalFee: Double = kDouble0
	var rideDateTime: String = kBlankString
	var currentTimeSpan: CurrentTimeSpan = CurrentTimeSpan()
	
	init() {}
	
	init(_ json: JSON) {
		id = json["Id"].int ?? kInt0
		ownerId = json["BusinessProfileId"].int ?? kInt0
		bookingUserId = json["ShooteUserId"].int ?? kInt0
		carListingId = json["CarListingId"].int ?? kInt0
		carListing = CarListing(fromJson: json["CarListing"])
		startDate = json["StartDate"].string ?? kBlankString
		endDate = json["EndDate"].string ?? kBlankString
		email = json["Email"].string ?? kBlankString
		phoneNo = json["PhoneNo"].string ?? kBlankString
		typeOfBooking = json["TypeOfBooking"].string ?? kBlankString
		userDrive = json["UserDrive"].int ?? kInt0
		noOfPassengers = json["NoOfPassengers"].int ?? kInt0
		description = json["Description"].string ?? kBlankString
		locationType = json["LocationType"].string ?? kBlankString
		location = PhysicalAddres(json["Location"])
		isApproved = json["IsApproved"].bool ?? false
		bookingStatus = json["BookingStatus"].string ?? kBlankString
		createdAt = json["CreatedAt"].string ?? kBlankString
		if let feesArray = json["Reviews"].array {
			for feeJson in feesArray{
				let value = AdditionalFee(fromJson: feeJson)
				additionalFees.append(value)
			}
		}
		
		if let reviewsArray = json["Reviews"].array {
			for reviewsJson in reviewsArray{
				let value = Review(fromJson: reviewsJson)
				reviews.append(value)
			}
		}
		
		hourlyTotal = json["HourlyTotal"].double ?? kDouble0
		processingFee = json["ProcessingFee"].double ?? kDouble0
		totalFee = json["TotalFee"].double ?? kDouble0
		rideDateTime = json["RideDateTime"].string ?? kBlankString
		currentTimeSpan = CurrentTimeSpan(json["CurrentTimeSpan"])
		
		startDateUserTimeZome = Utilities.utcToLocal(withoutSSS: startDate)
		endDateUserTimeZone = Utilities.utcToLocal(withoutSSS: endDate)
	}
}
