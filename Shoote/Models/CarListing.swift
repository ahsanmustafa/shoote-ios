//
//  CarListing.swift
//  Shoote
//
//  Created by Muhammad Ansaf on 29/10/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import Foundation
import SwiftyJSON

class CarListing {
	
	var id: Int = kInt0
	var businessProfileId: Int = kInt0
	var businessProfile: BusinessProfile = BusinessProfile()
	var name : String = kBlankString
	var maxCapacity : String = kBlankString
	var maxHours : Float = kFloat0
	var description : String = kBlankString
	var condition : String = kBlankString
	var typeOfCar : String = kBlankString
	var bodyType : String = kBlankString
	var year : Int = kInt0
	var make : String = kBlankString
	var model : String = kBlankString
	var transmission : String = kBlankString
	var location: PhysicalAddres = PhysicalAddres()
	var mondayTiming : String = kBlankString
	var tuesdayTiming : String = kBlankString
	var wednesdayTiming : String = kBlankString
	var thursdayTiming : String = kBlankString
	var fridayTiming : String = kBlankString
	var saturdayTiming : String = kBlankString
	var sundayTiming : String = kBlankString
	var policy : String = kBlankString
	var ownerPresence : Int = kInt0 // 0 = No, 1 = Yes, 2 = Co Host
	var userDrive : Int = kInt0
	var carHostActivity : String = kBlankString
	var cancellation : Int = kInt0 // 0 = Flexible, 1 = Strict, 2 = Very Strict
	var hourlyRate : Float = kFloat0
	var minHours : Float = kFloat0
	var discountTime : Int = kInt0
	var discountPercentage : Int = kInt0
	var additionalFees = [AdditionalFee]()
	var listingImages = [ListingImage]()
	var reviews = [Review]()
	var isAvailable : Bool = false
	var isActive : Bool = false
	var isApproved : Bool = false
	var exteriorColor : String = kBlankString
	var interiorColor: String = kBlankString
	var inActiveDate : String = kBlankString
	var isManual : Bool = false
	var createdAt : String = kBlankString
	var isDeleted : Bool = false
	var maxDeliveryRadius: Int = kInt0
	var averageRating: Float = kFloat0
	
	init() {}
	
	init(fromJson json: JSON) {
		id = json["Id"].int ?? kInt0
		businessProfileId = json["BusinessProfileId"].int ?? kInt0
		businessProfile = BusinessProfile(fromJson: json["BusinessProfile"])
		location = PhysicalAddres(json["Location"])
		additionalFees = [AdditionalFee]()
		if let additionalFeesArray = json["AdditionalFees"].array {
			for feeJson in additionalFeesArray {
				let fee = AdditionalFee(fromJson: feeJson)
				additionalFees.append(fee)
			}
		}
		bodyType = json["BodyType"].string ?? kBlankString
		cancellation = json["Cancellation"].int ?? kInt0
		carHostActivity = json["CarHostActivity"].string ?? kBlankString
		exteriorColor = json["ExteriorColor"].string ?? kBlankString
		interiorColor = json["InteriorColor"].string ?? kBlankString
		
		condition = json["Condition"].string ?? kBlankString
		createdAt = json["CreatedAt"].string ?? kBlankString
		description = json["Description"].string ?? kBlankString
		discountPercentage = json["DiscountPercentage"].int ?? kInt0
		discountTime = json["DiscountTime"].int ?? kInt0
		fridayTiming = json["FridayTiming"].string ?? "Not Available"
		hourlyRate = json["HourlyRate"].float ?? kFloat0
		isActive = json["IsActive"].bool ?? false
		isApproved = json["IsApproved"].bool ?? false
		isAvailable = json["IsAvailable"].bool ?? false
		
		var listingImages = [ListingImage]()
		if let listingImagessArray = json["ListingImages"].array {
			for imageJson in listingImagessArray {
				let fee = ListingImage(fromJson: imageJson)
				listingImages.append(fee)
			}
		}
		self.listingImages = listingImages
		
		make = json["Make"].string ?? kBlankString
		maxCapacity = json["MaxCapacity"].string ?? kBlankString
		maxHours = json["MaxHours"].float ?? kFloat0
		minHours = json["MinHours"].float ?? kFloat0
		model = json["Model"].string ?? kBlankString
		mondayTiming = json["MondayTiming"].string ?? "Not Available"
		name = json["Name"].string ?? kBlankString
		ownerPresence = json["OwnerPresence"].int ?? kInt0
		policy = json["AdditionalRules"].string ?? kBlankString
		saturdayTiming = json["SaturdayTiming"].string ?? "Not Available"
		sundayTiming = json["SundayTiming"].string ?? "Not Available"
		thursdayTiming = json["ThursdayTiming"].string ?? "Not Available"
		transmission = json["Transmission"].string ?? kBlankString
		tuesdayTiming = json["TuesdayTiming"].string ?? "Not Available"
		typeOfCar = json["TypeOfCar"].string ?? kBlankString
		userDrive = json["UserDrive"].int ?? kInt0
		wednesdayTiming = json["WednesdayTiming"].string ?? "Not Available"
		year = json["Year"].int ?? kInt0
		isDeleted = json["IsDeleted"].bool ?? false
		inActiveDate = json["InActiveDate"].string ?? kBlankString
		isManual = json["IsManual"].bool ?? false
		maxDeliveryRadius = json["MaxDeliveryRadius"].int ?? kInt0
		averageRating = json["AverageRating"].float ?? kFloat0
		if let reviewsListArray = json["Reviews"].array {
			for reviewJson in reviewsListArray {
				let fee = Review(fromJson: reviewJson)
				reviews.append(fee)
			}
		}
	}
	
}
