//
//  UserProfile.swift
//  Shoote
//
//  Created by Zahid Usman Cheema on 18/11/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import Foundation
import SwiftyJSON

class BusinessProfile {
	
	var id : Int = kInt0
	var title: String = kBlankString
	var postalAddress: PhysicalAddres = PhysicalAddres()
	var physicalAddress: PhysicalAddres = PhysicalAddres()
	var email : String = kBlankString
	var description: String = kBlankString
	var primaryContact: String = kBlankString
	var secondaryContact: String = kBlankString
	var profileImage : String = kBlankString
	var shooteUsers: [ShooteUser] = [ShooteUser]()
	var reviews = [Review]()
	var carListings = [CarListing]()
	var isActive : Bool = false
	var isVerified: Bool = false
	var providedService : Bool = false
	var multipleListings : Bool = false
	var tenCompletedRentals : Bool = false
	var createdAt : String = kBlankString
	var ratingAvg: Float = kFloat0
	
	
	init(){}
	
	init(fromJson json: JSON){
		id = json["Id"].int ?? kInt0
		title = json["Title"].string ?? kBlankString
		postalAddress = PhysicalAddres(json["PostalAddress"])
		physicalAddress = PhysicalAddres(json["PhysicalAddress"])
		email = json["Email"].string ?? kBlankString
		description = json["Description"].string ?? kBlankString
		primaryContact = json["PrimaryContact"].string ?? kBlankString
		secondaryContact = json["SecondaryContact"].string ?? kBlankString
		profileImage = json["ProfileImage"].string ?? kBlankString
		
		if let usersArray = json["ShooteUsers"].array {
			for userJson in usersArray{
				let value = ShooteUser(fromJson: userJson)
				shooteUsers.append(value)
			}
		}
		
		if let reviewsArray = json["Reviews"].array {
			for reviewsJson in reviewsArray{
				let value = Review(fromJson: reviewsJson)
				reviews.append(value)
			}
		}
		
		if let shooteServiceListsArray = json["CarListings"].array {
			for shooteServiceListsJson in shooteServiceListsArray{
				let value = CarListing(fromJson: shooteServiceListsJson)
				carListings.append(value)
			}
		}
		
		isActive = json["IsActive"].bool ?? false
		isVerified = json["isVerified"].bool ?? false
		providedService = json["ProvidedService"].bool ?? false
		multipleListings = json["MultipleListings"].bool ?? false
		tenCompletedRentals = json["TenCompletedRentals"].bool ?? false
		createdAt = json["CreatedAt"].string ?? kBlankString
		ratingAvg = json["RatingAvg"].float ?? kFloat0
	}
	
}
