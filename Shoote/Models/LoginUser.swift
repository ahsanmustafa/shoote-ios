//
//  LoginUser.swift
//  GirlsChase
//
//  Created by Rapidzz Macbook on 14/06/2019.
//  Copyright © 2019 Rapidzz. All rights reserved.
//

import Foundation
import SwiftyJSON

class LoginUser:NSObject, NSCoding {
	
    var accessToken: String
    var tokenType: String
	var refreshToken: String
	var expiresIn: String
	var uid: Int
	var businessProfileId: Int
	var name: String
	var email: String
	var userRole: String
	var phoneNo: String
	var issued: String
    var expires: String
    var payoutsEnabled: Bool
    var paymentsEnabled: Bool
	
    override init() {
        
        self.accessToken = kBlankString
        self.tokenType = kBlankString
        self.expires = kBlankString
        self.name = kBlankString
        self.expiresIn = kBlankString
        self.issued = kBlankString
        self.refreshToken = kBlankString
        self.email = kBlankString
		self.phoneNo = kBlankString
		self.uid = kInt0
		self.businessProfileId = kInt0
		self.userRole = kBlankString
        self.payoutsEnabled = false
        self.paymentsEnabled = false
    }
    
    convenience init(_ json:JSON){
        self.init()
		self.uid = Int(json["UserId"].string ?? kBlankString) ?? kInt0
		self.businessProfileId = Int(json["BusinessProfileId"].string ?? kBlankString) ?? kInt0
        self.accessToken = json["access_token"].string ?? ""
        self.tokenType = json["token_type"].string ?? ""
        self.expires = json[".expires"].string ?? ""
        self.name = json["Name"].string ?? ""
        self.expiresIn = json["expires_in"].string ?? ""
        self.issued = json[".issued"].string ?? ""
        self.refreshToken = json["refresh_token"].string ?? ""
        self.email = json["Email"].string ?? ""
		self.phoneNo = json["PhoneNo"].string ?? ""
		self.userRole = json["UserRole"].string ?? ""
        self.payoutsEnabled = json["PayoutsEnabled"].boolValue
        self.paymentsEnabled = json["PaymentsEnabled"].boolValue
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        self.init()
        self.accessToken = aDecoder.decodeObject(forKey: "access_token") as? String ?? kBlankString
        self.tokenType = aDecoder.decodeObject(forKey: "token_type") as? String ?? kBlankString
        self.expires = aDecoder.decodeObject(forKey: ".expires") as? String ?? kBlankString
        self.name = aDecoder.decodeObject(forKey: "Name") as? String ?? kBlankString
        self.expiresIn = aDecoder.decodeObject(forKey: "expires_in") as? String ?? kBlankString
        self.issued = aDecoder.decodeObject(forKey: ".issued") as? String ?? kBlankString
        self.refreshToken = aDecoder.decodeObject(forKey: "refresh_token") as? String ?? kBlankString
        self.email = aDecoder.decodeObject(forKey: "Email") as? String ?? kBlankString
		self.phoneNo = aDecoder.decodeObject(forKey: "PhoneNo") as? String ?? kBlankString
		self.uid = aDecoder.decodeInteger(forKey: "UserId")
		self.businessProfileId = aDecoder.decodeInteger(forKey: "BusinessProfileId")
		self.userRole = aDecoder.decodeObject(forKey: "UserRole") as? String ?? kBlankString
        self.payoutsEnabled = aDecoder.decodeBool(forKey: "PayoutsEnabled")
        self.paymentsEnabled = aDecoder.decodeBool(forKey: "PaymentsEnabled")
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.accessToken, forKey: "access_token")
        aCoder.encode(self.tokenType, forKey: "token_type")
        aCoder.encode(self.expires, forKey: ".expires")
        aCoder.encode(self.name, forKey: "Name")
        aCoder.encode(self.expiresIn, forKey: "expires_in")
        aCoder.encode(self.issued, forKey: ".issued")
        aCoder.encode(self.refreshToken, forKey: "refresh_token")
        aCoder.encode(self.email, forKey: "Email")
		aCoder.encode(self.phoneNo, forKey: "PhoneNo")
		aCoder.encode(self.uid, forKey: "UserId")
		aCoder.encode(self.businessProfileId, forKey: "BusinessProfileId")
		aCoder.encode(self.userRole, forKey: "UserRole")
        aCoder.encode(self.payoutsEnabled, forKey: "PayoutsEnabled")
        aCoder.encode(self.paymentsEnabled, forKey: "PaymentsEnabled")
    }
}
