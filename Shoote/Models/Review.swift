//
//  Review.swift
//  Shoote
//
//  Created by BrainX IOS 3 on 20/04/2021.
//  Copyright © 2021 Digi Dev. All rights reserved.
//

import Foundation
import SwiftyJSON

class Review {
	
	var id : Int = kInt0
	var reviewDescription : String = kBlankString
	var listingId : Int = kInt0
	var carListing: CarListing = CarListing()
	var carBookingId: Int = kInt0
	var rating : Float = kFloat0
	var communicationRating : Float = kFloat0
	var cleanlinessRating : Float = kFloat0
	var accuracyRating : Float = kFloat0
	var timingRating : Float = kFloat0
	var recommendRating : Float = kFloat0
	var createdAt : String = kBlankString
	var bookingUserId: Int = kInt0
	var businessProfileId: Int = kInt0
	var reviewerImage : String = kBlankString
	var givenTo: GivenTo = .USER
	
	init() {}
	
	init(fromJson json: JSON) {
		id = json["Id"].int ?? kInt0
		reviewDescription = json["ReviewDescription"].string ?? kBlankString
		listingId = json["ListingId"].int ?? kInt0
		carListing = CarListing(fromJson: json["CarListing"])
		carBookingId = json["CarBookingId"].int ?? kInt0
		rating = json["Rating"].float ?? kFloat0
		communicationRating = json["CommunicationRating"].float ?? kFloat0
		cleanlinessRating = json["CleanlinessRating"].float ?? kFloat0
		accuracyRating = json["AccuracyRating"].float ?? kFloat0
		timingRating = json["TimingRating"].float ?? kFloat0
		recommendRating = json["RecommendRating"].float ?? kFloat0
		createdAt = json["CreatedAt"].string ?? kBlankString
		bookingUserId = json["BookingUserId"].int ?? kInt0
		businessProfileId = json["BusinessProfileId"].int ?? kInt0
		reviewerImage = json["ReviewerImage"].string ?? kBlankString
		givenTo = GivenTo(rawValue: json["GivenTo"].int ?? kInt0) ?? .USER
	}
	
}

enum GivenTo:Int {
	case BUSINESS = 1
	case USER = 2
}
