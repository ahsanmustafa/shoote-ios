//
//  CustomTimeFormatter.swift
//  Shoote
//
//  Created by BrainX IOS 3 on 05/09/2021.
//  Copyright © 2021 Digi Dev. All rights reserved.
//

import Foundation
import MagicTimer

class CustomTimeFormatter: MGTimeFormatter {
	
	//MARK: Properties
	let dateComponent = DateComponentsFormatter()
	
	//MARK: Init
	init() {
		dateComponent.unitsStyle = .abbreviated
		dateComponent.allowedUnits = [.day,.hour,.minute,.second]
	}
	
	//MARK: Delegate
	func converToValidFormat(ti: TimeInterval) -> String? {
		dateComponent.string(from: ti)?.uppercased()
	}
}
