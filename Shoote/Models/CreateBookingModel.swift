//
//  CreateBookingModel.swift
//  Shoote
//
//  Created by BrainX IOS 3 on 16/12/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import Foundation
import SwiftyJSON

class CreateBookingModel {
	var startDate = Date()
	var endDate = Date()
	var startDateString = kBlankString
	var endDateString = kBlankString
	var typeOfBooking = kBlankString
	var numberOfPersons = kInt0
	var willCarDriven = false
	var useOfVehicle = kBlankString
	var location = kBlankString
	var address = kBlankString
	var city = kBlankString
	var state = kBlankString
	var zip = kBlankString
	var phoneNumber = kBlankString
	var email = kBlankString
	var listingId = kInt0
	var hourlyTotal: Float = kFloat0
	var processingFee: Float = kFloat0
	var totalFee: Float = kFloat0
	var additionalFees = [AdditionalFee]()
	var appUserId = kInt0
	var id = kInt0
	var bookingUserId = kInt0
	var carImage = kBlankString
	var carName = kBlankString
    var carLocation = kBlankString
    var currentTimeSpan = CurrentTimeSpan()
	var rideDateTime = kBlankString
    var reviews = [ReviewModel]()
	var bookingStatus = kBlankString
    
	init() {
		
	}
	
	convenience init(_ json:JSON){
		self.init()
		id = json["Id"].int ?? kInt0
		appUserId = json["AppUserId"].int ?? kInt0
		bookingUserId = json["BookingUserId"].int ?? kInt0
		listingId = json["ShooteServiceListId"].int ?? kInt0
		typeOfBooking = json["TypeOfBooking"].string ?? kBlankString
		willCarDriven = json["UserDrive"].bool ?? false
		numberOfPersons = json["NoOfPax"].int ?? kInt0
		useOfVehicle = json["Description"].string ?? kBlankString
		location = json["Location"].string ?? kBlankString
		address = json["Address"].string ?? kBlankString
		city = json["City"].string ?? kBlankString
		state = json["State"].string ?? kBlankString
		
		zip = json["Zip"].string ?? kBlankString
		let startDateStringInUTC = json["StartDate"].string ?? kBlankString
		let endDateStringInUTC = json["EndDate"].string ?? kBlankString
        
        let dateforamtter  = DateFormatter()
        if let timezone = TimeZone(abbreviation: "UTC") {
            dateforamtter.timeZone = timezone
        }
        dateforamtter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        
        let startDateInUTC = dateforamtter.date(from: startDateStringInUTC) ?? Date()
        let endDateInUTC = dateforamtter.date(from: endDateStringInUTC) ?? Date()
        dateforamtter.timeZone = TimeZone.current
        
        startDateString = dateforamtter.string(from: startDateInUTC)
        endDateString = dateforamtter.string(from: endDateInUTC)
		
		hourlyTotal = json["HourlyTotal"].float ?? kFloat0
		processingFee = json["ProcessingFee"].float ?? kFloat0
		totalFee = json["TotalFee"].float ?? kFloat0
		
		email = json["Email"].string ?? kBlankString
		phoneNumber = json["PhoneNo"].string ?? kBlankString
		carImage = json["ShooteServiceList"]["ListingImages"][0]["ImagePath"].stringValue
		carName = json["ShooteServiceList"]["Name"].string ?? kBlankString
        carLocation = json["ShooteServiceList"]["City"].string ?? kBlankString
        currentTimeSpan = CurrentTimeSpan(json["CurrentTimeSpan"])
        rideDateTime = json["RideDateTime"].string ?? kBlankString
        
        if let reviewArray = json["Reviews"].array {
            for review in reviewArray {
                let rvw = ReviewModel(review)
                reviews.append(rvw)
            }
        }
        
        if let jsonArray = json["AdditionalFees"].array {
            for fee in jsonArray{
				let adFee = AdditionalFee(fromJson: fee)
                additionalFees.append(adFee)
            }
        }
		bookingStatus = json["BookingStatus"].string ?? kBlankString
	}
	
}

class ReviewModel {
    var type = ""
    
    init() {
        
    }
    
    convenience init(_ json:JSON){
        self.init()
        type = json["Type"].string ?? kBlankString
    }
}
