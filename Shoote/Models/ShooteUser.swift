//
//  ShooteUser.swift
//  Shoote
//
//  Created by Faheem on 06/09/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import Foundation
import SwiftyJSON

class ShooteUser {
	
	var id: Int = kInt0
	var firstName: String = kBlankString
	var lastName: String = kBlankString
	var email: String = kBlankString
	var password: String = kBlankString
	var dob: String = kBlankString
	var primaryContact: String = kBlankString
    var secondaryContact: String = kBlankString
	var profileImage: String = kBlankString
	var postalAddress: String  = kBlankString
	var physicalAddress: PhysicalAddres = PhysicalAddres()
	var idType: String = kBlankString
	var idDocNum: String = kBlankString
	var idLocation: String = kBlankString
	var idExpiryDate : String = kBlankString
	var idFront: String = kBlankString
	var idBack: String = kBlankString
	var isVerified: Bool = false
	var isActive: Bool = false
	var role: Int  = kInt0
	var forgotPasswordHash: String = kBlankString
	var ratingAvg: Float  = kFloat0
	var createdAt: String = kBlankString
	var businessProfileId: Int = kInt0
	var reviews: [Review] = []
	var bio: String = kBlankString
	var fullName: String = kBlankString
    
    init() {}
	
	init(fromJson json: JSON) {
		
        secondaryContact = json["SecondaryContact"].string ?? kBlankString
        primaryContact = json["PrimaryContact"].string ?? kBlankString
        dob = json["Dob"].string ?? kBlankString
        id = json["Id"].int ?? kInt0
		businessProfileId = json["BusinessProfileId"].int ?? kInt0
		idType = json["IdType"].string ?? kBlankString
		idDocNum = json["IdDocNum"].string ?? kBlankString
		idLocation = json["IdLocation"].string ?? kBlankString
        lastName = json["LastName"].string ?? kBlankString
        idFront = json["IdFront"].string ?? kBlankString
        idExpiryDate = json["IdExpiryDate"].string ?? kBlankString
        firstName = json["FirstName"].string ?? kBlankString
        profileImage = json["ProfileImage"].string ?? kBlankString
        idBack = json["IdBack"].string ?? kBlankString
        forgotPasswordHash = json["ForgotPasswordHash"].string ?? kBlankString
		ratingAvg = json["RatingAvg"].float ?? kFloat0
        password = json["Password"].string ?? kBlankString
        createdAt = json["CreatedAt"].string ?? kBlankString
        email = json["Email"].string ?? kBlankString
		bio = json["Description"].string ?? kBlankString
        isActive = json["isActive"].bool ?? false
		isVerified = json["isVerified"].bool ?? false
		postalAddress = json["PostalAddress"].string ?? kBlankString
		role = json["Role"].int ?? kInt0
		physicalAddress = PhysicalAddres(json["PhysicalAddress"])
		if let reviewsArray = json["Reviews"].array {
			for reviewsJson in reviewsArray{
				let value = Review(fromJson: reviewsJson)
				reviews.append(value)
			}
		}
		fullName = firstName + " " + lastName
    }
}

class PhysicalAddres {
	
	var id: Int
	var address: String
	var city: String
	var state: String
	var zip: String
	
	init() {
		self.id = kInt0
		self.address = kBlankString
		self.state = kBlankString
		self.city = kBlankString
		self.zip = kBlankString
	}
	
	convenience init(_ json : JSON) {
		self.init()
		self.id = json["Id"].int ?? kInt0
		self.address = json["Address"].string ?? kBlankString
		self.state = json["State"].string ?? kBlankString
		self.city = json["City"].string ?? kBlankString
		self.zip = json["Zip"].string ?? kBlankString
	}
	
}
