//
//  Conversation.swift
//  Shoote
//
//  Created by BrainX IOS 3 on 20/06/2021.
//  Copyright © 2021 Digi Dev. All rights reserved.
//

import Foundation
import SwiftyJSON

class Conversation{
	
	var id = kInt0
	var conversationId = kBlankString
	var businessProfileId = kInt0
	var businessProfile = BusinessProfile()
	var shooteUserId = kInt0
	var shooteUser = ShooteUser()
	var T_BusinessParticipantId = kBlankString
	var T_ShooteParticipantId = kBlankString
	
	init() {
		
	}
	
	init(fromJson json: JSON) {
		id = json["Id"].int ?? kInt0
		conversationId = json["T_ConversationId"].string ?? kBlankString
		businessProfileId = json["BusinessProfileId"].int ?? kInt0
		businessProfile = BusinessProfile(fromJson: json["BusinessProfile"])
		shooteUserId = json["ShooteUserId"].int ?? kInt0
		shooteUser = ShooteUser(fromJson: json["ShooteUser"])
		T_BusinessParticipantId = json["T_BusinessParticipantId"].string ?? kBlankString
		T_ShooteParticipantId = json["T_ShooteParticipantId"].string ?? kBlankString
	}
}
