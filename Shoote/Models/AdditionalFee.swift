//
//  AdditionalFee.swift
//  Shoote
//
//  Created by Muhammad Ansaf on 29/10/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import Foundation
import SwiftyJSON

class AdditionalFee {
	
	var amount : Float = kFloat0
	var durationOrDistance : String = kBlankString
	var id : Int = kInt0
	var shooteServiceList : String = kBlankString
	var shooteServiceListId : Int = kInt0
	var type : String = kBlankString
	
	
	init(fromJson json: JSON) {
		amount = json["Amount"].float ?? kFloat0
		durationOrDistance = json["DurationOrDistance"].string ?? kBlankString
		id = json["Id"].int ?? kInt0
		shooteServiceList = json["ShooteServiceList"].string ?? kBlankString
		shooteServiceListId = json["ShooteServiceListId"].int ?? kInt0
		type = json["Type"].string ?? kBlankString
	}
}
