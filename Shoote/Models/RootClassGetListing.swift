//
//  RootClassGetListing.swift
//  Shoote
//
//  Created by Muhammad Ansaf on 29/10/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import Foundation
import SwiftyJSON

class RootClassGetListing {
	
	var code = kInt0
	var data = [CarListing]()
	var message = kBlankString
	
	init(fromJson json: JSON) {
		code = json["Code"].int ?? kInt0
		message = json["Message"].string ?? kBlankString
		
		if let info = json["Data"].array {
			for item in info {
				let obj = CarListing(fromJson: item)
				data.append(obj)
			}
		}
	}
	
}
