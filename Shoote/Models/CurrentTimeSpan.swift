//
//  CurrentTimeSpan.swift
//  Shoote
//
//  Created by Faheem on 20/02/2021.
//  Copyright © 2021 Digi Dev. All rights reserved.
//

import Foundation
import SwiftyJSON

class CurrentTimeSpan {
    
    var days = kInt0
    var hours = kInt0
    var minutes = kInt0
    var seconds = kInt0
    
    
    init() {
        
    }
    
    convenience init(_ json:JSON){
        self.init()
        
        days = json["Days"].int ?? kInt0
        hours = json["Hours"].int ?? kInt0
        minutes = json["Minutes"].int ?? kInt0
        seconds = json["Seconds"].int ?? kInt0
        
    }
}
