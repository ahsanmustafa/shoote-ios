//
//  StripePayment.swift
//  Shoote
//
//  Created by Faheem on 07/12/2022.
//  Copyright © 2022 Digi Dev. All rights reserved.
//

import Foundation
import SwiftyJSON

class StripePayment{
    
    var customer = kBlankString
    var ephemeralKey = kBlankString
    var setupIntentClientSecret = kBlankString
    var publishableKey = kBlankString
    
    init() {
        
    }
    
    init(fromJson json: JSON) {
    
        customer = json["CustomerID"].stringValue
        ephemeralKey = json["EphemeralKeySecret"].stringValue
        setupIntentClientSecret = json["SetupIntentClientSecret"].stringValue
        publishableKey = json["PublishableKey"].stringValue
    }
}
