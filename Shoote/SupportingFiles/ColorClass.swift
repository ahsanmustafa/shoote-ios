//
//  ColorClass.swift
//  Shoote
//
//  Created by Muhammad Ansaf on 05/11/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import Foundation

import UIKit

extension UIColor{
    convenience init (hex:String) {
        let cString:String = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        self.init(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

// custom colors assign to use in the Project
extension UIColor{
    static let blackColor = UIColor(hex: "000000")
    static let whiteColor = UIColor(hex: "ffffff")
    static let yellowwColor = UIColor(hex: "ffffff")
    static let checkColor = UIColor(hex: "FFCC00")
    static let yellowNewColor = UIColor(hex: "E39B00")
}
