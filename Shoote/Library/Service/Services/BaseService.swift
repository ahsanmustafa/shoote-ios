//
//  BaseService.swift
//  OrderAte
//
//  Created by Gulfam Khan on 17/02/2020.
//  Copyright © 2020 Rapidzz. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class BaseService {
    //MARK:- Shared data
    private var dataRequest:DataRequest?
    
    init() {}
    
    fileprivate var sessionManager:SessionManager {
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 60
        return manager
    }
    
    func getHeaders() -> HTTPHeaders {
        let headers:HTTPHeaders = ["" : kBlankString]
        return headers
    }
    
    //MARK:- POST API Call
    func makePostAPICall(with completeURL:String, params:Parameters?, completion: @escaping (_ error: String, _ success: Bool, _ jsonData:JSON?, _ responseCode:Int)->Void){
//        let headers: HTTPHeaders = ["Authorization": "Bearer \(Global.shared.user.authToken)"]
        print(completeURL)
//        print(headers)
        print(params ?? "")
        dataRequest = sessionManager.request(completeURL, method: .post, parameters: params, encoding: URLEncoding.default,headers: nil)
        dataRequest?
            .validate(statusCode: 200...500)
            .responseJSON(completionHandler: { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print(json)
                    completion("",true, json, response.response?.statusCode ?? -200)
                    
                case .failure(let error):
                    let errorMessage:String = error.localizedDescription
                    print(error)
                    if let err = response.error as? URLError, err.code == .notConnectedToInternet {
                        // no internet connection
                        completion(INTERNET_UNAVAILABLE, false, nil,-200)
                    } else {
                        completion(errorMessage, false,nil, -200)
                    }
                    
                }
            })
    }
    
    //MARK:- Get API Call
    func makeGetAPICall(with completeURL:String, params:Parameters?,completion: @escaping (_ error: String, _ success: Bool, _ resultList:JSON?, _ responseCode:Int)->Void){
        let headers: HTTPHeaders = ["Authorization": "\(Global.shared.user.tokenType) \(Global.shared.user.accessToken)"]
        print(headers)
        print(completeURL)
        print(params ?? "")
        dataRequest = sessionManager.request(completeURL, method: .get, parameters: params, encoding: URLEncoding.default, headers: headers)
        
        dataRequest?
            .validate(statusCode: 200...500)
            .responseJSON(completionHandler: { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print(json)
                    completion("",true, json, response.response?.statusCode ?? -200)
                    
                case .failure(let error):
                    let errorMessage:String = error.localizedDescription
                    print(error)
                    if let err = response.error as? URLError, err.code == .notConnectedToInternet {
                        // no internet connection
                        completion(INTERNET_UNAVAILABLE, false, nil,-200)
                    } else {
                        completion(errorMessage, false,nil, -200)
                    }
                    
                }
            })
        
    }
    
    //MARK:- Multipart Post API Call
    func makePostAPICallWithMultipart(with completeURL:String, dict:[String:Data]?, params:[String:String],dummy : ParamsString, completion: @escaping (_ error: String, _ success: Bool, _ jsonData:JSON?,  _ responseCode:Int)->Void) {
        
        let headers: HTTPHeaders = ["Authorization": "\(Global.shared.user.tokenType) \(Global.shared.user.accessToken)"]
        print(headers)
        print(completeURL)
        print(params)
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            
            for (key, value) in params {
                multipartFormData.append(value.data(using: .utf8)!, withName: key)
            }
//            for (key, value) in dict ?? [:] {
//                multipartFormData.append(value, withName: key)
//            }
            if let data = dict?["profilePicture"] {
                multipartFormData.append(data, withName: "profilePicture")
            }
            if let data = dict?["frontPicture"] {
                multipartFormData.append(data, withName: "frontPicture")
            }
            if let data = dict?["backPicture"] {
                multipartFormData.append(data, withName: "backPicture")
            }
            for (key, value) in dummy {
                multipartFormData.append(value.data(using: .utf8)!, withName: key)
            }
            
            
            
        }, to: completeURL, headers:headers, encodingCompletion: {
            encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { (response) -> Void in
                    
                    switch response.result {
                        
                    case .success(let value):
                        let json = JSON(value)
                        print(json)
                        completion("",true, json, response.response?.statusCode ?? -200)
                        
                    case .failure(let error):
                        let errorMessage:String = error.localizedDescription
                        print(errorMessage)
                        if let err = response.error as? URLError, err.code == .notConnectedToInternet {
                            // no internet connection
                            completion(INTERNET_UNAVAILABLE, false, nil,-200)
                        } else {
                            completion(errorMessage, false,nil, -200)
                        }
                    }
                    
                }
            case .failure(let encodingError):
                print(encodingError.localizedDescription)
            }
        })
        
    }
    
}
