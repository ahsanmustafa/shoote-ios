//
//  API.swift
//  Zon Bau
//
//  Created by Gulfam Khan on 04/09/2019.
//  Copyright © 2019 AcclivousByte. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class LoginService: BaseService {
    
    //MARK:- Shared Instance
    private override init() {}
    static func shared() -> LoginService {
        return LoginService()
    }
    
    fileprivate func saveUserInfo(_ userInfo:LoginUser) {
        Global.shared.user = userInfo
        UserDefaultsManager.shared.isUserLoggedIn = true
        UserDefaultsManager.shared.loggedInUserInfo = userInfo
    }
    
    //MARK:- Register User API.
    func getUserRegister(params:ParamsAny, completion: @escaping (_ error: String, _ success: Bool)->Void){
        let completeURL = EndPoints.BASE_URL + EndPoints.Register
        self.makePostAPICall(with: completeURL, params: params) { (message, success, json, responseCode) in
            if success {
                let code = json!["Code"].int ?? -200
                let msg = json!["Message"].string ?? ""
                if code == 200 {
                    if msg.contains("already Registered"){
                        completion(msg, false)
                    }
                    else {
                         completion(msg, true)
                    }
                }
                else {
                     completion(msg, false)
                }
                
            }
            else {
                if json == nil {
                    if message.isEmpty {
                        completion(PopupMessages.SomethingWentWrong, false)
                    }
                    else {
                        completion(message, false)
                    }
                }
                else {
                    completion(json!["Message"].string ?? PopupMessages.SomethingWentWrong, false)
                }
                
            }
            
        }
    }
    //MARK:- Login User API.
    func getUserLogin(params:ParamsAny, completion: @escaping (_ error: String, _ success: Bool) -> Void){
        let completeURL = EndPoints.BASE_URL + EndPoints.Login
        self.makePostAPICall(with: completeURL, params: params) { (message, success, json, responseCode) in
            
            if success{
                if json!["error"].string != nil {
                    completion(json!["error_description"].string ?? PopupMessages.EmailExists, false)
                }
                else {
                    //User Registered Successfully
                    self.saveUserInfo(LoginUser(json!))
                    GCD.async(.Background, delay: 2) {
                        completion("", true)
                    }
                }
            }
            else {
                if message.isEmpty {
                    completion(PopupMessages.SomethingWentWrong, false)
                }
                else {
                    completion(message, false)
                }
            }
            
        }
    }
    //MARK:- Get User Profile API.
    func getUserProfile(params:ParamsAny,
						completion: @escaping (_ error: String, _ success: Bool, _ userProfile : ShooteUser?) -> Void){
        let completeURL = EndPoints.BASE_URL + EndPoints.GetUserProfile
        self.makeGetAPICall(with: completeURL, params: params) { (message, success, json, responseCode) in
            
            if success{
				completion("", true, ShooteUser(fromJson: json!))
            } else {
				let msg = message.isEmpty ? PopupMessages.SomethingWentWrong : message
                completion(msg, false, nil)
            }
            
        }
    }
    //MARK:- Update User Profile API.
    func updateUserProfile(params:ParamsString, dict:[String:Data]?,dummy : ParamsString, completion: @escaping (_ error: String, _ success: Bool)->Void){
        let completeURL = EndPoints.BASE_URL + EndPoints.UpdateUserProfile
        self.makePostAPICallWithMultipart(with: completeURL,dict: dict, params: params, dummy: dummy) { (message, success, json, responseCode) in
            
            if success {
                let code = json!["Code"].int ?? 200
                let msg = json!["Message"].string ?? ""
                if code == 200 {
                    completion(msg, true)
                }
                else {
                     completion(msg, false)
                }
                
            }
            else {
                if json != nil {
                    if message.isEmpty {
                        completion(json!["Message"].string ?? PopupMessages.SomethingWentWrong, false)
                    }
                    else {
                        completion(json!["Message"].string ?? message, false)
                    }
                }
                else {
					let msg = message.isEmpty ? PopupMessages.SomethingWentWrong : message
					completion(msg, false)
                }
                
            }
            
        }
    }
}
