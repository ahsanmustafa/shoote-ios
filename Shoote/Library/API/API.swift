//
//  API.swift
//  Shoote
//
//  Created by Muhammad Ansaf on 29/10/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import Foundation
import Networking
import SwiftyJSON

class API {
	
	static let shared = API()
    private var networking : Networking!
    private init() {
		networking = Networking(baseURL: EndPoints.BASE_URL, configuration: .default, cache: nil)
    }
    
    // save token of UserDefaullts in networking object
    func setAuthToken(token: String) {
        networking.setAuthorizationHeader(token: token)
    }
	
	private func saveUserInfo(_ userInfo:LoginUser) {
		Global.shared.user = userInfo
		UserDefaultsManager.shared.isUserLoggedIn = true
		UserDefaultsManager.shared.loggedInUserInfo = userInfo
	}
	
	private func parseError(err: FailureJSONResponse) -> String {
		var errorMsg = "Some Error Occured!"
		if let msg = err.dictionaryBody["error_description"] as? String {
			errorMsg = msg
		} else{
			errorMsg = err.error.localizedDescription;
		}
		return errorMsg
	}
    
    // login API call
	func login(params: ParamsAny,
			   result: @escaping ((_ message: String, _ status: Bool) -> Void )) {
		
        print(params)
		networking.post(EndPoints.Login,
						parameterType: Networking.ParameterType.formURLEncoded,
						parameters: params){ (response: JSONResult) in
			
            switch response {
            case .success(let value):
                print(value.dictionaryBody)
				let user = LoginUser(JSON(value.data))
				self.saveUserInfo(user)
				// to access the token
				self.networking.setAuthorizationHeader(token: user.accessToken)
				result(kBlankString, true)
				
            case .failure(let error):
				result(self.parseError(err: error), false)
            }
        }
    }
    
    // signup API call
	func signup(params: ParamsAny,
				result: @escaping ((_ message: String, _ status: Bool) -> Void )) {
        
		networking.post(EndPoints.Register,
						parameterType: Networking.ParameterType.json,
						parameters: params) { (response) in
			
            switch response {
            case .success(let value):
                return result(value.dictionaryBody["Message"] as? String ?? kBlankString, true)
            case .failure(let error):
				result(self.parseError(err: error), false)
            }
        }
    }
    
    // forgotPassword API call
    func forgotPassword(email: String,
						result: @escaping ((_ message: String, _ status: Bool) -> Void )) {
        let requestData = ["Email": email]
        
		networking.post(EndPoints.ForgotPassword,
						parameterType: Networking.ParameterType.formURLEncoded,
						parameters: requestData) { (response) in
			
            switch response {
            case .success(let value):
				result(value.dictionaryBody["Message"] as? String ?? kBlankString, true)
            case .failure(let error):
				result(self.parseError(err: error), false)
            }
        }
    }
	
    //UpdateListing
	func updateListing(jsonData : Data, images: [UIImage],
					   result: @escaping ((_ message: String, _ status: Bool) -> Void )) {
		var parts = [FormDataPart]()
		
		
		parts.append(FormDataPart(type: FormDataPartType.custom("Text"), data: jsonData, parameterName: "jsonData", filename: nil))
		var index = 0
		for image in images {
			if let imageData = image.pngData() {
				let imagePart = FormDataPart(type: .data, data: imageData, parameterName: "image", filename: "image\(index).png")
				parts.append(imagePart)
				index += 1
			}
		}
		
		networking.post(EndPoints.UpdateCarListing,
						parameters: nil,
						parts: parts) { (response) in
			
			switch response {
			case .success(let value):
				let code = value.dictionaryBody["Code"] as? Int ?? 201
				let message = value.dictionaryBody["Message"] as? String ?? kBlankString
				result(message, code == 200 ? true : false)
				
			case .failure(let error):
				return result(self.parseError(err: error), false)
			}
		}
	}
	
    // Create Car Listing
    func createCarListing(jsonData : Data, images: [UIImage],
					   result: @escaping ((_ message: String, _ status: Bool) -> Void )) {
        
        var parts = [FormDataPart]()
        
        parts.append(FormDataPart(type: FormDataPartType.custom("Text"), data: jsonData, parameterName: "jsonData", filename: nil))
		var index = 0
		for image in images {
			if let imageData = image.pngData() {
				let imagePart = FormDataPart(type: .data, data: imageData, parameterName: "image", filename: "image\(index).png")
				parts.append(imagePart)
				index += 1
			}
		}
        
		networking.post(EndPoints.CreateCarListing,
						parameters: nil,
						parts: parts) { (response) in
			
            switch response {
            case .success(let value):
				
				let code = value.dictionaryBody["Code"] as? Int ?? 201
				let message = value.dictionaryBody["Message"] as? String ?? kBlankString
				
				result(message, code == 200 ? true : false)
            case .failure(let error):
				result(self.parseError(err: error), false)
            }
        }
    }
    
    // updateUserProfile
    func updateUserProfile(id : String, image: UIImage, ddf: UIImage,
						   result: @escaping ((_ message: String, _ status: Bool)-> Void )){
        
        var parts = [FormDataPart]()
        
        if let imageData = image.pngData() {
            let imagePart = FormDataPart(type: FormDataPartType.data, data: imageData, parameterName: "image", filename: "file1.png")
            parts.append(imagePart)
        }
		
        if let imageData = ddf.pngData() {
            let imagePart = FormDataPart(type: FormDataPartType.data, data: imageData, parameterName: "ddf", filename: "file2.png")
            parts.append(imagePart)
        }
        
        parts.append(FormDataPart.init(type: FormDataPartType.custom("Text"), data: id.data(using: .utf8)!, parameterName: "id", filename: nil))
        
		networking.post(EndPoints.UpdateUserProfile,
						parameters: nil,
						parts: parts) { (response) in
			
            switch response {
            case .success(let value):
				result(value.dictionaryBody["Message"] as? String ?? kBlankString, true)
            case .failure(let error):
				result(self.parseError(err: error), false)
            }
        }
    }
    
    // Get Car Listings
    func getCarListings( result: @escaping ((_ status: Bool, _ message: String, _ listing: RootClassGetListing?)->())) {
        
		networking.get(EndPoints.GetCarListings) { (response) in
			
            switch response {
            case .success(let value):
				
				let resp = RootClassGetListing(fromJson: JSON(value.data))
				let message = value.dictionaryBody["Message"] as? String ?? kBlankString
				result(true, message , resp)
				
            case .failure(let error):
				result(false, self.parseError(err: error), nil)
            }
        }
    }
	
	// getListings
	func getListingsByUser(result: @escaping ((_ status: Bool, _ message: String, _ listing: RootClassGetListing?) -> Void)) {
		
		networking.get(EndPoints.GetCarListingByBusinessProfile) { (response) in
			
			switch response {
			case .success(let value):
				
				let response = RootClassGetListing(fromJson: JSON(value.data))
				let message = value.dictionaryBody["Message"] as? String ?? kBlankString
				result(true, message , response)
			
			case .failure(let error):
				return result(false, self.parseError(err: error), nil)
			}
		}
	}
    
    // getListingByFilter API call
	func getListingByFilter(name: String?, make: String?, model: String?, year: String?, city: String?, state: String?, exteriorColor: String?, interiorColor: String?, typeOfCar: String?, bodyType: String?, minHours: String?, maxHourlyRate: String?, bookingType: String?,
							result: @escaping ((_ status: Bool, _ message: String, _ list: RootClassGetListing?) -> Void)) {
        var params = [String: Any]()
        if let name = name {
            params["Name"] = name
        }
        
        if let make = make {
            params["Make"] = make
        }
        
        if let val = model {
            params["Model"] = val
        }
        
        if let val = year {
            params["Year"] = val
        }
        
        if let val = city {
            params["City"] = val
        }
        
        if let val = state {
            params["State"] = val
        }
        
        if let val = exteriorColor{
            params["exteriorColor"] = val
        }
		
		if let val = interiorColor{
			params["interiorColor"] = val
		}
        
        if let val = typeOfCar {
            params["TypeOfCar"] = val
        }
        
        if let val = bodyType {
            params["BodyType"] = val
        }
        
        if let val = minHours {
            params["MinHours"] = val
        }
        
        if let val = maxHourlyRate {
            params["MaxHourlyRate"] = val
        }
        
        if let val = bookingType {
            params["BookingType"] = val
        }
        
		networking.get(EndPoints.GetCarListingsByFilter,
					   parameters: params,
					   completion: { (response) in
            switch response {
            case .success(let value):
				
				let response = RootClassGetListing(fromJson: JSON(value.data))
				let message = value.dictionaryBody["Message"] as? String ?? kBlankString
				result(true, message , response)
				
            case .failure(let error):
				return result(false, self.parseError(err: error), nil)
            }
        })
    }
    
    
    // activeOrInActiveListing API call
    func activeOrInActiveListing(Id: Int, SetStatus: Bool,
								 result: @escaping ((_ message: String, _ status: Bool) -> Void )) {
		
        let requestData = ["Id": Id, "SetStatus": SetStatus] as [String : Any]
        
        print("Requested Data List: \(requestData)")
        
		networking.post(EndPoints.ActiveOrInActiveCarListing,
						parameterType: Networking.ParameterType.formURLEncoded,
						parameters: requestData) { (response) in
			
            switch response {
            case .success(let value):
				result(value.dictionaryBody["Message"] as? String ?? kBlankString, true)
            case .failure(let error):
				result(self.parseError(err: error), false)
            }
        }
    }
    
    // ApproveOrUnApproveBooking API call
    func approveOrUnApproveBooking(Id: Int, SetStatus: Bool,
								   result: @escaping ((_ message: String, _ status: Bool)-> Void )) {
        let requestData = ["Id": Id, "SetStatus": SetStatus] as [String : Any]
        print("Requested Data List: \(requestData)")
        
		networking.post(EndPoints.ApproveCarBookingRequest,
						parameterType: Networking.ParameterType.formURLEncoded,
						parameters: requestData) { (response) in
			
            switch response {
            case .success(let value):
				result(value.dictionaryBody["Message"] as? String ?? kBlankString, true)
            case .failure(let error):
				result(self.parseError(err: error), false)
            }
        }
    }
    
//    // getUserProfile
//    func getUserProfile(id: Int,
//						result: @escaping ((_ status: Bool, _ message: String, _ user: BusinessProfile?) -> Void)) {
//        let subUrl = EndPoints.GetAppUserById+"\(id)"
//
//		networking.get(subUrl) { (response) in
//
//            switch response {
//            case .success(let value):
//
//				let response = BusinessProfile(fromJson: JSON(value.data))
//				result(true, "Success", response)
//            case .failure(let error):
//				result(false, self.parseError(err: error), nil)
//            }
//        }
//    }
//
//	// getUserDetailProfile
//	func getUserDetailProfile(id: Int,
//							  result: @escaping ((_ status: Bool, _ message: String, _ user: BusinessProfile?) -> Void)) {
//		let subUrl = EndPoints.GetAppUserProfileById+"\(id)"
//
//		networking.get(subUrl) { (response) in
//
//			switch response {
//			case .success(let value):
//
//				let response = BusinessProfile(fromJson: JSON(value.data))
//				result(true, "Success", response)
//
//			case .failure(let error):
//
//				result(false, self.parseError(err: error), nil)
//			}
//		}
//	}
	
	// getBusinessProfileById
	func getBusinessProfileById(id: Int,
							  result: @escaping ((_ status: Bool, _ message: String, _ user: BusinessProfile?) -> Void)) {
		let subUrl = EndPoints.GetBusinessProfileById
		let params: ParamsAny = ["businessId": id,
								 "includeReviews": true]
		
		networking.get(subUrl,parameters: params) { (response) in
			
			switch response {
			case .success(let value):
				
				let response = BusinessProfile(fromJson: JSON(value.data))
				result(true, "Success", response)
			
			case .failure(let error):
				
				result(false, self.parseError(err: error), nil)
			}
		}
	}
	
	// getShooteUserById
	func getShooteUserById(id: Int,
							  result: @escaping ((_ status: Bool, _ message: String, _ user: ShooteUser?) -> Void)) {
		let subUrl = EndPoints.GetShooteUserById
		let params: ParamsAny = ["UserId": id,
								 "includeReviews": true]
		
		networking.get(subUrl,parameters: params) { (response) in
			
			switch response {
			case .success(let value):
				
				let response = ShooteUser(fromJson: JSON(value.data))
				result(true, "Success", response)
			
			case .failure(let error):
				
				result(false, self.parseError(err: error), nil)
			}
		}
	}
	
	func getCarListingAvailability(listId: Int, startDate: String, endDate: String,
								result: @escaping ((_ status: Bool, _ message: String) -> Void )) {
		
		let params : ParamsAny = ["listingId":listId,
								  "startDate":startDate,
								  "endDate":endDate]
		
		networking.get(EndPoints.GetCarListingAvailability,
					   parameters: params,
					   completion: { (response) in
						
			switch response {
			case .success(let success):
				let code = success.dictionaryBody["Code"] as? Int ?? 400
				let msg = success.dictionaryBody["Message"] as? String ?? PopupMessages.SomethingWentWrong
				let status = code == 200 ? true : false
				return result(status, msg)
				
			case .failure(let error):
				return result(false, self.parseError(err: error))
			}
		})
	}
	
	func createCarBooking(booking: CreateBookingModel,
							  result: @escaping ((_ status: Bool, _ message: String)-> Void )){
		var feeParams = [ParamsAny]()
		
		for item in booking.additionalFees {
			let param : ParamsAny = ["BookingId":item.id,
									 "Type":item.type,
									 "DurationOrDistance":item.durationOrDistance,
									 "Amount":item.amount]
			feeParams.append(param)
		}
		
		let location: ParamsString = ["Address":booking.address,
									  "City":booking.city,
									  "State":booking.state,
									  "Zip":booking.zip]
		
		let params : ParamsAny = ["BusinessProfileId":booking.appUserId,
								  "CarListingId":booking.listingId,
								  "TypeOfBooking":booking.typeOfBooking,
								  "UserDrive":booking.willCarDriven ? 1 : 0,
								  "NoOfPassengers":booking.numberOfPersons,
								  "Description":booking.useOfVehicle,
								  "LocationType":booking.location,
								  "Location": location,
								  "StartDate":booking.startDateString,
								  "EndDate":booking.endDateString,
								  "HourlyTotal":booking.hourlyTotal,
								  "ProcessingFee":booking.processingFee,
								  "TotalFee":booking.totalFee,
								  "Email":booking.email,
								  "PhoneNo":booking.phoneNumber,
								  "AdditionalFees":feeParams]
		
		var jsonData : Data?
		do {
			jsonData = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
		} catch {
			
		}
		var parts = [FormDataPart]()
		parts.append(FormDataPart(type: FormDataPartType.custom("Text"), data: jsonData ?? Data(), parameterName: "jsonData", filename: nil))
		networking.post(EndPoints.CreateCarBooking,
						parameters: nil,
						parts: parts,
						completion: { (response) in

			switch response {
			case .success(let value):

				let code = value.dictionaryBody["Code"] as? Int ?? 400
				let message = value.dictionaryBody["Message"] as? String ?? PopupMessages.SomethingWentWrong
				let status = code == 200 ? true : false
				result(status, message)
				
			case .failure(let error):
				 
				result(false, self.parseError(err: error))
			}
		})
	}
	
	func getSentRequests(result: @escaping ((_ status: Bool, _ message: String, _ booking: [CarBooking]) -> Void)) {
		
		networking.get(EndPoints.GetCarRequestsSentByRenterId,
					   parameters: nil,
					   completion: { (response) in
						
			switch response {
			case .success(let value):
				
				let code = value.dictionaryBody["Code"] as? Int ?? 400
				let message = value.dictionaryBody["Message"] as? String ?? PopupMessages.SomethingWentWrong
				let status = code == 200 ? true : false
				let json = JSON(value.data)
				var list = [CarBooking]()
				for js in json["Data"].arrayValue {
					list.append(CarBooking(js))
				}
				result(status, message, list)
				
			case .failure(let error):
				result(false, self.parseError(err: error), [])
			}
		})
	}
	
	func getReceivedRequests(result: @escaping ((_ status: Bool, _ message: String, _ data: [CarBooking]) -> Void)) {
		
		networking.get(EndPoints.GetCarRequestsReceivedByBusinessProfileId,
					   parameters: nil,
					   completion: { (response) in
			switch response {
			
			case .success(let value):
				
				let code = value.dictionaryBody["Code"] as? Int ?? 400
				let message = value.dictionaryBody["Message"] as? String ?? PopupMessages.SomethingWentWrong
				let status = code == 200 ? true : false
				let json = JSON(value.data)
				var list = [CarBooking]()
				for js in json["Data"].arrayValue {
					list.append(CarBooking(js))
				}
				result(status, message, list)
				
			case .failure(let error):
				result(false, self.parseError(err: error), [])
			}
		})
	}
	
	func deleteCarListing(listId: Int,
						   result: @escaping ((_ status: Bool, _ message: String)-> Void )) {
		let params : ParamsAny = ["carListingId":listId]
		
		networking.get(EndPoints.DeleteCarListing,
					   parameters: params,
					   completion: { (response) in
			switch response {
			case .success(let value):
				
				let code = value.dictionaryBody["Code"] as? Int ?? 400
				let message = value.dictionaryBody["Message"] as? String ?? PopupMessages.SomethingWentWrong
				let status = code == 200 ? true : false
				result(status, message)
				
			case .failure(let error):
				
				result(false, self.parseError(err: error))
			}
		})
	}
	
	func activeOrInActiveListing(id : Int, status: Bool,
								 result: @escaping ((_ status: Bool, _ message: String)-> Void )) {
		let requestData : ParamsAny = ["Id": id, "SetStatus": status]
		
		networking.post(EndPoints.ActiveOrInActiveCarListing,
						parameterType: Networking.ParameterType.formURLEncoded,
						parameters: requestData){ (response: JSONResult) in
			switch response {
			case .success(let value):

				let code = value.dictionaryBody["Code"] as? Int ?? 400
				let message = value.dictionaryBody["Message"] as? String ?? PopupMessages.SomethingWentWrong
				let status = code == 200 ? true : false
				result(status, message)
				
			case .failure(let error):
				
				return result(false, self.parseError(err: error))
			}
		}
	}
    
    // Get Incoming Bookings By Booking User Id
    func getIncomingBookingsByBookingUserId( result: @escaping ((_ status: Bool, _ message: String, _ data: [CarBooking]) -> Void)) {
        
		networking.get(EndPoints.GetIncomingCarBookingsByBookingUser) { (response) in
			
            switch response{
            case .success(let value):
				//Decode the most outer data model.
				let code = value.dictionaryBody["Code"] as? Int ?? 400
				let message = value.dictionaryBody["Message"] as? String ?? PopupMessages.SomethingWentWrong
				let status = code == 200 ? true : false
				let json = JSON(value.data)
				var list = [CarBooking]()
				for js in json["Data"].arrayValue {
					list.append(CarBooking(js))
				}
				 
				result(status, message, list)
            case .failure(let error):
                
				return result(false, self.parseError(err: error), [])
            }
        }
    }
    
    // Get Past Bookings By Booking User Id
    func getPastBookingsByBookingUserId( result: @escaping ((_ status: Bool, _ message: String, _ data: [CarBooking]) -> Void)) {
        
		networking.get(EndPoints.GetPastCarBookingsByBookingUserId) { (response) in
            switch response{
            case .success(let value):
				//Decode the most outer data model.
				let code = value.dictionaryBody["Code"] as? Int ?? 400
				let msg = value.dictionaryBody["Message"] as? String ?? PopupMessages.SomethingWentWrong
				let status = code == 200 ? true : false
				let json = JSON(value.data)
				var list = [CarBooking]()
				for js in json["Data"].arrayValue {
					list.append(CarBooking(js))
				}
				result(status, msg, list)
            case .failure(let error):
                
				result(false, self.parseError(err: error) , [])
            }
        }
    }
    
    // Get Current Bookings
    func getCurrentBookings( result: @escaping ((_ status: Bool, _ message: String, _ data: [CarBooking]) -> Void)) {
        
		networking.get(EndPoints.GetCurrentCarRidesByRenter) { (response) in
            switch response {
            case .success(let value):
				//Decode the most outer data model.
				let code = value.dictionaryBody["Code"] as? Int ?? 400
				let msg = value.dictionaryBody["Message"] as? String ?? PopupMessages.SomethingWentWrong
				let status = code == 200 ? true : false
				let json = JSON(value.data)
				var list = [CarBooking]()
				for js in json["Data"].arrayValue {
					list.append(CarBooking(js))
				}
				result(status, msg, list)
            case .failure(let error):
				
				result(false, self.parseError(err: error) , [])
            }
        }
    }
    
    //startBooking
    func startBooking(bookingId: Int, result: @escaping ((_ status: Bool, _ message: String)-> Void )) {
        let params : ParamsAny = ["bookingId":bookingId]
        
		networking.get(EndPoints.StartCarRiding,
					   parameters: params,
					   completion: { (response) in
            switch response {
            case .success(let value):
                
                let code = value.dictionaryBody["Code"] as? Int ?? 400
                let msg = value.dictionaryBody["Message"] as? String ?? PopupMessages.SomethingWentWrong
                let status = code == 200 ? true : false
                return result(status, msg)
                
            case .failure(let error):
				return result(false, self.parseError(err: error))
            }
        })
    }
    
	func addReview(review: Review,
				   result: @escaping ((_ status: Bool, _ message: String) -> Void )){
		let params : ParamsAny = ["ListingId":review.listingId,
								  "GivenTo": review.givenTo.rawValue,
								  "CarBookingId": review.carBookingId,
								  "BookingUserId": review.bookingUserId,
								  "BusinessProfileId": review.businessProfileId,
								  "ReviewDescription": review.reviewDescription,
								  "CommunicationRating": review.communicationRating,
								  "CleanlinessRating":review.cleanlinessRating,
								  "AccuracyRating":review.accuracyRating,
								  "TimingRating":review.timingRating,
								  "RecommendRating":review.recommendRating]
		
		networking.post(EndPoints.AddReview,
						parameterType: Networking.ParameterType.formURLEncoded,
						parameters: params) { (response) in
			switch response {
			case .success(let value):
				
				let code = value.dictionaryBody["Code"] as? Int ?? 400
				let msg = value.dictionaryBody["Message"] as? String ?? PopupMessages.SomethingWentWrong
				let status = code == 200 ? true : false
				return result(status, msg)
				
			case .failure(let error):
				
				return result(false, self.parseError(err: error))
			}
		}
	}
    
    // Get Incoming Rentals By Owner Id
    func getIncomingRentalsByOwnerId( result: @escaping ((_ status: Bool, _ message: String, _ data: [CarBooking]) -> Void)) {
        
		networking.get(EndPoints.GetIncomingCarRentalsByOwnerId) { (response) in
            switch response {
            case .success(let value):
				//Decode the most outer data model.
				let code = value.dictionaryBody["Code"] as? Int ?? 400
				let msg = value.dictionaryBody["Message"] as? String ?? PopupMessages.SomethingWentWrong
				let status = code == 200 ? true : false
				let json = JSON(value.data)
				var list = [CarBooking]()
				for js in json["Data"].arrayValue {
					list.append(CarBooking(js))
				}
				
				result(status, msg, list)
            case .failure(let error):
                
				return result(false, self.parseError(err: error), [])
            }
        }
    }
    
    // Get Current Rentals
    func GetCurrentRentals( result: @escaping ((_ status: Bool, _ message: String, _ data: [CarBooking]) -> Void)) {
        
		networking.get(EndPoints.GetCurrentCarRidesByOwner) { (response) in
            switch response{
            case .success(let value):
				//Decode the most outer data model.
				let code = value.dictionaryBody["Code"] as? Int ?? 400
				let msg = value.dictionaryBody["Message"] as? String ?? PopupMessages.SomethingWentWrong
				let status = code == 200 ? true : false
				let json = JSON(value.data)
				var list = [CarBooking]()
				for js in json["Data"].arrayValue {
					list.append(CarBooking(js))
				}
				result(status, msg, list)
            case .failure(let error):
                
				result(false, self.parseError(err: error), [])
            }
        }
    }
    
    // Get Past Rentals
    func GetPastRentals( result: @escaping ((_ status: Bool, _ message: String, _ data: [CarBooking]) -> Void)) {
        
		networking.get(EndPoints.GetPastCarRentalsByOwnerId) { (response) in
            switch response{
            case .success(let value):
				//Decode the most outer data model.
				let code = value.dictionaryBody["Code"] as? Int ?? 400
				let msg = value.dictionaryBody["Message"] as? String ?? PopupMessages.SomethingWentWrong
				let status = code == 200 ? true : false
				let json = JSON(value.data)
				var list = [CarBooking]()
				for js in json["Data"].arrayValue {
					list.append(CarBooking(js))
				}
				result(status, msg, list)
            case .failure(let error):
                
				return result(false, self.parseError(err: error), [])
            }
        }
    }
	
	// GetRentalsForCalendarByOwnerId API call
	func getRentalsForCalendarByOwnerId(fromDate: String, toDate: String,
										result: @escaping ((_ status: Bool, _ message: String,_ data: [CarBooking]) -> Void)) {
		var params = [String: Any]()
		params["fromDate"] = fromDate
		params["toDate"] = toDate
		
		networking.get(EndPoints.GetCarRentalsForCalendarByBusinessProfile,
					   parameters: params,
					   completion: { (response) in
			switch response {
			case .success(let value):
				//Decode the most outer data model.
				let code = value.dictionaryBody["Code"] as? Int ?? 400
				let msg = value.dictionaryBody["Message"] as? String ?? PopupMessages.SomethingWentWrong
				let status = code == 200 ? true : false
				let json = JSON(value.data)
				var list = [CarBooking]()
				for js in json["Data"].arrayValue {
					list.append(CarBooking(js))
				}
				result(status, msg, list)
			case .failure(let error):
				
				return result(false, self.parseError(err: error), [])
			}
		})
	}
	
	// GetBookingsForCalendarByUserId API call
	func getBookingsForCalendarByUserId(fromDate: String, toDate: String,
										result: @escaping ((_ status: Bool, _ message: String, _ data: [CarBooking]) -> Void)) {
		var params = [String: Any]()
		params["fromDate"] = fromDate
		params["toDate"] = toDate
		
		networking.get(EndPoints.GetCarBookingsForCalendarByShooteUser,
					   parameters: params,
					   completion: { (response) in
			switch response {
			case .success(let value):
				//Decode the most outer data model.
				let code = value.dictionaryBody["Code"] as? Int ?? 400
				let msg = value.dictionaryBody["Message"] as? String ?? PopupMessages.SomethingWentWrong
				let status = code == 200 ? true : false
				let json = JSON(value.data)
				var list = [CarBooking]()
				for js in json["Data"].arrayValue {
					list.append(CarBooking(js))
				}
				
				result(status, msg, list)
			case .failure(let error):
				
				result(false, self.parseError(err: error), [])
			}
		})
	}
	
	// getConversations API call
	func getConversations(result: @escaping ((_ status: Bool, _ message: String, _ data: [Conversation]) -> Void)) {
		
		networking.get(EndPoints.GetConversations,
					   completion: { (response) in
			switch response {
			case .success(let value):
				//Decode the most outer data model.
				let code = value.dictionaryBody["Code"] as? Int ?? 400
				let msg = value.dictionaryBody["Message"] as? String ?? PopupMessages.SomethingWentWrong
				let status = code == 200 ? true : false
				let json = JSON(value.data)
				var list = [Conversation]()
				for js in json["Data"].arrayValue {
					list.append(Conversation(fromJson: js))
				}
				
				result(status, msg, list)
			case .failure(let error):
				
				result(false, self.parseError(err: error), [])
			}
		})
	}
	
	// openConversation API call
	func openConversation(id: Int, result: @escaping ((_ status: Bool, _ message: String, _ data: Conversation) -> Void)) {
		
		let params = ["BusinessProfileId": id,
					  "ShooteUserId":Global.shared.user.uid]
		networking.post(EndPoints.OpenConversation,
						parameterType: Networking.ParameterType.formURLEncoded,
						parameters: params,
					   completion: { (response) in
			switch response {
			case .success(let value):
				//Decode the most outer data model.
				let code = value.dictionaryBody["Code"] as? Int ?? 400
				let msg = value.dictionaryBody["Message"] as? String ?? PopupMessages.SomethingWentWrong
				let status = code == 200 ? true : false
				let json = JSON(value.data)
				let conversation = Conversation(fromJson: json["Data"])
				
				result(status, msg, conversation)
			case .failure(let error):
				
				result(false, self.parseError(err: error), Conversation())
			}
		})
	}
	
	// getTwillioToken API call
	func getTwillioToken(isBusiness: Bool, result: @escaping ((_ status: Bool, _ message: String, _ token: String) -> Void)) {
		
		let params: ParamsAny = ["isBusiness": isBusiness]
		
		networking.get(EndPoints.GetTwillioToken,
					   parameters: params,
					   completion: { (response) in
			switch response {
			case .success(let value):
				//Decode the most outer data model.
				let token = value.dictionaryBody["token"] as? String ?? kBlankString
				let status = !token.isEmpty
				
				result(status, kBlankString, token)
			case .failure(let error):
				
				result(false, self.parseError(err: error), kBlankString)
			}
		})
	}
	
	// addCohost API call
	func addCohost(params: ParamsAny,
				result: @escaping ((_ message: String, _ status: Bool) -> Void )) {
		
		networking.post(EndPoints.AddShooteUsersByBusinessProfileId,
						parameterType: Networking.ParameterType.json,
						parameters: params) { (response) in
			
			switch response {
			case .success(let value):
				let code = value.dictionaryBody["Code"] as? Int ?? 400
				let msg = value.dictionaryBody["Message"] as? String ?? PopupMessages.SomethingWentWrong
				let status = code == 200 ? true : false
				return result(msg, status)
			case .failure(let error):
				result(self.parseError(err: error), false)
			}
		}
	}
	
	// getCoHosts API call
	func getCoHosts(result: @escaping ((_ status: Bool, _ message: String, _ data: [ShooteUser]) -> Void)) {
		networking.get(EndPoints.GetShooteUsersByBusinessProfileId,
					   completion: { (response) in
			switch response {
			case .success(let value):
				//Decode the most outer data model.
				let code = value.dictionaryBody["Code"] as? Int ?? 400
				let msg = value.dictionaryBody["Message"] as? String ?? PopupMessages.SomethingWentWrong
				let status = code == 200 ? true : false
				let json = JSON(value.data)
				var list = [ShooteUser]()
				for js in json["Data"].arrayValue {
					list.append(ShooteUser(fromJson: js))
				}
				
				result(status, msg, list)
			case .failure(let error):
				
				result(false, self.parseError(err: error), [])
			}
		})
	}
    
    func getStripeKeys(result: @escaping ((_ status: Bool, _ message: String, _ data: StripePayment?) -> Void)) {
        networking.get(EndPoints.CreatePaymentSheet,
                       completion: { (response) in
            switch response {
            case .success(let value):
                //Decode the most outer data model.
                print(value.dictionaryBody)
                let code = value.dictionaryBody["Code"] as? Int ?? 400
                let msg = value.dictionaryBody["Message"] as? String ?? PopupMessages.SomethingWentWrong
                let status = code == 200 ? true : false
                let model = StripePayment(fromJson: JSON(value.data)["Data"])
                
                result(status, msg, model)
            case .failure(let error):
                
                result(false, self.parseError(err: error), nil)
            }
        })
    }
    
    func getPayoutBalance(result: @escaping ((_ status: Bool, _ message: String, _ data: String) -> Void)) {
        networking.get(EndPoints.GetPayoutBalance,
                       completion: { (response) in
            switch response {
            case .success(let value):
                //Decode the most outer data model.
                print(value.dictionaryBody)
                let code = value.dictionaryBody["Code"] as? Int ?? 400
                let msg = value.dictionaryBody["Message"] as? String ?? PopupMessages.SomethingWentWrong
                let status = code == 200 ? true : false
                
                result(status, msg, value.dictionaryBody["Data"] as? String ?? "$ _ . _ _")
            case .failure(let error):
                
                result(false, self.parseError(err: error), "$ _ . _ _")
            }
        })
    }
    
    func getStripePayoutStatus(result: @escaping ((_ status: Bool, _ message: String, _ isPayoutsEnabled: Bool, _ url: String) -> Void)) {
        networking.get(EndPoints.GetStripePayoutStatus,
                       completion: { (response) in
            switch response {
            case .success(let value):
                //Decode the most outer data model.
                print(value.dictionaryBody)
                let code = value.dictionaryBody["Code"] as? Int ?? 400
                let msg = value.dictionaryBody["Message"] as? String ?? PopupMessages.SomethingWentWrong
                let status = code == 200 ? true : false
                let json = JSON(value.data)
                result(status, msg,
                       json["Data"]["PayoutsEnabled"].boolValue,
                       json["Data"]["Url"].stringValue)
            case .failure(let error):
                
                result(false,
                       self.parseError(err: error),
                       false, "")
            }
        })
    }
    
    func processPayout(result: @escaping ((_ status: Bool, _ message: String) -> Void)) {
        networking.get(EndPoints.ProcessPayout,
                       completion: { (response) in
            switch response {
            case .success(let value):
                //Decode the most outer data model.
                print(value.dictionaryBody)
                let code = value.dictionaryBody["Code"] as? Int ?? 400
                let msg = value.dictionaryBody["Message"] as? String ?? PopupMessages.SomethingWentWrong
                let status = code == 200 ? true : false
                result(status, msg)
                
            case .failure(let error):
                result(false, self.parseError(err: error))
            }
        })
    }
}
