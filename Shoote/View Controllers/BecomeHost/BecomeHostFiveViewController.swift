//
//  BecomeHostFiveViewController.swift
//  Shoote
//
//  Created by Faheem on 25/07/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit

class BecomeHostFiveViewController: BaseViewController {
    
	//MARK: Properties
	private var services = [String]()
	
	//MARK: Outlets
    @IBOutlet weak private var username : UILabel!
    @IBOutlet weak private var carTitle : UILabel!
    @IBOutlet weak private var model : UILabel!
    @IBOutlet weak private var image : UIImageView!
    
    @IBOutlet weak private var min : UILabel!
    @IBOutlet weak private var rate : UILabel!
    @IBOutlet weak private var table : UITableView!
    
	//MARK: Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()

		username.text = Global.shared.user.name
        carTitle.text = createListingRequest["Name"] as? String
        model.text = (createListingRequest["Make"] as? String)! + " " + (createListingRequest["Model"] as? String)! + " 2000"
        carTitle.text = createListingRequest["Name"] as? String
        if createListingImages.count > 0
        {
            image.image = createListingImages[0]
        }
        
        
        if let minHours = createListingRequest["MinHours"] as? String,
           let hoours = Double(minHours) {
            if hoours > 1.0 {
                min.text = String(format: "%0.1f", hoours) + "hrs"
            } else {
                min.text = String(format: "%0.1f", hoours) + "hr"
            }
        } else {
            min.text = (createListingRequest["MinHours"] as! String) + "hrs"
        }
        
        if let ratePerHour = createListingRequest["HourlyRate"] as? String,
           let rateHour = Double(ratePerHour){
            rate.text = String(format: "$%0.2f", rateHour)
            
        } else {
            rate.text =  "$" + (createListingRequest["HourlyRate"] as! String)
        }
        
        services = (createListingRequest["CarHostActivity"] as! String).components(separatedBy: ",")
        
        table.dataSource = self
    }

	//MARK: Actions
    @IBAction
	func actionHome(_ sender : UIButton){
        createListingRequest = [:]
        self.navigationController?.popToRootViewController(animated: true)
    }

}

//MARK: UITableViewDataSource Delegate Methods
extension BecomeHostFiveViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return services.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        (cell.contentView.viewWithTag(1) as! UIButton).setTitle(services[indexPath.row], for: .normal)
        
        return cell
    }
}
