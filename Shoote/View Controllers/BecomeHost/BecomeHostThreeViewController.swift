//
//  BecomeHostThreeViewController.swift
//  Shoote
//
//  Created by Faheem on 25/07/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit
import DLRadioButton
import SimpleCheckbox

class BecomeHostThreeViewController: BaseViewController {
    
    //MARK: Outlets
    @IBOutlet weak var policy : UITextView!
    @IBOutlet weak var ownerPresenceYesRadio : DLRadioButton!
    @IBOutlet weak var ownerPresenceNoRadio : DLRadioButton!
    @IBOutlet weak var ownerPresenceCoRadio : DLRadioButton!
    
    @IBOutlet weak var canDriveYesRadio : DLRadioButton!
    @IBOutlet weak var canDriveNoRadio : DLRadioButton!
    
    @IBOutlet weak var videoCheck : Checkbox!
    @IBOutlet weak var filmCheck : Checkbox!
    @IBOutlet weak var photoCheck : Checkbox!
    @IBOutlet weak var commercialCheck : Checkbox!
    @IBOutlet weak var movieCheck : Checkbox!
    
    @IBOutlet weak var flexibleRadio : DLRadioButton!
    @IBOutlet weak var strictRadio : DLRadioButton!
    @IBOutlet weak var veryStrictRadio : DLRadioButton!
    
	//MARK: Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
		
        videoCheck.borderStyle = .square
        videoCheck.checkmarkStyle = .tick
        videoCheck.checkmarkColor = .black
        videoCheck.checkedBorderColor = .clear
        videoCheck.checkboxFillColor = .checkColor
        
        photoCheck.borderStyle = .square
        photoCheck.checkmarkStyle = .tick
        photoCheck.checkmarkColor = .black
        photoCheck.checkedBorderColor = .clear
        photoCheck.checkboxFillColor = .checkColor
        
        commercialCheck.borderStyle = .square
        commercialCheck.checkmarkStyle = .tick
        commercialCheck.checkmarkColor = .black
        commercialCheck.checkedBorderColor = .clear
        commercialCheck.checkboxFillColor = .checkColor
        
        filmCheck.borderStyle = .square
        filmCheck.checkmarkStyle = .tick
        filmCheck.checkmarkColor = .black
        filmCheck.checkedBorderColor = .clear
        filmCheck.checkboxFillColor = .checkColor
        
        movieCheck.borderStyle = .square
        movieCheck.checkmarkStyle = .tick
        movieCheck.checkmarkColor = .black
        movieCheck.checkedBorderColor = .clear
        movieCheck.checkboxFillColor = .checkColor
    }
    
    //MARK: Private Functions
    func checkValidations() -> Bool {
        var isValid = true
        
        if self.policy.text!.isEmpty {
            isValid = false
            policy.becomeFirstResponder()
            self.showAlertView(message: "Enter Policy Rules",title: ALERT_TITLE_APP_NAME)
        }
        
        if !(videoCheck.isChecked || movieCheck.isChecked || filmCheck.isChecked || commercialCheck.isChecked || photoCheck.isChecked)
        {
            isValid = false
            self.showAlertView(message: "Enter Select Activity",title: ALERT_TITLE_APP_NAME)
        }
        
        return isValid
    }
    
	//MARK: Actions
    @IBAction
	func actionBack(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
	
	@IBAction
	func actionContinue(_ sender : UIButton){
		if checkValidations(){
			createListingRequest["AdditionalRules"] = policy.text ?? kBlankString
			// 0 = No, 1 = Yes, 2 = Co Host
			if ownerPresenceNoRadio.isSelected {
				createListingRequest["OwnerPresence"] = 0
			} else if ownerPresenceYesRadio.isSelected {
				createListingRequest["OwnerPresence"] = 1
			} else {
				createListingRequest["OwnerPresence"] = 2
			}
			// 0 = No, 1 = Yes
			if canDriveNoRadio.isSelected {
				createListingRequest["UserDrive"] = 0
			} else {
				createListingRequest["UserDrive"] = 1
			}
			// 0 = Flexible, 1 = Strict, 2 = Very Strict
			if flexibleRadio.isSelected {
				createListingRequest["Cancellation"] = 0
			} else if strictRadio.isSelected{
				createListingRequest["Cancellation"] = 1
			} else {
				createListingRequest["Cancellation"] = 2
			}
			
			var acts = [String]()
			if videoCheck.isChecked {
				acts.append("Video Shoot")
			}
			
			if movieCheck.isChecked {
				acts.append("Movie Scene")
			}
			
			if commercialCheck.isChecked {
				acts.append("Commercials")
			}
			
			if filmCheck.isChecked {
				acts.append("Film Shoot")
			}
			
			if photoCheck.isChecked {
				acts.append("Photo Shoot")
			}
			
			createListingRequest["CarHostActivity"] = acts.joined(separator: ",")
			let vc = UIViewController.instantiate(BecomeHostFourViewController.self, fromStoryboard: .BecomeHostFour)
			self.navigationController?.pushViewController(vc, animated: true)
		}
	}
}
