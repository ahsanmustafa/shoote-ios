//
//  BecomeHostFourViewController.swift
//  Shoote
//
//  Created by Faheem on 25/07/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit

class BecomeHostFourViewController: BaseViewController {
    
	//MARK: Outlets
    @IBOutlet weak var hourlyRate : UITextField!
    
    private var minHoursDD : DropDown!
    @IBOutlet weak private var minHours: UITextField!
    @IBOutlet weak private var minHoursViewDD: UIView!
    
    private var discountDD : DropDown!
    @IBOutlet weak private var discount: UITextField!
    @IBOutlet weak private var discountViewDD: UIView!
    
    @IBOutlet weak private var discountPercentage: UITextField!
    
    private var milesDD : DropDown!
    @IBOutlet weak private var miles: UITextField!
    @IBOutlet weak private var milesViewDD: UIView!
    
    
    @IBOutlet weak private var deliveryFee: UITextField!
    @IBOutlet weak private var cleaning: UITextField!
    @IBOutlet weak private var late: UITextField!
    @IBOutlet weak private var paint: UITextField!
    @IBOutlet weak private var smoke: UITextField!
    @IBOutlet weak private var damage: UITextField!
    
    //MARK: Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        minHoursDD = DropDown(anchorView: minHoursViewDD, selectionAction: { (index, item) in
            self.minHours.text = item
		}, dataSource: Utilities.getHoursList(), topOffset: nil, bottomOffset: nil, cellConfiguration: nil, cancelAction: nil)
        
        discountDD = DropDown(anchorView: discountViewDD, selectionAction: { (index, item) in
            self.discount.text = item
		}, dataSource: Utilities.getDiscountList(), topOffset: nil, bottomOffset: nil, cellConfiguration: nil, cancelAction: nil)
        
        milesDD = DropDown(anchorView: milesViewDD, selectionAction: { (index, item) in
            self.miles.text = item
		}, dataSource:Utilities.getMilesList(), topOffset: nil, bottomOffset: nil, cellConfiguration: nil, cancelAction: nil)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.minHoursDD.selectRow(at: 0)
            self.minHoursDD.selectionAction?(0, (Utilities.getHoursList()[0]))
            
            self.discountDD.selectRow(at: 0)
            self.discountDD.selectionAction?(0, (Utilities.getDiscountList()[0]))
            
            self.milesDD.selectRow(at: 0)
            self.milesDD.selectionAction?(0, (Utilities.getMilesList()[0]))
        }
    }
    
    
	//MARK: Private Methods
    func checkValidations() -> Bool {
        var isValid = true
        
        if NSString(string: hourlyRate.text!).integerValue == 0 {
            isValid = false
            hourlyRate.becomeFirstResponder()
            self.showAlertView(message: "Enter Hourly Rate",title: ALERT_TITLE_APP_NAME)
        } else if NSString(string: discountPercentage.text!).integerValue == 0 {
            isValid = false
            discountPercentage.becomeFirstResponder()
            self.showAlertView(message: "Enter Discount Percentage",title: ALERT_TITLE_APP_NAME)
        }
        
        return isValid
    }
    
	//MARK: Dropdown Methods
    private func toggleHoursDD() {
        if minHoursDD.isHidden
        {
            minHoursDD.show()
        }
    }
	
    private func toggleDiscountDD() {
        if discountDD.isHidden
        {
            discountDD.show()
        }
    }
	
    private func toggleMilesDD() {
        if milesDD.isHidden
        {
            milesDD.show()
        }
    }
    
	
	//MARK: ACtions
	@IBAction
	func actionBack(_ sender : UIButton){
		self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction
	func submit(_ sender : UIButton){
		
		if checkValidations(){
			createListingRequest["HourlyRate"] = hourlyRate.text ?? kBlankString
			if let minHours = minHours.text {
				createListingRequest["MinHours"] = minHours.components(separatedBy: " ")[0]
			}
			
			if let discount = discount.text {
				createListingRequest["DiscountTime"] = discount.components(separatedBy: " ")[0]
			}
			
			createListingRequest["DiscountPercentage"] = discountPercentage.text ?? kBlankString
			
			createListingRequest["ServiceTypeId"] = 1
			createListingRequest["AppUserId"] = 1003
			
			createListingRequest["MaxHours"] = 24
			
			var fees = [[String:Any]]()
			
			if NSString(string: deliveryFee.text ?? kBlankString).integerValue != 0{
				let mi = Utilities.getMilesList()[milesDD.indexForSelectedRow!].components(separatedBy: " ")[0]
				let fee1 = ["ShooteServiceListId" : 1003, "Type" : "Delivery", "DurationOrDistance" : mi, "Amount" : deliveryFee.text!] as [String : Any]
				
				fees.append(fee1)
			}
			
			if NSString(string: cleaning.text ?? kBlankString).integerValue != 0 {
				let fee2 = ["ShooteServiceListId" : 1003, "Type" : "Cleaning Fee", "Amount" : cleaning.text!] as [String : Any]
				fees.append(fee2)
			}
			
			if NSString(string: late.text ?? kBlankString).integerValue != 0 {
				let fee3 = ["ShooteServiceListId" : 1003, "Type" : "Late Penalty Fee", "Amount" : late.text!] as [String : Any]
				fees.append(fee3)

			}
			
			if NSString(string: smoke.text ?? kBlankString).integerValue != 0 {
				let fee4 = ["ShooteServiceListId" : 1003, "Type" : "Smoking Fee", "Amount" : smoke.text!] as [String : Any]
				fees.append(fee4)

			}
			
			if NSString(string: paint.text ?? kBlankString).integerValue != 0 {
				let fee5 = ["ShooteServiceListId" : 1003, "Type" : "Paint Damage", "Amount" : paint.text!] as [String : Any]
				fees.append(fee5)

			}
			
			if NSString(string: damage.text ?? kBlankString).integerValue != 0 {
				let fee6 = ["ShooteServiceListId" : 1003, "Type" : "Interior Tear Damage", "Amount" : damage.text!] as [String : Any]
				fees.append(fee6)

			}
			
			createListingRequest["AdditionalFees"] = fees
			var json : Data?
			do {
				json = try JSONSerialization.data(withJSONObject: createListingRequest, options: .prettyPrinted)
			} catch {
				showAlertView(message: "Error parsing request values: \(error.localizedDescription)")
				return
			}
			
			guard let jsonData = json else {
				return
			}
			
			self.startLoading()
			API.shared.createCarListing(jsonData: jsonData, images: createListingImages) { (msg, status) in
				self.stopLoading()
				if status {
					self.showAlertView(message: msg, title: ALERT_TITLE_APP_NAME, doneButtonTitle: "OK") { (ac) in
						let vc = UIViewController.instantiate(BecomeHostFiveViewController.self, fromStoryboard: .BecomeHostFive)
						self.navigationController?.pushViewController(vc, animated: true)
					}
				} else {
					self.showAlertView(message: msg)
				}
			}
		}
	}
	
    
}

//MARK: UITextField Delegate Methods
extension BecomeHostFourViewController: UITextFieldDelegate {
	
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		   if textField == hourlyRate || textField == discountPercentage || textField == deliveryFee || textField == cleaning || textField == late || textField == paint || textField == smoke || textField == damage {
			   guard CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string)) else {
				   return false
			   }
		   }
		   return true
	   }
	
	func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
		if textField == miles
		{
			toggleMilesDD()
			return false
		}
		else if textField == discount
		{
			toggleDiscountDD()
			return false
		}
		else if textField == minHours
		{
			toggleHoursDD()
			return false
		}
		return true
	}
}
