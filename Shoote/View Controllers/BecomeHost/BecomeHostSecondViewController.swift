//
//  BecomeHostSecondViewController.swift
//  Shoote
//
//  Created by Faheem on 25/07/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit
import SVProgressHUD
import SDWebImage

class BecomeHostSecondViewController: BaseViewController {
    
	//MARK: Outlets
    private var conditionDD : DropDown!
    @IBOutlet weak private var condition: UITextField!
    @IBOutlet weak private var conditionViewDD: UIView!
    
    private var exteriorColorDD : DropDown!
    @IBOutlet weak private var exteriorColor: UITextField!
    @IBOutlet weak private var exteriorColorViewDD: UIView!
	
	private var interiorColorDD : DropDown!
	@IBOutlet weak private var interiorColor: UITextField!
	@IBOutlet weak private var interiorColorViewDD: UIView!
    
    private var typeOfCarDD : DropDown!
    @IBOutlet weak private var typeOfCar: UITextField!
    @IBOutlet weak private var typeOfCarViewDD: UIView!
    
    @IBOutlet weak private var year: UITextField!
    @IBOutlet weak private var make: UITextField!
    @IBOutlet weak private var model: UITextField!
    
    private var transmissionDD : DropDown!
    @IBOutlet weak private var transmission: UITextField!
    @IBOutlet weak private var transmissionViewDD: UIView!
    
    private var bodyStyleDD : DropDown!
    @IBOutlet weak private var bodyStyle: UITextField!
    @IBOutlet weak private var bodyStyleViewDD: UIView!
    
    @IBOutlet weak private var maxCapacity: UITextField!
    
    @IBOutlet weak private var alertSelectTiming: UIView!
    private var startTimingDD : DropDown!
    @IBOutlet weak private var startTiming: UITextField!
    @IBOutlet weak private var startTimingViewDD: UIView!
    
    private var endTimingDD : DropDown!
    @IBOutlet weak private var endTiming: UITextField!
    @IBOutlet weak private var endTimingViewDD: UIView!
    
    
    @IBOutlet weak private var monStart: UIButton!
    @IBOutlet weak private var monEnd: UIButton!
    @IBOutlet weak private var monDash: UILabel!
    
    @IBOutlet weak private var tueStart: UIButton!
    @IBOutlet weak private var tueEnd: UIButton!
    @IBOutlet weak private var tueDash: UILabel!
    
    @IBOutlet weak private var wedStart: UIButton!
    @IBOutlet weak private var wedEnd: UIButton!
    @IBOutlet weak private var wedDash: UILabel!
    
    @IBOutlet weak private var thuStart: UIButton!
    @IBOutlet weak private var thuEnd: UIButton!
    @IBOutlet weak private var thuDash: UILabel!
    
    @IBOutlet weak private var friStart: UIButton!
    @IBOutlet weak private var friEnd: UIButton!
    @IBOutlet weak private var friDash: UILabel!
    
    @IBOutlet weak private var satStart: UIButton!
    @IBOutlet weak private var satEnd: UIButton!
    @IBOutlet weak private var satDash: UILabel!
    
    @IBOutlet weak private var sunStart: UIButton!
    @IBOutlet weak private var sunEnd: UIButton!
    @IBOutlet weak private var sunDash: UILabel!
    
	//MARK: Properties
    private var hoursOfOperations = [Int:[String]]()
    private var notAvailable = [Int:Bool]()
	var selectTimeCurrentDay : Int = 0
	
	//MARK: Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        alertSelectTiming.superview?.isHidden = true
        condition.delegate = self
		exteriorColor.delegate = self
		interiorColor.delegate = self
        typeOfCar.delegate = self
        year.delegate = self
        make.delegate = self
        model.delegate = self
        transmission.delegate = self
        bodyStyle.delegate = self
        maxCapacity.delegate = self
        startTiming.delegate = self
        endTiming.delegate = self
        
        let paddingViewCondition = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: self.condition.frame.height))
        condition.leftView = paddingViewCondition
        condition.leftViewMode = UITextField.ViewMode.always
        
        let paddingViewExteriorColor = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: self.exteriorColor.frame.height))
		exteriorColor.leftView = paddingViewExteriorColor
		exteriorColor.leftViewMode = UITextField.ViewMode.always
		
		let paddingViewInteriorColor = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: self.interiorColor.frame.height))
		interiorColor.leftView = paddingViewInteriorColor
		interiorColor.leftViewMode = UITextField.ViewMode.always
        
        let paddingViewtypeOfCar = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: self.typeOfCar.frame.height))
        typeOfCar.leftView = paddingViewtypeOfCar
        typeOfCar.leftViewMode = UITextField.ViewMode.always
        
        let paddingViewYear = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: self.year.frame.height))
        year.leftView = paddingViewYear
        year.leftViewMode = UITextField.ViewMode.always
        
        let paddingViewTransmission = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: self.transmission.frame.height))
        transmission.leftView = paddingViewTransmission
        transmission.leftViewMode = UITextField.ViewMode.always
        
        let paddingViewbodyStyle = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: self.bodyStyle.frame.height))
        bodyStyle.leftView = paddingViewbodyStyle
        bodyStyle.leftViewMode = UITextField.ViewMode.always
        
        let paddingViewMaxCapacity = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: self.maxCapacity.frame.height))
        maxCapacity.leftView = paddingViewMaxCapacity
        maxCapacity.leftViewMode = UITextField.ViewMode.always
        
        let paddingViewstartTiming = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: self.startTiming.frame.height))
        startTiming.leftView = paddingViewstartTiming
        startTiming.leftViewMode = UITextField.ViewMode.always
        
        let paddingViewendTiming = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: self.endTiming.frame.height))
        endTiming.leftView = paddingViewendTiming
        endTiming.leftViewMode = UITextField.ViewMode.always
        
        condition.attributedPlaceholder = NSAttributedString(string: "Not Selected", attributes: [NSAttributedString.Key.foregroundColor : UIColor.whiteColor])
		exteriorColor.attributedPlaceholder = NSAttributedString(string: "Not Selected", attributes: [NSAttributedString.Key.foregroundColor : UIColor.whiteColor])
		interiorColor.attributedPlaceholder = NSAttributedString(string: "Not Selected", attributes: [NSAttributedString.Key.foregroundColor : UIColor.whiteColor])
        typeOfCar.attributedPlaceholder = NSAttributedString(string: "Not Selected", attributes: [NSAttributedString.Key.foregroundColor : UIColor.whiteColor])
        transmission.attributedPlaceholder = NSAttributedString(string: "Not Selected", attributes: [NSAttributedString.Key.foregroundColor : UIColor.whiteColor])
        bodyStyle.attributedPlaceholder = NSAttributedString(string: "Not Selected", attributes: [NSAttributedString.Key.foregroundColor : UIColor.whiteColor])
        
        conditionDD = DropDown(anchorView: conditionViewDD, selectionAction: { (index, item) in
            self.condition.text = item
		}, dataSource: Utilities.getConditionList(), topOffset: nil, bottomOffset: nil, cellConfiguration: nil, cancelAction: nil)
        view.bringSubviewToFront(condition)
        
		exteriorColorDD = DropDown(anchorView: exteriorColorViewDD, selectionAction: { (index, item) in
            self.exteriorColor.text = item
		}, dataSource: Utilities.getColorList(), topOffset: nil, bottomOffset: nil, cellConfiguration: nil, cancelAction: nil)
        view.bringSubviewToFront(exteriorColor)
		
		interiorColorDD = DropDown(anchorView: interiorColorViewDD, selectionAction: { (index, item) in
			self.interiorColor.text = item
		}, dataSource: Utilities.getColorList(), topOffset: nil, bottomOffset: nil, cellConfiguration: nil, cancelAction: nil)
		view.bringSubviewToFront(interiorColor)
        
        typeOfCarDD = DropDown(anchorView: typeOfCarViewDD, selectionAction: { (index, item) in
            self.typeOfCar.text = item
		}, dataSource: Utilities.getTypeOfCarList(), topOffset: nil, bottomOffset: nil, cellConfiguration: nil, cancelAction: nil)
        view.bringSubviewToFront(typeOfCar)
        
        transmissionDD = DropDown(anchorView: transmissionViewDD, selectionAction: { (index, item) in
            self.transmission.text = item
		}, dataSource: Utilities.getTransmissionList(), topOffset: nil, bottomOffset: nil, cellConfiguration: nil, cancelAction: nil)
        view.bringSubviewToFront(transmission)
        
        bodyStyleDD = DropDown(anchorView: bodyStyleViewDD, selectionAction: { (index, item) in
            self.bodyStyle.text = item
		}, dataSource: Utilities.getBodyStyleList(), topOffset: nil, bottomOffset: nil, cellConfiguration: nil, cancelAction: nil)
        view.bringSubviewToFront(bodyStyle)
        
        startTimingDD = DropDown(anchorView: startTimingViewDD, selectionAction: { (index, item) in
            self.startTiming.text = item
		}, dataSource: Utilities.getStartTimingList(), topOffset: nil, bottomOffset: nil, cellConfiguration: nil, cancelAction: nil)
        view.bringSubviewToFront(startTiming)
        
        endTimingDD = DropDown(anchorView: endTimingViewDD, selectionAction: { (index, item) in
            self.endTiming.text = item
		}, dataSource: Utilities.getEndTimingList(), topOffset: nil, bottomOffset: nil, cellConfiguration: nil, cancelAction: nil)
        view.bringSubviewToFront(endTiming)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.conditionDD.selectRow(at: 0)
			self.conditionDD.selectionAction?(0, (Utilities.getConditionList()[0]))

            self.endTimingDD.selectRow(at: 0)
			self.endTimingDD.selectionAction?(0, (Utilities.getEndTimingList()[0]))
            
            self.startTimingDD.selectRow(at: 0)
			self.startTimingDD.selectionAction?(0, (Utilities.getStartTimingList()[0]))
            
            self.bodyStyleDD.selectRow(at: 0)
			self.bodyStyleDD.selectionAction?(0, (Utilities.getBodyStyleList()[0]))
            
            self.transmissionDD.selectRow(at: 0)
			self.transmissionDD.selectionAction?(0, (Utilities.getTransmissionList()[0]))
            
            self.typeOfCarDD.selectRow(at: 0)
			self.typeOfCarDD.selectionAction?(0, (Utilities.getTypeOfCarList()[0]))
            
            self.exteriorColorDD.selectRow(at: 0)
			self.exteriorColorDD.selectionAction?(0, (Utilities.getColorList()[0]))
			
			self.interiorColorDD.selectRow(at: 0)
			self.interiorColorDD.selectionAction?(0, (Utilities.getColorList()[0]))
            
        }
    }
    
	//MARK:- DropDown Methods
    private func toggleConditionListDD() {
        if conditionDD.isHidden {
            conditionDD.show()
        }
    }
    
    private func toggleExteriorColorListDD() {
        if exteriorColorDD.isHidden{
			exteriorColorDD.show()
        }
    }
	
	private func toggleInteriorColorListDD() {
		if interiorColorDD.isHidden{
			interiorColorDD.show()
		}
	}
    
    
    private func toggleTypeOfCarListDD(){
        if typeOfCarDD.isHidden{
            typeOfCarDD.show()
        }
    }
    
    
    private func toggleTransmissionListDD(){
        if transmissionDD.isHidden{
            transmissionDD.show()
        }
    }
    
    
    private func toggleBodyStyleListDD(){
        if bodyStyleDD.isHidden{
            bodyStyleDD.show()
        }
    }
    
    private func toggleStartTimingListDD(){
        if startTimingDD.isHidden{
            startTimingDD.show()
        }
    }
    
    private func toggleEndTimingListDD(){
        if endTimingDD.isHidden{
            endTimingDD.show()
        }
    }
    
    //MARK:- Private Methods
    private func checkValidations() -> Bool {
        var isValid = true
        
        if self.conditionDD.selectedItem == nil {
            isValid = false
            self.showAlertView(message: "Select Car Condition",title: ALERT_TITLE_APP_NAME)
        } else if self.exteriorColorDD.selectedItem == nil {
            isValid = false
            self.showAlertView(message: "Select Car Exterior Color",title: ALERT_TITLE_APP_NAME)
        } else if self.interiorColorDD.selectedItem == nil {
			isValid = false
			self.showAlertView(message: "Select Car Interior Color",title: ALERT_TITLE_APP_NAME)
		} else if self.typeOfCarDD.selectedItem == nil {
            isValid = false
            self.showAlertView(message: "Select Car Type",title: ALERT_TITLE_APP_NAME)
        } else if NSString(string: year.text!).integerValue == 0 {
            isValid = false
            year.becomeFirstResponder()
            self.showAlertView(message: "Enter a Valid Year",title: ALERT_TITLE_APP_NAME)
        } else if self.make.text!.isEmpty {
            isValid = false
            make.becomeFirstResponder()
            self.showAlertView(message: "Enter Make", title: ALERT_TITLE_APP_NAME)
        } else if self.model.text!.isEmpty {
            isValid = false
            model.becomeFirstResponder()
            self.showAlertView(message: "Enter Model", title: ALERT_TITLE_APP_NAME)
        } else if self.transmissionDD.selectedItem == nil {
            isValid = false
            self.showAlertView(message: "Select Transmission",title: ALERT_TITLE_APP_NAME)
        } else if self.bodyStyleDD.selectedItem == nil {
            isValid = false
            self.showAlertView(message: "Select Body Style",title: ALERT_TITLE_APP_NAME)
        } else if NSString(string: maxCapacity.text!).integerValue == 0 {
            isValid = false
            self.showAlertView(message: "Enter a Valid Max Capacity", title: ALERT_TITLE_APP_NAME)
            maxCapacity.becomeFirstResponder()
        } else if (hoursOfOperations.keys.count + notAvailable.filter { $0.value == true }.count) != 7 {
            isValid = false
            self.showAlertView(message: "All Days Operational Hours are required", title: ALERT_TITLE_APP_NAME)
        }
        
        return isValid
    }
	
	//MARK: Actions
	@IBAction
	func actionBack(_ sender : UIButton){
		self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction
	func actionContinue(_ sender : UIButton){
		if checkValidations(){
			createListingRequest["Make"] = make.text ?? kBlankString
			createListingRequest["Model"] = model.text ?? kBlankString
			createListingRequest["Condition"] = condition.text ?? kBlankString
			createListingRequest["ExteriorColor"] = exteriorColor.text ?? kBlankString
			createListingRequest["InteriorColor"] = interiorColor.text ?? kBlankString
			createListingRequest["TypeOfCar"] = typeOfCar.text ?? kBlankString
			createListingRequest["Year"] = year.text ?? kBlankString
			createListingRequest["Transmission"] = transmission.text ?? kBlankString
			createListingRequest["BodyType"] = bodyStyle.text ?? kBlankString
			createListingRequest["MaxCapacity"] = maxCapacity.text ?? kBlankString
			
			//Timing
			for key in hoursOfOperations.keys {
				if key == 101{
					//Mon
					createListingRequest["MondayTiming"] = hoursOfOperations[key]?.joined(separator: " - ")
				} else if key == 102 {
					//Tue
					createListingRequest["TuesdayTiming"] = hoursOfOperations[key]?.joined(separator: " - ")
				 }else if key == 103 {
					//Wed
					createListingRequest["WednesdayTiming"] = hoursOfOperations[key]?.joined(separator: " - ")
				} else if key == 104 {
					//Thu
					createListingRequest["ThursdayTiming"] = hoursOfOperations[key]?.joined(separator: " - ")
				} else if key == 105 {
					//Fri
					createListingRequest["FridayTiming"] = hoursOfOperations[key]?.joined(separator: " - ")
				} else if key == 106 {
					//Sat
					createListingRequest["SaturdayTiming"] = hoursOfOperations[key]?.joined(separator: " - ")
				} else if key == 107 {
					//Sun
					createListingRequest["SundayTiming"] = hoursOfOperations[key]?.joined(separator: " - ")
				}
			}
			let vc = UIViewController.instantiate(BecomeHostThreeViewController.self, fromStoryboard: .BecomeHostThree)
			self.navigationController?.pushViewController(vc, animated: true)
		}
	}
	
	@IBAction
	func selectTiming(_ sender : UIButton){
		selectTimeCurrentDay = sender.tag
		
		if hoursOfOperations.keys.contains(sender.tag){
			startTiming.text = hoursOfOperations[sender.tag]?[0]
			endTiming.text = hoursOfOperations[sender.tag]?[0]
		} else {
			startTimingDD.selectRow(0)
			endTimingDD.selectRow(0)
		}
		
		alertSelectTiming.superview?.isHidden = false
	}
	
	@IBAction
	func confirmTiming(_ sender : UIButton){
		if startTimingDD.selectedItem == nil {
			self.showAlertView(message: "Select Start Time",title: ALERT_TITLE_APP_NAME)
			return
		}
		
		if endTimingDD.selectedItem == nil {
			self.showAlertView(message: "Select Close Time",title: ALERT_TITLE_APP_NAME)
			return
		}
		
		alertSelectTiming.superview?.isHidden = true
		hoursOfOperations[selectTimeCurrentDay] = [startTiming.text!, endTiming.text!]
		notAvailable[selectTimeCurrentDay] = false
		
		if selectTimeCurrentDay == 101 {
			//Mon
			monStart.setTitle(startTiming.text!, for: .normal)
			monEnd.setTitle(endTiming.text!, for: .normal)
			monDash.text = "-"
		} else if selectTimeCurrentDay == 102 {
			//Tue
			tueStart.setTitle(startTiming.text!, for: .normal)
			tueEnd.setTitle(endTiming.text!, for: .normal)
			tueDash.text = "-"
		} else if selectTimeCurrentDay == 103 {
			//Wed
			wedStart.setTitle(startTiming.text!, for: .normal)
			wedEnd.setTitle(endTiming.text!, for: .normal)
			wedDash.text = "-"
		} else if selectTimeCurrentDay == 104 {
			//Thr
			thuStart.setTitle(startTiming.text!, for: .normal)
			thuEnd.setTitle(endTiming.text!, for: .normal)
			thuDash.text = "-"
		} else if selectTimeCurrentDay == 105 {
			//Fri
			friStart.setTitle(startTiming.text!, for: .normal)
			friEnd.setTitle(endTiming.text!, for: .normal)
			friDash.text = "-"
		} else if selectTimeCurrentDay == 106 {
			//Sat
			satStart.setTitle(startTiming.text!, for: .normal)
			satEnd.setTitle(endTiming.text!, for: .normal)
			satDash.text = "-"
		} else if selectTimeCurrentDay == 107 {
			//Sun
			sunStart.setTitle(startTiming.text!, for: .normal)
			sunEnd.setTitle(endTiming.text!, for: .normal)
			sunDash.text = "-"
		}
		selectTimeCurrentDay = 0
	}
	
	@IBAction func notAvailableTiming(_ sender : UIButton){
		alertSelectTiming.superview?.isHidden = true
		hoursOfOperations.removeValue(forKey: selectTimeCurrentDay)
		notAvailable[selectTimeCurrentDay] = true
		
		if selectTimeCurrentDay == 101 {
			monStart.setTitle("", for: .normal)
			monDash.text = ""
			monEnd.setTitle("Not Available", for: .normal)
		} else if selectTimeCurrentDay == 102 {
			tueStart.setTitle("", for: .normal)
			tueDash.text = ""
			tueEnd.setTitle("Not Available", for: .normal)
		} else if selectTimeCurrentDay == 103 {
			wedStart.setTitle("", for: .normal)
			wedDash.text = ""
			wedEnd.setTitle("Not Available", for: .normal)
		} else if selectTimeCurrentDay == 104 {
			thuStart.setTitle("", for: .normal)
			thuDash.text = ""
			thuEnd.setTitle("Not Available", for: .normal)
		} else if selectTimeCurrentDay == 105 {
			friStart.setTitle("", for: .normal)
			friDash.text = ""
			friEnd.setTitle("Not Available", for: .normal)
		} else if selectTimeCurrentDay == 106 {
			satStart.setTitle("", for: .normal)
			satDash.text = ""
			satEnd.setTitle("Not Available", for: .normal)
		} else if selectTimeCurrentDay == 107 {
			sunStart.setTitle("", for: .normal)
			sunDash.text = ""
			sunEnd.setTitle("Not Available", for: .normal)
		}
		selectTimeCurrentDay = 0
	}

}

//MARK:- UITextField Delegate Methods
extension BecomeHostSecondViewController : UITextFieldDelegate {
	
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		if textField == year || textField == maxCapacity{
			guard CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string)) else {
				return false
			}
		}
		return true
	}
	
	func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
		
		if textField == condition {
			toggleConditionListDD()
			return false
		} else if textField == exteriorColor {
			toggleExteriorColorListDD()
			return false
		} else if textField == interiorColor {
			toggleInteriorColorListDD()
			return false
		} else if textField == typeOfCar {
			toggleTypeOfCarListDD()
			return false
		} else if textField == transmission {
			toggleTransmissionListDD()
			return false
		} else if textField == bodyStyle {
			toggleBodyStyleListDD()
			return false
		} else if textField == startTiming {
			toggleStartTimingListDD()
			return false
		} else if textField == endTiming {
			toggleEndTimingListDD()
			return false
		}
		return true
	}
}
