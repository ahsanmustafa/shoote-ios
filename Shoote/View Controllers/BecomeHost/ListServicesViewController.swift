//
//  ListServicesViewController.swift
//  Shoote
//
//  Created by Faheem on 18/07/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit

class ListServicesViewController: BaseViewController {

	//MARK: Properties
    var nameArr = ["Car","Home","Actor","Jet","Location","Model","Yacht","Prop","Makeup\nArtist","Photo-\ngraphy","Cinemato\ngraphy","Mansion","Villa","Condos","Photo Studio","Music\nRecording\nStudio","Office","Store Front"]
    
	//MARK: Override Functions
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
	//MARK: Actions
    @IBAction
	func actionBack(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
	
	@IBAction
	func actionCreateListing(_ sender : UIButton){
		let vc = UIViewController.instantiate(BecomeHostViewController.self, fromStoryboard: .CreateService)
		self.navigationController?.pushViewController(vc, animated: true)
	}

}

//MARK: UICollectionViewDelegate and UICollectionViewDataSource
extension ListServicesViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return nameArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ListServicesCollectionViewCell", for: indexPath) as! ListServicesCollectionViewCell
        let size = cell.frame.size.width / 2
        cell.viewMain.layer.cornerRadius = size
        cell.viewMain.clipsToBounds = true
        
        cell.name.text = nameArr[indexPath.row]
        
        if indexPath.row == 0 {
            cell.viewMain.backgroundColor = UIColor.white
        } else {
            cell.viewMain.backgroundColor = UIColor.lightGray
        }
        
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = (collectionView.frame.size.width / 3) - 10
        return CGSize(width: size, height: size)
    }
    
}
