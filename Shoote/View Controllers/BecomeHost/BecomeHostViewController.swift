//
//  BecomeHostViewController.swift
//  Shoote
//
//  Created by Faheem on 19/07/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit
import DLRadioButton
import GooglePlaces
import DKImagePickerController
import SkyFloatingLabelTextField

var createListingRequest = ParamsAny()
var createListingImages = [UIImage]()
class BecomeHostViewController: BaseViewController {
	
	//MARK: Properties
	private var imagesPicked = false
	private var radiusDD : DropDown!
	private var milesIndex = 0
	lazy var imagePicker : DKImagePickerController = {
		return DKImagePickerController()
	}()
	
    //MARK: Outlets
    @IBOutlet weak private var carTitle: SkyFloatingLabelTextField!
    @IBOutlet weak private var search: UITextField!
    @IBOutlet weak private var city: SkyFloatingLabelTextField!
    @IBOutlet weak private var state: SkyFloatingLabelTextField!
    @IBOutlet weak private var zip: SkyFloatingLabelTextField!
    @IBOutlet weak private var desc: UITextView!
    @IBOutlet weak var viewRadiusDropDown: UIView!
    @IBOutlet weak var lblRaiusValue: UILabel!
    @IBOutlet weak private var instantRadio: DLRadioButton!
    @IBOutlet weak private var manualRadio: DLRadioButton!
    
	//MARK: Override Functions
    override func viewDidLoad() {
        super.viewDidLoad()
		setData()
    }
    
    //MARK: Private Functions
	
	private func setData() {
		GMSPlacesClient.provideAPIKey(APIKeys.GoogleMapsKey)
		search.delegate = self
		[carTitle, city, state, zip].forEach({$0.titleFormatter = { $0 }})
		radiusDD = DropDown(anchorView: viewRadiusDropDown, selectionAction: { (index, item) in
			self.milesIndex = index
			self.lblRaiusValue.text = item
		}, dataSource: Utilities.getRaduisMilesList(), topOffset: nil, bottomOffset: nil, cellConfiguration: nil, cancelAction: nil)
	}
	
    private func checkValidations() -> Bool {
        var isValid = true
        
        if self.carTitle.text!.isEmpty {
            isValid = false
            carTitle.becomeFirstResponder()
            self.showAlertView(message: "Enter Car Title",title: ALERT_TITLE_APP_NAME)
        }
        else if self.city.text!.isEmpty {
            isValid = false
            city.becomeFirstResponder()
            self.showAlertView(message: "Enter City",title: ALERT_TITLE_APP_NAME)
        }
        else if self.state.text!.isEmpty {
            isValid = false
            state.becomeFirstResponder()
            self.showAlertView(message: "Enter State",title: ALERT_TITLE_APP_NAME)
        }
        else if self.zip.text!.isEmpty {
            isValid = false
            zip.becomeFirstResponder()
            self.showAlertView(message: "Enter Zip", title: ALERT_TITLE_APP_NAME)
        }
        else if self.desc.text!.isEmpty {
            isValid = false
            desc.becomeFirstResponder()
            self.showAlertView(message: "Enter Car Description", title: ALERT_TITLE_APP_NAME)
        }else if !self.manualRadio.isSelected && !self.instantRadio.isSelected
        {
            isValid = false
            self.showAlertView(message: "Select Confirmation Type", title: ALERT_TITLE_APP_NAME)
        }else if !imagesPicked
        {
            isValid = false
            self.showAlertView(message: "Booking Images are required", title: ALERT_TITLE_APP_NAME)
        }
        
        return isValid
    }
    
	//MARK: Actions
	@IBAction
	func actionBack(_ sender : UIButton){
		self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction
	func actionContinue(_ sender : UIButton){
		if checkValidations() {
			createListingRequest["Name"] = carTitle.text
			let location = ["City": city.text ?? kBlankString,
							"State": state.text ?? kBlankString,
							"Zip": zip.text ?? kBlankString]
			createListingRequest["Location"] = location
			createListingRequest["Description"] = desc.text ?? kBlankString
			createListingRequest["IsManual"] = manualRadio.isSelected
			createListingRequest["MaxDeliveryRadius"] = (milesIndex + 1) * 5
			let vc = UIViewController.instantiate(BecomeHostSecondViewController.self, fromStoryboard: .BecomeHostSecond)
			self.navigationController?.pushViewController(vc, animated: true)
		}
	}
	
	@IBAction
	func actionShowDropDown(_ sender : UIButton){
		radiusDD.show()
	}
	
	@IBAction
	func pickImage(_ sender : UIButton){
		imagePicker.allowSwipeToSelect = true
		imagePicker.maxSelectableCount = 10
		imagePicker.delegate = self
		imagePicker.didSelectAssets = { (assets: [DKAsset]) in
			createListingImages = [UIImage]()
			for ass in assets
			{
				self.imagesPicked = true
				ass.fetchOriginalImage(completeBlock: { (ui, info) in
					if let ui = ui {
						createListingImages.append(ui)
					}
				})
			}
		}
		
		self.present(imagePicker, animated: true, completion: nil)
	}
	
}

//MARK:- ImagePickerControllerDelegate Methods
extension BecomeHostViewController {
	
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let chosenImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        createListingImages.append(chosenImage)
        picker.dismiss(animated: true, completion: nil)
    }
}

//MARK:- UITextFieldDelegate Methods
extension BecomeHostViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.endEditing(true)
        let acController = GMSAutocompleteViewController()
        acController.delegate = self
        let filter = GMSAutocompleteFilter()
        filter.country = "us"
        acController.autocompleteFilter = filter
        present(acController, animated: true, completion: nil)
    }
}

//MARK:- GMSAutocompleteViewControllerDelegate Methods
extension BecomeHostViewController : GMSAutocompleteViewControllerDelegate {
	
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        // Get the place name from 'GMSAutocompleteViewController'
        // Then display the name in textField
        search.text = place.name
        if let address = place.addressComponents {
            for comp in address {
                for type in comp.types {
                    if type == "locality" {
                        city.text = comp.name
                    } else if type == "administrative_area_level_1" {
                        state.text = comp.name
                    } else if type == "postal_code" {
                        zip.text = comp.name
                    }
                }
            }
        }
        // Dismiss the GMSAutocompleteViewController when something is selected
        dismiss(animated: true, completion: nil)
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // Handle the error
        print("Error: ", error.localizedDescription)
    }
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        // Dismiss when the user canceled the action
        dismiss(animated: true, completion: nil)
    }
}
