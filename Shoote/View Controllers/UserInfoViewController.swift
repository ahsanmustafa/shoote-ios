//
//  UserInfoViewController.swift
//  Shoote
//
//  Created by Faheem on 25/07/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class UserInfoViewController: BaseViewController {

	
	//MARK: Properties
	var userImage : UIImage?
	var frontImage : UIImage?
	var baackImage : UIImage?
	var isProfileImg = false
	var isFrondImg = false
	var isBackEndImg = false
	var toolBar = UIToolbar()
	var datePicker  = UIDatePicker()
	var userProfile = ShooteUser()
	
    //MARK: Outlets
    @IBOutlet weak var txtFname : SkyFloatingLabelTextField!
    @IBOutlet weak var txtLname : SkyFloatingLabelTextField!
	@IBOutlet weak var txtBio : SkyFloatingLabelTextField!
    @IBOutlet weak var txtEmail : SkyFloatingLabelTextField!
    @IBOutlet weak var txtPhone : SkyFloatingLabelTextField!
    @IBOutlet weak var txtHomePhone : SkyFloatingLabelTextField!
    @IBOutlet weak var txtCity : SkyFloatingLabelTextField!
    @IBOutlet weak var txtState : SkyFloatingLabelTextField!
    @IBOutlet weak var txtZip : SkyFloatingLabelTextField!
    @IBOutlet weak var txtGovtID : SkyFloatingLabelTextField!
    @IBOutlet weak var txtIDState : SkyFloatingLabelTextField!
    @IBOutlet weak var imgUserProfile : UIImageView!
    @IBOutlet weak var imgFrontTick : UIImageView!
    @IBOutlet weak var imgBackTick : UIImageView!
    @IBOutlet weak var btnExpirtDate : UIButton!
	
    //MARK: Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        getUserProfile()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.actionProfileImage(_:)))
        imgUserProfile.isUserInteractionEnabled = true
        imgUserProfile.addGestureRecognizer(tap)
    }
	
	//MARK: Private Methods
    private func populateDate(){
		[txtFname, txtLname, txtBio, txtEmail, txtPhone, txtHomePhone, txtCity, txtState,
		 txtZip, txtGovtID, txtIDState].forEach { textfield in
			textfield?.titleFormatter = { $0 }
		}
        txtFname.text = userProfile.firstName
        txtLname.text = userProfile.lastName
		txtBio.text = userProfile.bio
        txtEmail.text = userProfile.email
        txtPhone.text = userProfile.primaryContact
        txtHomePhone.text = userProfile.secondaryContact
		txtCity.text = userProfile.physicalAddress.city
		txtState.text = userProfile.physicalAddress.state
		txtZip.text = userProfile.physicalAddress.zip
        txtGovtID.text = userProfile.idDocNum
        txtIDState.text = userProfile.idLocation
        if !userProfile.profileImage.isEmpty {
            let url = userProfile.profileImage
            setImageWithUrl(imageView: imgUserProfile, url: EndPoints.BASE_URL_IMG + url, placeholderImage: "user-large")
        }
        if !userProfile.idFront.isEmpty {
            imgFrontTick.isHidden = false
        }
        if !userProfile.idBack.isEmpty {
            imgBackTick.isHidden = false
        }
        if !userProfile.idExpiryDate.isEmpty {
            btnExpirtDate.setTitle(userProfile.idExpiryDate, for: .normal)
        }
    }
	
	private func checkValidation(){
		
	}
	
	private func getParams () -> (ParamsString, ParamsDic,ParamsString) {
		
		var params : ParamsString = ["FirstName":txtFname.text!,
									 "LastName":txtLname.text!,
									 "Bio": txtBio.text!,
									 "Email":txtEmail.text!,
									 "PhoneNumber":txtPhone.text!,
									 "HomePhone":txtHomePhone.text!,
									 "City":txtCity.text!,
									 "State":txtState.text!,
									 "ZipCode":txtZip.text!,
									 "GovtId":txtGovtID.text!,
									 "IdState":txtIDState.text!]
		let expiryDate = btnExpirtDate.titleLabel!.text!
		if expiryDate != "Expiry Date" {
			params["ExpiryDate"] = expiryDate
		}
		else {
			params["ExpiryDate"] = ""
		}
		let json = try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
		let finalParams : ParamsString = ["key1":String(data: json, encoding: .utf8)!]
		var dictDummy : ParamsString = [:]
		var dict : ParamsDic = [:]
		if let img = userImage
		{
			dict["profilePicture"] = img.jpegData(compressionQuality: 0.5)!
		}
		else {
			dictDummy["profilePicture"] = ""
		}
		if let img = frontImage {
			dict["frontPicture"] = img.jpegData(compressionQuality: 0.5)!
		}
		else {
			dictDummy["frontPicture"] = ""
		}
		if let img = baackImage {
			dict["backPicture"] = img.jpegData(compressionQuality: 0.5)!
		}
		else {
			dictDummy["backPicture"] = ""
		}
		
		return (finalParams, dict,dictDummy)
	}

	//MARK: Actions
    @IBAction
	func actionBack(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
	
    @IBAction
	func actionUpdate(_ sender : UIButton){
        if checkInternetConnection() {
            let params = getParams()
            updateUserProfile(withParams: params.0, withDict: params.1, withDummy: params.2)
        }
    }
    
    @IBAction
	func actionIdFrontImage(_ sender : UIButton){
        isFrondImg = true
        fetchProfileImage("Selcet Front Image")
    }
    
    @IBAction
	func actionIdBackImage(_ sender : UIButton){
        isBackEndImg = true
        fetchProfileImage("Selcet Back Image")
    }
	
    @objc
	func actionProfileImage(_ tap : UITapGestureRecognizer){
        isProfileImg = true
        fetchProfileImage("Select Profile Picture")
    }
	
    @IBAction
	func showDatePicker(_ sender: UIButton) {
        datePicker = UIDatePicker.init()
        datePicker.backgroundColor = UIColor.white

        datePicker.autoresizingMask = .flexibleWidth
        datePicker.datePickerMode = .date

        datePicker.addTarget(self, action: #selector(self.dateChanged(_:)), for: .valueChanged)
        datePicker.frame = CGRect(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 300)
        self.view.addSubview(datePicker)

        toolBar = UIToolbar(frame: CGRect(x: 0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 50))
        toolBar.barStyle = .blackTranslucent
        toolBar.items = [UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil), UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.onDoneButtonClick))]
        toolBar.sizeToFit()
        self.view.addSubview(toolBar)
    }

    @objc
	func dateChanged(_ sender: UIDatePicker?) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yy"

        if let date = sender?.date {
            let date = dateFormatter.string(from: date)
            print("Picked the date \(date)")
            btnExpirtDate.setTitle(date, for: .normal)
        }
    }

    @objc
	func onDoneButtonClick() {
        toolBar.removeFromSuperview()
        datePicker.removeFromSuperview()
    }

	//MARK: UIImagePickerController Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image =  info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        if isProfileImg {
            userImage = image
            imgUserProfile.image = image
        }
        else if isFrondImg {
            frontImage = image
            imgFrontTick.isHidden = false
        }
        else if isBackEndImg {
            baackImage = image
            imgBackTick.isHidden = false
        }
        isProfileImg = false
        isFrondImg = false
        isBackEndImg = false
        picker.dismiss(animated: true, completion: nil)
    }

}

//MARK: - API CALL
extension UserInfoViewController {
	
    private func getUserProfile() {
        startLoading()
        GCD.async(.Background) {
            LoginService.shared().getUserProfile(params: [:]) { (message, success, userProfile) in
                GCD.async(.Main) {
                    self.stopLoading()
                    if success {
                        if let profile = userProfile {
                            self.userProfile = profile
                            self.populateDate()
                        }
                    }else {
                        self.showAlertView(message: message)
                    }
                }
            }
        }
    }
	
    private func updateUserProfile(withParams params : ParamsString, withDict dict : ParamsDic?, withDummy dummy : ParamsString) {
		
        startLoading()
        GCD.async(.Background) {
            LoginService.shared().updateUserProfile(params: params, dict: dict, dummy: dummy) { (message, success) in
                GCD.async(.Main) {
                    self.stopLoading()
                    if success {
                        self.showAlertView(message: message, title: ALERT_TITLE_APP_NAME, doneButtonTitle: "Ok") { (alert) in
                            self.navigationController?.popViewController(animated: true)
                        }
                    } else {
                        self.showAlertView(message: message)
                    }
                }
            }
        }
    }
}
