//
//  ViewBookingsAndRentalsViewController.swift
//  Shoote
//
//  Created by Faheem on 20/02/2021.
//  Copyright © 2021 Digi Dev. All rights reserved.
//

import UIKit

enum ViewBookingsAndRentals : String {
    case incommingBookings = "Incomming Bookings"
    case currentBookings = "Current Bookings"
    case pastBookings = "Past Bookings"
    case incomingRentals = "Incoming Rentals"
    case currentRentals = "Current Rentals"
    case pastRentals = "Past Rentals"
}


class ViewBookingsAndRentalsViewController: BaseViewController {

	//MARK: Properties
	var dataList = [CarBooking]()
	var selectedType: ViewBookingsAndRentals = .incommingBookings
	var selectedIndexForRating:Int?
	var isOwner = false
	
	//MARK: Outlets
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var lblTopHeading : UILabel!
    
	//MARK: Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        getListingDate(with: selectedType)
        lblTopHeading.text = selectedType.rawValue
        if selectedType == .pastBookings {
            isOwner = true
        } else {
            isOwner = false
        }
    }
    
	//MARK: Private Methods
    private func getListingDate(with type: ViewBookingsAndRentals) {
        switch type {
        case .incommingBookings:
            getIncomingBookings()
        case .currentBookings:
            getCurrentBookings()
        case .pastBookings:
            getPastBookings()
        case .incomingRentals:
            getIncomingRentals()
        case .currentRentals:
            getCurrentRentals()
        case .pastRentals:
            getPastRentals()
        }
    }
    
    
	//MARK: Actions
    @IBAction
	func actionBack(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction
	func actionViewBooking(_ sender : UIButton){
		let vc = UIViewController.instantiate(ViewBookingViewController.self, fromStoryboard: .CreateService)
        vc.type = selectedType
        vc.data = dataList[sender.tag]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction
	func actionReviewOwner(_ sender : UIButton){
        selectedIndexForRating = sender.tag
        let vc = UIViewController.instantiate(RatingReviewDialogueViewController.self, fromStoryboard: .CreateService)
        vc.modalPresentationStyle = .overFullScreen
        vc.delegate = self
        if isOwner {
            vc.text = "Review Owner"
			vc.acuracyTitle = "Listing Accuracy"
        } else {
            vc.text = "Review Renter"
			vc.acuracyTitle = "Profile Accuracy"
        }
        present(vc, animated: false, completion: nil)
    }
	
    @IBAction
	func actionStartBooking(_ sender : UIButton){
        self.startLoading()
        API.shared.startBooking(bookingId: dataList[sender.tag].id) { (status, message) in
            self.stopLoading()
            if status {
                self.showAlertView(message: message, title: ALERT_TITLE_APP_NAME, doneButtonTitle: "Ok") { (alert) in
                    self.navigationController?.popViewController(animated: true)
                }
            } else {
                self.showAlertView(message: message, title: ALERT_TITLE_APP_NAME)
            }
        }
    }
    
    @objc
	func actionImageHost(_ tap : UITapGestureRecognizer){
		let vc = UIViewController.instantiate(ViewUserProfileViewController.self, fromStoryboard: .CreateService)
		vc.userType = "host"
		vc.id = dataList[tap.view?.tag ?? 0].ownerId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc
	func actionImageRenter(_ tap : UITapGestureRecognizer){
		let vc = UIViewController.instantiate(ViewUserProfileViewController.self, fromStoryboard: .CreateService)
		vc.userType = "renter"
		vc.id = dataList[tap.view?.tag ?? 0].bookingUserId
		self.navigationController?.pushViewController(vc, animated: true)
    }

}

//MARK: UITableViewDelegate and UITableViewDataSource Methods
extension ViewBookingsAndRentalsViewController: UITableViewDelegate, UITableViewDataSource {
	
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if selectedType == .currentBookings || selectedType == .incommingBookings || selectedType == .pastBookings {
            let cell = tableView.dequeueReusableCell(withIdentifier: "IncommingBookingsTableViewCell") as! IncommingBookingsTableViewCell
            cell.imgHost.isUserInteractionEnabled = true
            cell.imgRenter.isUserInteractionEnabled = true
            let hostTap = UITapGestureRecognizer(target: self, action: #selector(self.actionImageHost(_:)))
            cell.imgHost.addGestureRecognizer(hostTap)
            let renterTap = UITapGestureRecognizer(target: self, action: #selector(self.actionImageRenter(_:)))
            cell.imgRenter.addGestureRecognizer(renterTap)
            cell.btnViewBooking.addTarget(self, action: #selector(self.actionViewBooking(_:)), for: .touchUpInside)
            cell.btnViewBooking.tag = indexPath.row
            cell.btnStartBooking.tag = indexPath.row
            if selectedType == .incommingBookings {
                cell.btnStartBooking.addTarget(self, action: #selector(self.actionStartBooking(_:)), for: .touchUpInside)
            } else if selectedType == .pastBookings {
                cell.btnStartBooking.addTarget(self, action: #selector(self.actionReviewOwner(_:)), for: .touchUpInside)
            }
            cell.setData(dataList[indexPath.row], type : selectedType)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "IncomingRentalTableViewCell") as! IncomingRentalTableViewCell
            cell.imgHost.isUserInteractionEnabled = true
            cell.imgRenter.isUserInteractionEnabled = true
            let hostTap = UITapGestureRecognizer(target: self, action: #selector(self.actionImageHost(_:)))
            cell.imgHost.addGestureRecognizer(hostTap)
            let renterTap = UITapGestureRecognizer(target: self, action: #selector(self.actionImageRenter(_:)))
            cell.imgRenter.addGestureRecognizer(renterTap)
            cell.btnViewBooking.addTarget(self, action: #selector(self.actionViewBooking(_:)), for: .touchUpInside)
            cell.btnViewBookingSingle.addTarget(self, action: #selector(self.actionViewBooking(_:)), for: .touchUpInside)
            cell.btnViewBooking.tag = indexPath.row
            cell.btnStartBooking.tag = indexPath.row
            cell.btnStartBooking.tag = indexPath.row
            if selectedType == .pastRentals {
                cell.btnStartBooking.addTarget(self, action: #selector(self.actionReviewOwner(_:)), for: .touchUpInside)
            }
            cell.setData(dataList[indexPath.row], type : selectedType)
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 487
    }
}

//MARK: RatingReviewDialogueDelegate Methods
extension ViewBookingsAndRentalsViewController : RatingReviewDialogueDelegate {
	
    func didSelectRating(review: Review) {
        guard let index = selectedIndexForRating else {
            return
        }
        if isOwner {
			review.givenTo = .BUSINESS
        } else {
			review.givenTo = .USER
        }
		review.listingId = dataList[index].carListingId
		review.carBookingId = dataList[index].id
		review.bookingUserId = dataList[index].bookingUserId
		review.businessProfileId = dataList[index].ownerId
		addReview(review: review)
    }
}

//MARK: Api Call
extension ViewBookingsAndRentalsViewController {
	
	private func getCurrentBookings() {
		self.startLoading()
		API.shared.getCurrentBookings { (status, message, datalisting) in
			self.stopLoading()
			if status {
				self.dataList = datalisting
				self.tableView.reloadData()
			} else {
				self.showAlertView(message: message, title: ALERT_TITLE_APP_NAME)
			}
		}
	}
	
	private func getIncomingBookings() {
		self.startLoading()
		API.shared.getIncomingBookingsByBookingUserId { (status, message, datalisting) in
			self.stopLoading()
			if status {
				self.dataList = datalisting
				self.tableView.reloadData()
			} else {
				self.showAlertView(message: message, title: ALERT_TITLE_APP_NAME)
			}
		}
	}
	
	private func getPastBookings() {
		self.startLoading()
		API.shared.getPastBookingsByBookingUserId { (status, message, datalisting) in
			self.stopLoading()
			if status {
				self.dataList = datalisting
				self.tableView.reloadData()
			} else {
				self.showAlertView(message: message, title: ALERT_TITLE_APP_NAME)
			}
		}
	}
	
	private func getIncomingRentals() {
		self.startLoading()
		API.shared.getIncomingRentalsByOwnerId { (status, message, datalisting) in
			self.stopLoading()
			if status {
				self.dataList = datalisting
				self.tableView.reloadData()
			} else {
				self.showAlertView(message: message, title: ALERT_TITLE_APP_NAME)
			}
		}
	}
	
	private func getCurrentRentals() {
		self.startLoading()
		API.shared.GetCurrentRentals { (status, message, datalisting) in
			self.stopLoading()
			if status {
				self.dataList = datalisting
				self.tableView.reloadData()
			} else {
				self.showAlertView(message: message, title: ALERT_TITLE_APP_NAME)
			}
		}
	}
	private func getPastRentals() {
		self.startLoading()
		API.shared.GetPastRentals { (status, message, datalisting) in
			self.stopLoading()
			if status {
				self.dataList = datalisting
				self.tableView.reloadData()
			} else {
				self.showAlertView(message: message, title: ALERT_TITLE_APP_NAME)
			}
		}
	}
	
	private func addReview(review : Review) {
		
		self.startLoading()
		API.shared.addReview(review: review) { (status, msg) in
			self.stopLoading()
			self.showAlertView(message: msg, title: ALERT_TITLE_APP_NAME, doneButtonTitle: "Ok") { alert in
				self.navigationController?.popViewController(animated: true)
			}
		}
	}
}
