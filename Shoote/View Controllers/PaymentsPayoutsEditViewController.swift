//
//  PaymentsPayoutsEditViewController.swift
//  Shoote
//
//  Created by Faheem on 25/07/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit

class PaymentsPayoutsEditViewController: BaseViewController {

	//MARK: Properties
	
	//MARK: Outlets
	
	//MARK: Override Methods
	override func viewDidLoad() {
		super.viewDidLoad()
	}
	
	
	//MARK: Private Methods

	//MARK: Actions
    @IBAction
	func actionBack(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }

}
