//
//  InboxViewController.swift
//  Shoote
//
//  Created by Faheem on 14/08/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit

class InboxViewController: BaseViewController {

	//MARK: Properties
	var conversatioList = [Conversation]()
	
	//MARK: Outlets
	@IBOutlet weak var tableView: UITableView!
	
	//MARK: Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
		self.getConversations()
		
    }
    
	//MARK: Private Methods
	func getConversations() {
		self.startLoading()
		API.shared.getConversations { status, message, list in
			self.stopLoading()
			if status {
				self.conversatioList = list
				self.tableView.reloadData()
			} else {
				self.showAlertView(message: message, title: ALERT_TITLE_APP_NAME)
			}
		}
	}
	

	//MARK: Actions
    @IBAction
	func actionBack(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
	
}

//MARK: UITableViewDataSource and UITableViewDelegate Methods
extension InboxViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return conversatioList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InboxTableViewCell") as! InboxTableViewCell
		cell.setData(conversatioList[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return UITableView.automaticDimension
    }
	
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
		let vc = UIViewController.instantiate(ChatDetailsViewController.self, fromStoryboard: .Main)
		vc.conversation = conversatioList[indexPath.row]
		self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
