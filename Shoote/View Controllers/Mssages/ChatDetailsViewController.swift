//
//  ChatDetailsViewController.swift
//  Shoote
//
//  Created by Faheem on 14/08/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit

class ChatDetailsViewController: BaseViewController {
    
	//MARK: Properties
	var conversation = Conversation()
	let mainPath = "chats"
	var isLoaded = false
	var isBusiness = false
	var conversationsManager = QuickstartConversationsManager()

	//MARK: Outlets
    @IBOutlet weak var txtFieldMsg: UITextView!
	@IBOutlet weak var lblUserName: UILabel!
	@IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var btnSendMsg: UIButton!
	@IBOutlet weak var imgMask: UIImageView!
	@IBOutlet weak var imgSend: UIImageView!
    @IBOutlet weak var tableView: UITableView!
	
	
	//MARK: Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        txtFieldMsg.delegate = self
		btnSendMsg.isUserInteractionEnabled = false
		conversationsManager.delegate = self

		// Checking if the current user a business in this chat
		if conversation.businessProfileId == Global.shared.user.businessProfileId {
			isBusiness = true
		}
		self.startLoading()
		conversationsManager.loginFromServer(isBusiness: isBusiness, conversation.conversationId) { status in
			if !status {
				self.stopLoading()
				self.showAlertView(message: "Token is not retrived from server", title: ALERT_TITLE_APP_NAME)
			}
		}
		
		// Setting up header name
		if !isBusiness {
			if !conversation.businessProfile.title.isEmpty{
				lblUserName.text = conversation.businessProfile.title.uppercased()
			} else {
				lblUserName.text = "Test User: \(conversation.businessProfileId)"
			}
			setImageWithUrl(imageView: self.imgUser, url: EndPoints.BASE_URL_IMG + conversation.businessProfile.profileImage, placeholderImage: "user")
		} else {
			if !conversation.shooteUser.firstName.isEmpty{
				lblUserName.text = conversation.shooteUser.fullName.uppercased()
			} else {
				lblUserName.text = "Test User: \(conversation.shooteUserId)"
			}
			setImageWithUrl(imageView: self.imgUser, url: EndPoints.BASE_URL_IMG + conversation.shooteUser.profileImage, placeholderImage: "user")
		}
    }
	
	//MARK: Private Methods
	private func scrollToBottomMessage(animated: Bool = true) {
		if conversationsManager.messages.count == 0 {
			return
		}
		let bottomMessageIndex = IndexPath(row: conversationsManager.messages.count - 1,
										   section: 0)
		tableView.scrollToRow(at: bottomMessageIndex, at: .bottom, animated: animated)
	}
	
	private func resetInputViews() {
		btnSendMsg.isUserInteractionEnabled = false
		imgMask.image = imgMask.image?.withRenderingMode(.alwaysOriginal)
		imgSend.image = imgSend.image?.withRenderingMode(.alwaysOriginal)
	}
	
	private func sendMessage() {
		if txtFieldMsg.text.isEmpty{
			return
		}
		conversationsManager.sendMessage(txtFieldMsg.text!, completion: { (result, _) in
			if result.isSuccessful {
				self.txtFieldMsg.text = ""
				self.txtFieldMsg.resignFirstResponder()
				self.resetInputViews()
			} else {
				self.displayErrorMessage("Unable to send message")
			}
		})
	}
	
	private func resize(textView: UITextView) {
		var newFrame = textView.frame
		let width = newFrame.size.width
		let newSize = textView.sizeThatFits(CGSize(width: width,
												   height: CGFloat.greatestFiniteMagnitude))
		newFrame.size = CGSize(width: width, height: newSize.height)
		textView.frame = newFrame
	}

	//MARK: Actions
    @IBAction
	func actionBtnSend(_ sender: UIButton) {
        sendMessage()
    }
    
    @IBAction
	func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: UITextViewDelegate Delegate Methods
extension ChatDetailsViewController: UITextViewDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		sendMessage()
        return true
    }
	
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        resize(textView: textView)
        return true
    }
	func textViewDidChange(_ textView: UITextView) {
		if textView.text.isEmpty {
			self.resetInputViews()
			return
		}
		btnSendMsg.isUserInteractionEnabled = true
		imgMask.image = imgMask.image?.withRenderingMode(.alwaysTemplate)
		imgSend.image = imgSend.image?.withRenderingMode(.alwaysTemplate)
	}
	
}

//MARK: UITableViewDelegate and UITableViewDataSource Delegate Methods
extension ChatDetailsViewController: UITableViewDelegate, UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return conversationsManager.messages.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let message = conversationsManager.messages[indexPath.row]
		let author = message.author ?? kBlankString
		if isBusiness && author == "B\(Global.shared.user.businessProfileId)"
		|| !isBusiness && author == "S\(Global.shared.user.uid)" {
			let cell = tableView.dequeueReusableCell(withIdentifier: "SendChatTableViewCell") as! SendChatTableViewCell
			cell.textMsg.text = message.body
			cell.viewBack.roundCorners([.topLeft,.bottomLeft,.bottomRight], radius: 10.0)
			cell.textTimestamp.text = Utilities.twillioDateToTime(date: message.dateCreatedAsDate)
			return cell
		} else {
			let cell = tableView.dequeueReusableCell(withIdentifier: "DriverChatTableViewCell") as! DriverChatTableViewCell
			cell.textMsg.text = message.body
			cell.viewBack.roundCorners([.topRight,.bottomLeft,.bottomRight], radius: 10.0)
			cell.textTimestamp.text = Utilities.twillioDateToTime(date: message.dateCreatedAsDate)
			return cell
		}
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return UITableView.automaticDimension
	}
}

// MARK: QuickstartConversationsManagerDelegate
extension ChatDetailsViewController: QuickstartConversationsManagerDelegate {
	func displayStatusMessage(_ statusMessage: String) {
		self.navigationItem.prompt = statusMessage
	}
	
	
	func displayErrorMessage(_ errorMessage: String) {
		let alertController = UIAlertController(title: "Error",
												message: errorMessage,
												preferredStyle: .alert)
		let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
		alertController.addAction(okAction)
		present(alertController, animated: true, completion: nil)
	}
	
	
	func reloadMessages() {
		self.stopLoading()
		self.tableView.reloadData()
		if !isLoaded {
			isLoaded = true
			scrollToBottomMessage(animated: false)
		}
	}

	// Scroll to bottom of table view for messages
	func receivedNewMessage() {
		scrollToBottomMessage()
	}
}
