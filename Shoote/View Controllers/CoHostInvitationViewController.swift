//
//  CoHostInvitationViewController.swift
//  Shoote
//
//  Created by Faheem on 25/07/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit

class CoHostInvitationViewController: BaseViewController {

	//MARK: Properties
	var email = kBlankString
	
	//MARK: Outlets
	@IBOutlet weak var lblEmail : UILabel!
	
	//MARK: Override Methods
	override func viewDidLoad() {
		super.viewDidLoad()
		lblEmail.text = email
	}
	
	
	//MARK: Private Methods

	//MARK: Actions
    @IBAction
	func actionBack(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }

}
