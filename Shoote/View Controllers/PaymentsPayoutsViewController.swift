//
//  PaymentsPayoutsViewController.swift
//  Shoote
//
//  Created by Faheem on 25/07/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit
import StripePaymentSheet

class PaymentsPayoutsViewController: BaseViewController {

	//MARK: Properties
    var paymentSheet: PaymentSheet?
    var stripePayment = StripePayment()
    var checkStatus = false
    
    //MARK: Outlets
    @IBOutlet weak var btnAddCard : UIButton!
    @IBOutlet weak var btnConnectStripe : UIButton!
    @IBOutlet weak var btnWithDraw : UIButton!
    @IBOutlet weak var lblBalance : UILabel!
	
	//MARK: Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        if Global.shared.user.payoutsEnabled {
            updateButtonToConnected(btnConnectStripe)
        }
        if Global.shared.user.paymentsEnabled {
            updateButtonToConnected(btnAddCard)
        } else {
            updatePaymentSheetVariables()
        }
        getPayoutBalance();
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if checkStatus {
            actionConnectStripe(UIButton())
        }
    }
    
	
	//MARK: Private Methods
    private func updatePaymentSheetVariables(){
        startLoading()
        API.shared.getStripeKeys { stats, message, data in
            self.stopLoading()
            if stats, let data = data {
                self.stripePayment = data
            } else {
                self.showAlertView(message: message, title: ALERT_TITLE_APP_NAME)
            }
        }
    }
    
    private func getPayoutBalance(){
        startLoading()
        API.shared.getPayoutBalance { stats, message, blnc in
            self.stopLoading()
            if stats {
                self.lblBalance.text = "$"+blnc
            } else {
                self.showAlertView(message: message, title: ALERT_TITLE_APP_NAME)
            }
        }
    }
    
    private func updateButtonToConnected(_ button: UIButton) {
        button.backgroundColor = UIColor.yellowNewColor
        button.setTitle("Connected", for: .normal)
        button.setTitleColor(UIColor(hex: "343D43"), for: .normal)
    }

	//MARK: Actions
    @IBAction
	func actionBack(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction
    func actionConnectStripe(_ sender : UIButton){
        if Global.shared.user.payoutsEnabled {
            return
        }
        startLoading()
        API.shared.getStripePayoutStatus { stats, message, isPayoutsEnabled, url in
            self.stopLoading()
            if stats {
                if isPayoutsEnabled || message.contains("Account is active") {
                    Global.shared.user.payoutsEnabled = true
                    UserDefaultsManager.shared.loggedInUserInfo = Global.shared.user
                    self.updateButtonToConnected(self.btnConnectStripe)
                } else if !self.checkStatus {
                    let vc = UIViewController.instantiate(WebViewController.self, fromStoryboard: .BecomeHostFive)
                    vc.url = url
                    vc.parentVC = self
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                self.checkStatus = false
            } else {
                self.showAlertView(message: message, title: ALERT_TITLE_APP_NAME)
            }
        }
        
    }
    
    @IBAction
    func actionWithdraw(_ sender : UIButton){
        if !Global.shared.user.payoutsEnabled {
            self.showAlertView(message: "Please connect your stripe account first.", title: ALERT_TITLE_APP_NAME)
            return
        }
        API.shared.processPayout { stats, message in
            self.stopLoading()
            if stats {
                self.showAlertView(message: "Withdrawal Successful.", title: ALERT_TITLE_APP_NAME)
            } else {
                self.showAlertView(message: message, title: ALERT_TITLE_APP_NAME)
            }
        }
    }
    
    @IBAction
    func actionAddCard(_ sender : UIButton){
        if Global.shared.user.paymentsEnabled {
            return
        }
        STPAPIClient.shared.publishableKey = stripePayment.publishableKey
        var configuration = PaymentSheet.Configuration()
        configuration.merchantDisplayName = "Shoote, Inc."
        configuration.customer = .init(id: stripePayment.customer, ephemeralKeySecret: stripePayment.ephemeralKey)
        configuration.allowsDelayedPaymentMethods = false
        self.paymentSheet = PaymentSheet(setupIntentClientSecret: stripePayment.setupIntentClientSecret, configuration: configuration)
        paymentSheet?.present(from: self) { paymentResult in
            switch paymentResult {
            case .completed:
                self.showAlertView(message: "Payment method added.", title: ALERT_TITLE_APP_NAME)
                Global.shared.user.paymentsEnabled = true
                UserDefaultsManager.shared.loggedInUserInfo = Global.shared.user
                self.updateButtonToConnected(self.btnAddCard)
            case .canceled:
                self.showAlertView(message: "Process Canceled. Try Again.", title: ALERT_TITLE_APP_NAME)
            case .failed(let error):
                print(error)
                self.showAlertView(message: "Error while adding card. Try again.", title: ALERT_TITLE_APP_NAME)
            }
          }
    }

}
