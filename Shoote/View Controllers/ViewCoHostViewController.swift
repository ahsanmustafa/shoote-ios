//
//  ViewCoHostViewController.swift
//  Shoote
//
//  Created by Faheem on 25/07/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit

class ViewCoHostViewController: BaseViewController {

	//MARK: Properties
	var coHosts = [ShooteUser]()
	
	//MARK: Outlets
	@IBOutlet weak var tableView : UITableView!
	@IBOutlet weak private var tableHeightConstraint : NSLayoutConstraint!
	@IBOutlet weak var txtFName : UITextField!
	@IBOutlet weak var txtLName : UITextField!
	@IBOutlet weak var txtEmail : UITextField!
	@IBOutlet weak var txtPassword : UITextField!
	@IBOutlet weak var txtCPassword : UITextField!
	@IBOutlet weak var txtDateOfBirth : UITextField!
	@IBOutlet weak var txtPhone : UITextField!
	
	//MARK: Override Methods
	override func viewDidLoad() {
		super.viewDidLoad()
		self.txtDateOfBirth.setInputViewDatePicker(target: self, selector: #selector(txtDateOfBirth(sender:datePicker1:)))
		#if DEBUG
		txtFName.text = "Muhammad"
		txtLName.text = "Akram"
		txtEmail.text = "muhammadakram9544@gmail.com"
		txtPassword.text = "emnaqash"
		txtCPassword.text = "emnaqash"
		txtDateOfBirth.text = "04/16/94"
		txtPhone.text = "+923015857307"
		#endif
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		self.getCoHosts()
	}
	
	
	//MARK: Private Methods
	
	private func checkValidations() -> Bool {
		var isValid = true
		
		if self.txtFName.text!.isEmpty {
			isValid = false
			self.showAlertView(message: "Enter First Name",title: ALERT_TITLE_APP_NAME)
		} else if self.txtLName.text!.isEmpty {
			isValid = false
			self.showAlertView(message: "Enter Last Name",title: ALERT_TITLE_APP_NAME)
		} else if self.txtEmail.text!.isEmpty {
			isValid = false
			self.showAlertView(message: "Enter Email",title: ALERT_TITLE_APP_NAME)
		} else if self.txtPassword.text!.isEmpty {
			isValid = false
			self.showAlertView(message: "Enter Password",title: ALERT_TITLE_APP_NAME)
		} else if self.txtCPassword.text!.isEmpty {
			isValid = false
			self.showAlertView(message: "Enter Confirm Password",title: ALERT_TITLE_APP_NAME)
		} else if self.txtDateOfBirth.text!.isEmpty {
			isValid = false
			self.showAlertView(message: "Enter Date of birth",title: ALERT_TITLE_APP_NAME)
		} else if self.txtPhone.text!.isEmpty {
			self.showAlertView(message: "Enter Phone Number",title: ALERT_TITLE_APP_NAME)
			isValid = false
		} else if !Utilities.isValidEmail(self.txtEmail.text!) {
			self.showAlertView(message: "Email is not valid",title: ALERT_TITLE_APP_NAME)
			isValid = false
		} else if self.txtPassword.text! != self.txtCPassword.text! {
			self.showAlertView(message: "Passwords don't match.",title: ALERT_TITLE_APP_NAME)
			isValid = false
		}
		
		return isValid
	}
	
	private func clearFields() {
		txtFName.text = ""
		txtLName.text = ""
		txtEmail.text = ""
		txtPassword.text = ""
		txtCPassword.text = ""
		txtDateOfBirth.text = ""
		txtPhone.text = ""
	}

	//MARK: Actions
    @IBAction
	func actionBack(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
	
	@IBAction
	func actionAddCohost(_ sender: UIButton){
		if checkValidations() {
			if checkInternetConnection() {
				let params:ParamsString = ["FirstName":self.txtFName.text ?? kBlankString,
										"LastName":self.txtLName.text ?? kBlankString,
										"Email":self.txtEmail.text ?? kBlankString,
										"Password":self.txtPassword.text ?? kBlankString,
										"Dob":self.txtDateOfBirth.text ?? kBlankString,
										"PrimaryContact":self.txtPhone.text ?? kBlankString]
				self.addCoHost(param: params)
			}
		}
	}
	
	@objc
	func txtDateOfBirth(sender: Any, datePicker1: UIDatePicker) {
		
		if let datePicker = self.txtDateOfBirth.inputView as? UIDatePicker {
			let dateformatter = DateFormatter()
			dateformatter.dateFormat = "MM/dd/yy"
			self.txtDateOfBirth.text = dateformatter.string(from: datePicker.date)
		}
		self.view.endEditing(true)
	}

}

//MARK: API Calls
extension ViewCoHostViewController {
	
	private func getCoHosts() {
		startLoading()
		API.shared.getCoHosts { stats, message, list in
			self.stopLoading()
			if stats {
				self.coHosts = list
				self.tableView.reloadData()
				self.tableView.layoutIfNeeded()
				self.tableHeightConstraint.constant = self.tableView.contentSize.height
				self.view.layoutIfNeeded()
			} else {
				self.showAlertView(message: message, title: ALERT_TITLE_APP_NAME)
			}
		}
	}
	
	private func addCoHost(param: ParamsAny) {
		startLoading()
		API.shared.addCohost(params: param) { message, status in
			self.stopLoading()
			if status {
				let vc = UIViewController.instantiate(CoHostInvitationViewController.self, fromStoryboard: .Setting)
				vc.email = self.txtEmail.text ?? kBlankString
				self.navigationController?.pushViewController(vc, animated: true)
				self.clearFields()
			} else {
				self.showAlertView(message: message, title: ALERT_TITLE_APP_NAME)
			}
		}
	}
	
}

//MARK: UITableViewDataSource Methods
extension ViewCoHostViewController: UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "CoHostTableViewCell") as! CoHostTableViewCell
		cell.setData(user: self.coHosts[indexPath.row], index: indexPath.row)
		return cell
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return coHosts.count
	}
	
}

//MARK: UITableViewDelegate Methods
extension ViewCoHostViewController: UITableViewDelegate {
	
}
