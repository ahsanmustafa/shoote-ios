//
//  RequestsSentViewController.swift
//  Shoote
//
//  Created by BrainX IOS 3 on 20/12/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit

class RequestSentViewController: BaseViewController {
	
	//MARK: Properties
	var listRequestSent = [CarBooking]()
	
	//MARK: Outlets
	@IBOutlet weak var tableView : UITableView!
	
	//MARK: Override Methods
	override func viewDidLoad() {
		super.viewDidLoad()
		getALlRequests()
	}

	//MARK: Private Methods
	private func getALlRequests() {
		self.startLoading()
		API.shared.getSentRequests { (status, message, dataList) in
			self.stopLoading()
			if status {
				if dataList.count > 0 {
					self.listRequestSent = dataList
					self.tableView.reloadData()
				} else {
					self.showAlertView(message: "No Data Found", title: ALERT_TITLE_APP_NAME)
				}
			} else {
				self.showAlertView(message: message, title: ALERT_TITLE_APP_NAME)
			}
		}
	}
	
	//MARK: Actions
	@IBAction
	func actionBack(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
	@objc
	func imageTapped(_ tap: UITapGestureRecognizer) {
		let vc = UIViewController.instantiate(ViewUserProfileViewController.self, fromStoryboard: .CreateService)
		vc.userType = "host"
		vc.id = listRequestSent[tap.view?.tag ?? 0].ownerId
		self.navigationController?.pushViewController(vc, animated: true)
	}
	
}

//MARK: UITableViewDelegate and UITableViewDataSource
extension RequestSentViewController : UITableViewDelegate , UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return listRequestSent.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "RequestSentTableViewCell") as! RequestSentTableViewCell
		cell.setData(listRequestSent[indexPath.row])
		let tap = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped(_:)))
		cell.imgOwner.addGestureRecognizer(tap)
		cell.imgOwner.tag = indexPath.row
		return cell
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 345
	}
	
	
}
