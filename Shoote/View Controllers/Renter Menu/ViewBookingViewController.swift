//
//  ViewBookingViewController.swift
//  Shoote
//
//  Created by BrainX IOS 3 on 16/09/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit
import Cosmos

class ViewBookingViewController: BaseViewController {

	//MARK: Properties
	var data = CarBooking()
	var type: ViewBookingsAndRentals?
	var userProfile: ShooteUser?
	
	//MARK: Outlets
	@IBOutlet weak var lblheading : UILabel!
    @IBOutlet weak var lblBookingId : UILabel!
    @IBOutlet weak var imgCar : UIImageView!
    @IBOutlet weak var lblCarName : UILabel!
    @IBOutlet weak var lblCarLocation : UILabel!
    @IBOutlet weak var imgProfile : UIImageView!
    @IBOutlet weak var lblUserName : UILabel!
    @IBOutlet weak var viewRating : CosmosView!
    @IBOutlet weak var lblBookingType : UILabel!
	@IBOutlet weak var lblCarUsedFor : UILabel!
	@IBOutlet weak var lblCarDriven : UILabel!
	@IBOutlet weak var lblNoPeople : UILabel!
    @IBOutlet weak var lblAddress : UILabel!
    @IBOutlet weak var lblStartTime : UILabel!
    @IBOutlet weak var lblEndTime : UILabel!
    @IBOutlet weak var lblHourlyTotal : UILabel!
    @IBOutlet weak var lblDelieveryFee : UILabel!
    @IBOutlet weak var lblProcessingFee : UILabel!
    @IBOutlet weak var lblGrandTotal : UILabel!
    @IBOutlet weak var btnStatus : UIButton!
	@IBOutlet weak var btnContactHost : UIButton!
	@IBOutlet weak var btnContactRentals : UIButton!
    
	//MARK: Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setData()
    }
    
	//MARK: Private Methods
    private func setData() {
		lblCarName.text = data.carListing.name
        getRenterData(id: data.bookingUserId)
		let path = EndPoints.BASE_URL_IMG + (data.carListing.listingImages.first?.imagePath ?? kBlankString)
        
        setImageWithUrl(imageView: imgCar, url: path,placeholderImage: "img_car_placeholder")
        lblStartTime.text = data.startDateUserTimeZome
        lblEndTime.text = data.endDateUserTimeZone
		lblGrandTotal.text = "$\(data.totalFee.withCommas())"
        lblBookingType.text = data.typeOfBooking
		if data.userDrive == 1 {
			lblCarDriven.text = "Yes"
		} else {
			lblCarDriven.text = "No"
		}
		lblCarUsedFor.text = data.description
		lblNoPeople.text = data.noOfPassengers.toString()
		lblCarLocation.text = data.carListing.location.city
		lblAddress.text = data.locationType+" - "+data.location.address+", "+data.location.city+", "+data.location.state+" "+data.location.zip
		lblHourlyTotal.text = "$\(data.hourlyTotal.withCommas())"
		lblProcessingFee.text = "$\(data.processingFee.withCommas())"
		let deliveryFee = data.additionalFees.filter({($0.type ) == "Delivery" }).first?.amount ?? 0
		lblDelieveryFee.text = "$\(deliveryFee.withCommas())"
		lblBookingId.text = data.id.toString()
		guard let type = self.type else {
			btnStatus.setTitle("Request", for: .normal)
			btnStatus.backgroundColor = UIColor(hexFromString: "8219EB")
			lblheading.text = "Rental Details"
			btnContactHost.isHidden = true
			return
		}
		if type == .incommingBookings || type == .currentBookings || type == .pastBookings {
			lblheading.text = "Booking Details"
			btnContactRentals.isHidden = true
		} else {
			lblheading.text = "Rental Details"
			btnContactHost.isHidden = true
		}
        switch type {
        case .incommingBookings, .incomingRentals:
            btnStatus.setTitle("Pending", for: .normal)
            btnStatus.backgroundColor = UIColor(hexFromString: "8219EB")
            
		case .currentBookings, .currentRentals:
            btnStatus.setTitle("Active", for: .normal)
            btnStatus.backgroundColor = UIColor(hexFromString: "E39B00")
		case .pastBookings, .pastRentals:
            // Check whether expired or completed
            if data.rideDateTime.isEmpty {
                btnStatus.setTitle("Expired", for: .normal)
                btnStatus.backgroundColor = UIColor(hexFromString: "FF1D1D")
            } else {
                btnStatus.setTitle("Complete", for: .normal)
                btnStatus.backgroundColor = UIColor(hexFromString: "1DBB17")
            }
        }
        
    }
	
    private func getRenterData(id : Int) {
        API.shared.getShooteUserById(id: id) { (status, msg, dt) in
            if let dt = dt {
				self.userProfile = dt
				self.lblUserName.text = dt.fullName
				self.viewRating.rating = dt.ratingAvg.toDouble()
				let url = EndPoints.BASE_URL_IMG + dt.profileImage
				self.setImageWithUrl(imageView: self.imgProfile, url: url,placeholderImage: "img_car_placeholder")
            }
        }
    }
    
	//MARK: Actions
    @IBAction
	func actionBack(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
	
	@IBAction
	func actionContactHost(_ sender : UIButton){
		guard let profile = userProfile else {
			showAlertView(message: "Loading Profile in Background.", title: ALERT_TITLE_APP_NAME)
			return
		}
		self.startLoading()
		API.shared.openConversation(id: profile.id) { status, message, conversation in
			self.stopLoading()
			if status {
				let vc = UIViewController.instantiate(ChatDetailsViewController.self, fromStoryboard: .Main)
				vc.conversation = conversation
				self.navigationController?.pushViewController(vc, animated: true)
			} else {
				self.showAlertView(message: message, title: ALERT_TITLE_APP_NAME)
			}
		}
	}
}
