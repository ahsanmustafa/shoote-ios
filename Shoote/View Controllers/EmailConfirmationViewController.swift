//
//  EmailConfirmationViewController.swift
//  Shoote
//
//  Created by Faheem on 25/07/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit

class EmailConfirmationViewController: BaseViewController {

	//MARK: Outlets
    @IBOutlet weak var btnEmail : UIButton!
    @IBOutlet weak var btnPhone : UIButton!
    @IBOutlet weak var btnSend : UIButton!
    @IBOutlet weak var lblMsg : UILabel!
    @IBOutlet weak var txtPhone : UITextField!
    
	//MARK: Override Functions
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
	//MARK: Actions
    @IBAction
	func actionSend(_ sender : UIButton){
           self.navigationController?.popToRootViewController(animated: true)
    }
	
    @IBAction
	func actionBack(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
	
    @IBAction
	func actionPhoneConfirmation(_ sender : UIButton){
        self.btnPhone.backgroundColor = UIColor.init(hexFromString: "EAA215")
        self.btnEmail.backgroundColor = UIColor.init(hexFromString: "EAA215", alpha: 0.23)
        self.btnSend.setTitle("Send Text", for: .normal)
        self.txtPhone.placeholder = "363636"
        self.lblMsg.text = "A confirmation of your booking will be sent to your following number. "
    }
	
    @IBAction
	func actionEmailConfirmation(_ sender : UIButton){
        self.btnEmail.backgroundColor = UIColor.init(hexFromString: "EAA215")
        self.btnPhone.backgroundColor = UIColor.init(hexFromString: "EAA215", alpha: 0.23)
        self.btnSend.setTitle("Send Email", for: .normal)
        self.txtPhone.placeholder = "ahsan@gmail.com"
        self.lblMsg.text = "A confirmation of your booking will be sent to your email, please enter the email you wish to send to following email address."
    }

}
