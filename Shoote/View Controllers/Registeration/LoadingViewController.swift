//
//  LoadingViewController.swift
//  Shoote
//
//  Created by Faheem on 13/07/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit
import SVProgressHUD

class LoadingViewController: BaseViewController {
    
    //MARK: Properties
    var isFromRegisterScreen = false
    var isFromLoginSCreen = false
    var isFromForgotPasswordScreen = false
    var userName = ""
    var params : ParamsAny = [:]
    var loadingView : UIView!
    
	//MARK: Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
		//SVProgressHUD code Circle
		loadingView = UIView(frame: view.frame)
		loadingView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
		view.addSubview(loadingView)
		loadingView.isHidden = true
		
        if isFromLoginSCreen {
            loginUser(with: params)
        } else if isFromRegisterScreen {
            registerUser(with: params)
        } else if isFromForgotPasswordScreen {
            forgotPassword(with: params)
        }
    }
    
    //MARK: - Private Methods
    private func MoveToNext(){
		let vc = UIViewController.instantiate(SignUpConfirmationViewController.self, fromStoryboard: .Registration)
        vc.userName = self.userName
        self.navigationController?.pushViewController(vc, animated: true)
    }
	
    private func MoveToHome(){
		let vc = UIViewController.instantiate(HomeViewController.self, fromStoryboard: .Main)
        self.navigationController?.setViewControllers([vc], animated: true)
    }
}

//MARK: - API CALL
extension LoadingViewController {
	
	private func registerUser(with params:ParamsAny) {
		self.startLoading()
		API.shared.signup(params: params) { (message, status) in
			self.stopLoading()
			if status {
				self.showAlertView(message: message, title: ALERT_TITLE_APP_NAME, doneButtonCompletion: { ac in
					self.navigationController?.popToRootViewController(animated: true)
				})
			} else {
				self.showAlertView(message: message, title: ALERT_TITLE_APP_NAME, doneButtonCompletion: { ac in
					self.navigationController?.popViewController(animated: true)
				})
			}
		}
	}
	
	private func loginUser(with params:ParamsAny) {
		self.startLoading()
		API.shared.login(params: params) { (message, status) in
			self.stopLoading()
			if status {
				self.MoveToHome()
			} else {
				self.showAlertView(message: message, title: ALERT_TITLE_APP_NAME, doneButtonTitle: "Ok") { alert in
					self.navigationController?.popViewController(animated: true)
				}
			}
		}
	}
	
	private func forgotPassword(with params:ParamsAny) {
		self.startLoading()
		API.shared.forgotPassword(email: params["Email"] as! String) { (message, status) in
			DispatchQueue.main.async {
				self.stopLoading()
				if status
				{
					self.showAlertView(message: message, title: ALERT_TITLE_APP_NAME, doneButtonCompletion: { ac in
						self.navigationController?.popToRootViewController(animated: true)
					})
				}
				else
				{
					self.showAlertView(message: message, title: ALERT_TITLE_APP_NAME, doneButtonCompletion: { ac in
						self.navigationController?.popViewController(animated: true)
					})
				}
			}
		}
	}
	
}
