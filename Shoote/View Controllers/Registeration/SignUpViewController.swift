//
//  SignUpViewController.swift
//  Shoote
//
//  Created by Faheem on 19/07/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit

class SignUpViewController: BaseViewController {

    //MARK: Outlets
    @IBOutlet weak var txtFName : UITextField!
    @IBOutlet weak var txtLName : UITextField!
    @IBOutlet weak var txtEmail : UITextField!
    @IBOutlet weak var txtPassword : UITextField!
    @IBOutlet weak var txtCPassword : UITextField!
    @IBOutlet weak var txtDateOfBirth : UITextField!
    @IBOutlet weak var txtPhone : UITextField!
    
    //MARK: Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtDateOfBirth.setInputViewDatePicker(target: self, selector: #selector(txtDateOfBirth(sender:datePicker1:)))
		#if DEBUG
		//txtEmail.text = "f158116@nu.edu.pk"
		txtFName.text = "Faheem"
		txtLName.text = "Akram"
		txtEmail.text = "faheemakram920@gmail.com"
		txtPassword.text = "emnaqash"
		txtCPassword.text = "emnaqash"
		txtDateOfBirth.text = "04/16/94"
		txtPhone.text = "+923015857307"
		#endif
    }
    
	//MARK: - Private Methods
	private func MoveToNext(){
		let vc = UIViewController.instantiate(SignUpConfirmationViewController.self, fromStoryboard: .Registration)
		vc.userName = self.txtFName.text!
		 self.navigationController?.pushViewController(vc, animated: true)
	}
	
	private func checkValidations() -> Bool {
		var isValid = true
		
		if self.txtFName.text!.isEmpty {
			isValid = false
			self.showAlertView(message: "Enter First Name",title: ALERT_TITLE_APP_NAME)
		} else if self.txtLName.text!.isEmpty {
			isValid = false
			self.showAlertView(message: "Enter Last Name",title: ALERT_TITLE_APP_NAME)
		} else if self.txtEmail.text!.isEmpty {
			isValid = false
			self.showAlertView(message: "Enter Your Email",title: ALERT_TITLE_APP_NAME)
		} else if self.txtPassword.text!.isEmpty {
			isValid = false
			self.showAlertView(message: "Enter Your Password",title: ALERT_TITLE_APP_NAME)
		} else if self.txtCPassword.text!.isEmpty {
			isValid = false
			self.showAlertView(message: "Enter Confirm Password",title: ALERT_TITLE_APP_NAME)
		} else if self.txtDateOfBirth.text!.isEmpty {
			isValid = false
			self.showAlertView(message: "Enter Your Date of birth",title: ALERT_TITLE_APP_NAME)
		} else if self.txtPhone.text!.isEmpty {
			self.showAlertView(message: "Enter Phone Number",title: ALERT_TITLE_APP_NAME)
			isValid = false
		} else if !Utilities.isValidEmail(self.txtEmail.text!) {
			self.showAlertView(message: "Email is not valid",title: ALERT_TITLE_APP_NAME)
			isValid = false
		} else if self.txtPassword.text! != self.txtCPassword.text! {
			self.showAlertView(message: "Passwords don't match.",title: ALERT_TITLE_APP_NAME)
			isValid = false
		}
		
		return isValid
	}
	
    //MARK: Actions
    @IBAction
	func actionSignUp(_ sender: UIButton){
        if checkValidations() {
            if checkInternetConnection() {
                let params:ParamsString = ["FirstName":self.txtFName.text!,
                                        "LastName":self.txtLName.text!,
                                        "Email":self.txtEmail.text!,
                                        "Password":self.txtPassword.text!,
                                        "Dob":self.txtDateOfBirth.text!,
                                        "PrimaryContact":self.txtPhone.text!]
				let user: ParamsAny = ["ShooteUsers" : [params]]
				let vc = UIViewController.instantiate(LoadingViewController.self, fromStoryboard: .Registration)
                vc.userName = self.txtFName.text!
                vc.params = user
                vc.isFromRegisterScreen = true
                self.navigationController?.pushViewController(vc, animated: false)
            }
        }
    }
    
    @IBAction
	func actionBack(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
	
	@objc
	func txtDateOfBirth(sender: Any, datePicker1: UIDatePicker) {
		
		if let datePicker = self.txtDateOfBirth.inputView as? UIDatePicker {
			let dateformatter = DateFormatter()
			dateformatter.dateFormat = "MM/dd/yy"
			self.txtDateOfBirth.text = dateformatter.string(from: datePicker.date)
		}
		self.view.endEditing(true)
	}
}

