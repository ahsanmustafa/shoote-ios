//
//  SignUpConfirmationViewController.swift
//  Shoote
//
//  Created by Faheem on 13/07/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit

class SignUpConfirmationViewController: BaseViewController {

    //MARK: Outlets
    @IBOutlet weak var lblMessage : UILabel!
    
    //MARK: Properties
    var userName = ""
	
    //MARK: - Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblMessage.text = String(format: welcomeMessage, userName)
    }

	//MARK: - Action
    @IBAction
	func actionBack(_ sender : UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
}
