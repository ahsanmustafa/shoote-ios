//
//  ForgotPasswordVC.swift
//  Shoote
//
//  Created by Muhammad Ansaf on 10/11/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit
import SVProgressHUD

class ForgotPasswordVC: BaseViewController {
    
	
	//MARK: - Properties
	var loadingView : UIView!
	
    //MARK: Outlets
    @IBOutlet weak var txtEmail : UITextField!
    
	//MARK: Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadingView = UIView(frame: view.frame)
        loadingView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        view.addSubview(loadingView)
        loadingView.isHidden = true
    }
	
	//MARK: Private Methods
	func checkValidations() -> Bool {
		var isValid = true
		
		if self.txtEmail.text!.isEmpty {
			isValid = false
			self.showAlertView(message: "Enter Your Email",title: ALERT_TITLE_APP_NAME)
		}
		
		return isValid
	}
    
    //MARK: Actions
    @IBAction
	func actionForgotPassword(_ sender: UIButton){
        if checkValidations() {
            if checkInternetConnection() {
                let params:ParamsString = ["Email":self.txtEmail.text!]
				let vc = UIViewController.instantiate(LoadingViewController.self, fromStoryboard: .Registration)
                //vc.userName = self.txtFName.text!
                vc.params = params
                vc.isFromForgotPasswordScreen = true
                self.navigationController?.pushViewController(vc, animated: false)
            }
        }
    }
    
    @IBAction
	func actionBack(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
}

