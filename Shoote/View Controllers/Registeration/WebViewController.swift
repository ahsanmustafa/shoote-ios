//
//  WebViewController.swift
//  Shoote
//
//  Created by Faheem on 07/12/2022.
//  Copyright © 2022 Digi Dev. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: BaseViewController {
    
    @IBOutlet weak var webview:WKWebView!
    
    var url: String = ""
    var parentVC: PaymentsPayoutsViewController?

    override func viewDidLoad() {
        super.viewDidLoad()

        webview.uiDelegate = self
        webview.navigationDelegate = self
        if #available(iOS 14.0, *) {
            webview.configuration.defaultWebpagePreferences.allowsContentJavaScript = true
        }
        webview.configuration.preferences.javaScriptEnabled = true
        if let stripe = URL(string: url) {
            webview.load(URLRequest(url: stripe))
        }
         
    }
    
    
    @IBAction
    func actionBack(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
                         

}
extension WebViewController: WKNavigationDelegate, WKUIDelegate {
                
    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if let url = navigationAction.request.url {
            print("did decide",url)
            if url.absoluteString.contains("https://shooteapi.azurewebsites.net/Payment") {
                self.parentVC?.checkStatus = true
                self.navigationController?.popViewController(animated: true)
            }
        }
        
        decisionHandler(.allow)
    }
    
    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
    }
    public func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
    }
}
