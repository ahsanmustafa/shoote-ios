//
//  LoginViewController.swift
//  Shoote
//
//  Created by Faheem on 19/07/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit
import SVProgressHUD

class LoginViewController: BaseViewController {
    
    //MARK: Outlets
    @IBOutlet weak var txtEmail : UITextField!
    @IBOutlet weak var txtPassword : UITextField!
	
    //MARK: Properties
    var loadingView : UIView!
    
	//MARK: Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        //SVProgressHUD code Circle
        loadingView = UIView(frame: view.frame)
        loadingView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        view.addSubview(loadingView)
        loadingView.isHidden = true
        
        #if DEBUG
        //txtEmail.text = "f158116@nu.edu.pk"
		txtEmail.text = "testdevshoote@gmail.com"
        //txtEmail.text = "iahsanmustafa@gmail.com"
        //txtPassword.text = "123456"
		txtPassword.text = "123456"
        #endif
    }
	
	//MARK: Private Methods
	private func MoveToNext() {
		let vc = UIViewController.instantiate(HomeViewController.self, fromStoryboard: .Main)
		self.navigationController?.setViewControllers([vc], animated: true)
	}
	
	private func checkValidations() -> Bool {
		var isValid = true
		if self.txtEmail.text!.isEmpty {
			isValid = false
			self.showAlertView(message: "Enter Your Email",title: ALERT_TITLE_APP_NAME)
		}
		else if self.txtPassword.text!.isEmpty {
			isValid = false
			self.showAlertView(message: "Enter Your Password",title: ALERT_TITLE_APP_NAME)
		}
		return isValid
	}
    
    //MARK: Action
    @IBAction
	func actionLogin(_ sender: UIButton) {
        if checkValidations() {
            if checkInternetConnection() {
                let params:ParamsString = ["username":self.txtEmail.text!,
                                        "password":self.txtPassword.text!,
                                        "grant_type":"password",
                                        "device":"ios"]
				let vc = UIViewController.instantiate(LoadingViewController.self, fromStoryboard: .Registration)
                vc.params = params
                vc.isFromLoginSCreen = true
                self.navigationController?.pushViewController(vc, animated: false)
            }
        }
    }
    
    @IBAction
	func actionSignUp(_ sender: UIButton) {
		let vc = UIViewController.instantiate(SignUpViewController.self, fromStoryboard: .Registration)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction
	func actionForgotPassword(_ sender: UIButton) {
		let vc = UIViewController.instantiate(ForgotPasswordVC.self, fromStoryboard: .Registration)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

