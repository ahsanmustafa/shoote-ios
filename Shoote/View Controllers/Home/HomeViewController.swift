//
//  HomeViewController.swift
//  Shoote
//
//  Created by Faheem on 13/07/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit

class HomeViewController: BaseViewController {

	//MARK: Outlets
    @IBOutlet weak var imgNearByCars : UIImageView!
    @IBOutlet weak var imgLocation : UIImageView!
    @IBOutlet weak var imgModels : UIImageView!
    @IBOutlet weak var carsCollectionView : UICollectionView!
    @IBOutlet weak var carsCollectionViewHeightConstraint : NSLayoutConstraint!
    @IBOutlet weak var lblSearch : UILabel!
    @IBOutlet weak var lblSaved : UILabel!
    @IBOutlet weak var lblInbox : UILabel!
    @IBOutlet weak var lblBookings : UILabel!
    @IBOutlet weak var lblProfile : UILabel!
    
    //MARK: Properties
    var carListing = [CarListing]()
	
	//MARK: Override Functions
    override func viewDidLoad() {
        super.viewDidLoad()
		updateViews()
        getListingsAPI()
        self.carsCollectionViewHeightConstraint.constant = 0
    }
	
	//MARK: Private Methds
	
	private func updateViews() {
		let tap = UITapGestureRecognizer(target: self, action: #selector(carsTapped(_:)))
		self.imgNearByCars.addGestureRecognizer(tap)
		
		let tap1 = UITapGestureRecognizer(target: self, action: #selector(locationTapped(_:)))
		self.imgLocation.addGestureRecognizer(tap1)
		
		let tap2 = UITapGestureRecognizer(target: self, action: #selector(modelsTapped(_:)))
		self.imgModels.addGestureRecognizer(tap2)
        
        lblSearch.isHidden = false
        lblSaved.isHidden = true
        lblInbox.isHidden = true
        lblBookings.isHidden = true
        lblProfile.isHidden = true
	}
    
    private func getListingsAPI() {
        //self.startLoading()
        API.shared.getCarListings() { (status, message, list) in
            //self.stopLoading()
            if let info = list {
                self.carListing = info.data
                self.carsCollectionView.reloadData()
                self.carsCollectionViewHeightConstraint.constant = CGFloat(min(self.carListing.count, 3) * 275)
            } else {
                self.showAlertView(message: message, title: ALERT_TITLE_APP_NAME)
            }
        }
    }
	
	//MARK: Actions
	
    @objc
	func carsTapped(_ sender : UITapGestureRecognizer){
		let vc = UIViewController.instantiate(CarsNearViewController.self, fromStoryboard: .Main)
		self.navigationController?.pushViewController(vc, animated: true)
    }
	
    @objc
	func locationTapped(_ sender : UITapGestureRecognizer){
        self.performSegue(withIdentifier: "commingLocation", sender: self)
    }
	
    @objc
	func modelsTapped(_ sender : UITapGestureRecognizer){
        self.performSegue(withIdentifier: "comingModels", sender: self)
    }
	
    @IBAction
	func listYourService(_ sender : UIButton){
		
		let vc = UIViewController.instantiate(ListServicesViewController.self, fromStoryboard: .CreateService)
        self.navigationController?.pushViewController(vc, animated: true)
    }
	
    @IBAction
	func actionOpenSetting(_ sender : UIButton){
		
		let vc = UIViewController.instantiate(SettingViewController.self, fromStoryboard: .Setting)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction
    func actionSearchTapped(_ sender : UIButton){
        print("search_tapped")
        UIView.animate(withDuration: 0.75, animations: {
            // animation
            self.lblSearch.isHidden = false
            self.lblSaved.isHidden = true
            self.lblInbox.isHidden = true
            self.lblBookings.isHidden = true
            self.lblProfile.isHidden = true
        }, completion: { finished in
            // completion
            self.lblSearch.isHidden = false
            self.lblSaved.isHidden = true
            self.lblInbox.isHidden = true
            self.lblBookings.isHidden = true
            self.lblProfile.isHidden = true
        })
    }
    
    @IBAction
    func actionSavedTapped(_ sender : UIButton){
        UIView.animate(withDuration: 0.75, animations: {
            // animation
            self.lblSearch.isHidden = true
            self.lblSaved.isHidden = false
            self.lblInbox.isHidden = true
            self.lblBookings.isHidden = true
            self.lblProfile.isHidden = true
        }, completion: { finished in
            // completion
            self.lblSearch.isHidden = true
            self.lblSaved.isHidden = false
            self.lblInbox.isHidden = true
            self.lblBookings.isHidden = true
            self.lblProfile.isHidden = true
        })
    }
    
    @IBAction
    func actionInboxTapped(_ sender : UIButton){
        UIView.animate(withDuration: 0.75, animations: {
            self.lblSearch.isHidden = true
            self.lblSaved.isHidden = true
            self.lblInbox.isHidden = false
            self.lblBookings.isHidden = true
            self.lblProfile.isHidden = true
        }, completion: { finished in
            self.lblSearch.isHidden = true
            self.lblSaved.isHidden = true
            self.lblInbox.isHidden = false
            self.lblBookings.isHidden = true
            self.lblProfile.isHidden = true
        })
    }
    
    @IBAction
    func actionBookingsTapped(_ sender : UIButton){
        UIView.animate(withDuration: 0.75, animations: {
            self.lblSearch.isHidden = true
            self.lblSaved.isHidden = true
            self.lblInbox.isHidden = true
            self.lblBookings.isHidden = false
            self.lblProfile.isHidden = true
        }, completion: { finished in
            self.lblSearch.isHidden = true
            self.lblSaved.isHidden = true
            self.lblInbox.isHidden = true
            self.lblBookings.isHidden = false
            self.lblProfile.isHidden = true
        })
    }
    
    @IBAction
    func actionProfileTapped(_ sender : UIButton){
        UIView.animate(withDuration: 0.75, animations: {
            self.lblSearch.isHidden = true
            self.lblSaved.isHidden = true
            self.lblInbox.isHidden = true
            self.lblBookings.isHidden = true
            self.lblProfile.isHidden = false
        }, completion: { finished in
            self.lblSearch.isHidden = true
            self.lblSaved.isHidden = true
            self.lblInbox.isHidden = true
            self.lblBookings.isHidden = true
            self.lblProfile.isHidden = false
        })
    }
}

//MARK: UICollectionViewDelegate and UICollectionViewDataSource Methods
extension HomeViewController : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return min(carListing.count, 3)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifier.CarsCollectionViewCell, for: indexPath) as! CarsCollectionViewCell
        cell.setData(carListing[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: (collectionView.frame.width) - 60, height: 275)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = UIViewController.instantiate(CarDetailsViewController.self, fromStoryboard: .CarDetails)
        vc.carListing = carListing[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
