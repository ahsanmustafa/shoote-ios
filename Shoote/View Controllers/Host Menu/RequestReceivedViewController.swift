//
//  RequestReceivedViewController.swift
//  Shoote
//
//  Created by BrainX IOS 3 on 20/12/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit

class RequestReceivedViewController: BaseViewController {

	//MARK: Outlets
	@IBOutlet weak var tableView : UITableView!
	
	//MARK: Properties
	var listRequestSent = [CarBooking]()
	
	//MARK: Override Methods
	override func viewDidLoad() {
		super.viewDidLoad()
		getALlRequests()
	}

	//MARK: Api Call
	private func getALlRequests() {
		self.startLoading()
		API.shared.getReceivedRequests { (status, message, dataList) in
			self.stopLoading()
			if status {
				if dataList.count > 0 {
					self.listRequestSent = dataList
					self.tableView.reloadData()
				} else {
					self.showAlertView(message: "No Data Found", title: ALERT_TITLE_APP_NAME)
				}
			} else {
				self.showAlertView(message: message, title: ALERT_TITLE_APP_NAME)
			}
		}
	}
	
	private func updateStatus(id : Int , status : Bool) {
		startLoading()
		API.shared.approveOrUnApproveBooking(Id: id, SetStatus: status) { (message, status) in
			self.stopLoading()
			self.showAlertView(message: message, title: ALERT_TITLE_APP_NAME, doneButtonTitle: "OK") { (alert) in
				self.navigationController?.popViewController(animated: true)
			}
		}
	}
	
	//MARK: Actions
	@IBAction
	func actionBack(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction
	func actionAccept(_ sender: UIButton) {
		updateStatus(id: listRequestSent[sender.tag].id, status: true)
	}
	
	@IBAction
	func actionReject(_ sender: UIButton) {
		updateStatus(id: listRequestSent[sender.tag].id, status: false)
	}
	
	@IBAction
	func actionDetails(_ sender: UIButton) {
		let vc = UIViewController.instantiate(ViewBookingViewController.self, fromStoryboard: .CreateService)
		vc.data = listRequestSent[sender.tag]
		self.navigationController?.pushViewController(vc, animated: true)
	}
	
	@objc
	func imageTapped(_ tap: UITapGestureRecognizer) {
		let vc = UIViewController.instantiate(ViewUserProfileViewController.self, fromStoryboard: .CreateService)
		vc.userType = "renter"
		vc.id = listRequestSent[tap.view?.tag ?? 0].bookingUserId
		self.navigationController?.pushViewController(vc, animated: true)
	}
}

//MARK: UITableViewDelegate and UITableViewDataSource
extension RequestReceivedViewController : UITableViewDelegate , UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return listRequestSent.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "RequestReceivedTableViewCell") as! RequestReceivedTableViewCell
		cell.setData(listRequestSent[indexPath.row])
		cell.btnAccept.tag = indexPath.row
		cell.btnReject.tag = indexPath.row
		cell.btnDetails.tag = indexPath.row
		cell.btnAccept.addTarget(self, action: #selector(self.actionAccept(_:)), for: .touchUpInside)
		cell.btnReject.addTarget(self, action: #selector(self.actionReject(_:)), for: .touchUpInside)
		cell.btnDetails.addTarget(self, action: #selector(self.actionDetails(_:)), for: .touchUpInside)
		let tap = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped(_:)))
		cell.imgOwner.addGestureRecognizer(tap)
		cell.imgOwner.tag = indexPath.row
		return cell
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 400
	}
	
	
}
