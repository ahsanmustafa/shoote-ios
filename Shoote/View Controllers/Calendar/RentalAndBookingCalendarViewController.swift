//
//  RentalCalendarViewController.swift
//  Shoote
//
//  Created by BrainX IOS 3 on 15/09/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit

class RentalAndBookingCalendarViewController: BaseViewController {

	//MARK: Properties
	var dataList = [CarBooking]()
	var type = ""
	var startDate : Date?
	var endDate : Date?
	var range = GLCalendarDateRange()
	var createBooking = CreateBookingModel()
	
	//MARK: Outlets
	@IBOutlet weak var calendarView : GLCalendarView!
	@IBOutlet weak var lblTopTitle : UILabel!
	@IBOutlet weak var tabelView : UITableView!
	
	//MARK: Lazy Variables
	private lazy var datePicker : UIDatePicker = {
		let datePicker = UIDatePicker()
		datePicker.translatesAutoresizingMaskIntoConstraints = false
		datePicker.autoresizingMask = .flexibleWidth
		if #available(iOS 13.4, *) {
			datePicker.preferredDatePickerStyle = .wheels
			datePicker.backgroundColor = .white
		} else {
			datePicker.backgroundColor = .white
		}
		datePicker.datePickerMode = .time
		
		return datePicker
	}()
	private lazy var toolBar : UIToolbar = {
		let toolBar = UIToolbar()
		toolBar.translatesAutoresizingMaskIntoConstraints = false
		toolBar.barStyle = .default
		let done = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.onDoneButtonClick))
		let cancel = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.oncancelButtonClick))
		let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
		toolBar.setItems([cancel, flexible, done], animated: false)
		toolBar.sizeToFit()
		return toolBar
	}()
	
	//MARK: Override Functions
    override func viewDidLoad() {
        super.viewDidLoad()
		updateViews()
    }
    
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		self.calendarView.scroll(to: Date().startOfMonth, animated: true)
	}
	
	//MARK: Private Methods
	private func updateViews() {
		lblTopTitle.text = "Today's \(type) Activity"
		calendarView.delegate = self
		calendarView.showMagnifier = false
		calendarView.backgroundColor = .clear
		// set first date one year ago from now
		calendarView.firstDate = Calendar.current.date(byAdding: .year, value: -1, to: Date())
		// set last date one year ago from now
		calendarView.lastDate = Calendar.current.date(byAdding: .year, value: 1, to: Date())
		startDate = Date().startOfDay
		endDate = Date().endOfDay
		range = GLCalendarDateRange(begin: startDate, end: endDate)
		range.beginDate = startDate
		range.endDate = endDate
		range.backgroundColor = UIColor(hexFromString: "e39b00")
		range.editable = false
		calendarView.addRange(range)
		calendarView.reload()
		if type == "Rental" {
			createRentalRequest()
		} else {
			createbookingRequest()
		}
	}
	
	private func createRentalRequest() {
		guard let startDate = self.startDate, let endDate = self.endDate else {
			self.showAlertView(message: PopupMessages.SelectDateRange, title: ALERT_TITLE_APP_NAME)
			return
		}
		createBooking.startDate = startDate
		createBooking.endDate = endDate
		
		createBooking.startDateString = Utilities.localToUTCDateString(with: startDate)
		createBooking.endDateString = Utilities.localToUTCDateString(with: endDate)
		createRentalRequestApi(start: createBooking.startDateString, end: createBooking.endDateString)
		
	}
	
	private func createbookingRequest() {
		guard let startDate = self.startDate, let endDate = self.endDate else {
			self.showAlertView(message: PopupMessages.SelectDateRange, title: ALERT_TITLE_APP_NAME)
			return
		}
		createBooking.startDate = startDate
		createBooking.endDate = endDate
		let dateFormatter = DateFormatter()
		if let timezone = TimeZone(abbreviation: "UTC") {
			dateFormatter.timeZone = timezone
		}
		dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss a"
		createBooking.startDateString = dateFormatter.string(from: startDate)
		createBooking.endDateString = dateFormatter.string(from: endDate)
		createbookingRequestApi(start: createBooking.startDateString, end: createBooking.endDateString)
		
	}
	
	private func selectTime() {
		self.view.addSubview(self.datePicker)
		self.view.addSubview(self.toolBar)
		
		NSLayoutConstraint.activate([
			self.datePicker.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
			self.datePicker.leftAnchor.constraint(equalTo: self.view.leftAnchor),
			self.datePicker.rightAnchor.constraint(equalTo: self.view.rightAnchor),
			self.datePicker.heightAnchor.constraint(equalToConstant: 200)
		])
		
		NSLayoutConstraint.activate([
			self.toolBar.bottomAnchor.constraint(equalTo: self.datePicker.topAnchor),
			self.toolBar.leftAnchor.constraint(equalTo: self.view.leftAnchor),
			self.toolBar.rightAnchor.constraint(equalTo: self.view.rightAnchor),
			self.toolBar.heightAnchor.constraint(equalToConstant: 50)
		])
	}
	
	//MARK: Actions
	@IBAction
	func actionBack(_ sender : UIButton){
		self.navigationController?.popViewController(animated: true)
	}
	
	@objc
	func onDoneButtonClick() {
		toolBar.removeFromSuperview()
		datePicker.removeFromSuperview()
		let date = datePicker.date
		let calendar = Calendar.current
		let hour = calendar.component(.hour, from: date)
		let minute = calendar.component(.minute, from: date)
		let second = calendar.component(.second, from: date)
		if endDate == nil {
			startDate = Calendar.current.date(bySettingHour: hour, minute: minute, second: second, of: startDate!)!
		} else {
			endDate = Calendar.current.date(bySettingHour: hour, minute: minute, second: second, of: endDate!)!
			lblTopTitle.text = "\(type) Activity"
			if type == "Rental" {
				createRentalRequest()
			} else {
				createbookingRequest()
			}
		}
	}
	
	@objc
	func oncancelButtonClick() {
		toolBar.removeFromSuperview()
		datePicker.removeFromSuperview()
	}

}

//MARK: UITableViewDataSource and UITableViewDelegate Methods
extension RentalAndBookingCalendarViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return dataList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RentalCalendarTableViewCell") as! RentalCalendarTableViewCell
		cell.setData(dataList[indexPath.row])
        return cell
    }
    
}

//MARK: GLCalendarViewDelegate Methods
extension RentalAndBookingCalendarViewController : GLCalendarViewDelegate {
	func calenderView(_ calendarView: GLCalendarView!, canAddRangeWithBegin beginDate: Date!) -> Bool {
		return true
	}
	
	func calenderView(_ calendarView: GLCalendarView!, rangeToAddWithBegin beginDate: Date!) -> GLCalendarDateRange! {
		if startDate == nil {
			calendarView.removeRange(range)
			startDate = beginDate
			range = GLCalendarDateRange(begin: startDate, end: startDate)
			range.beginDate = startDate
			range.endDate = startDate
			range.backgroundColor = UIColor(hexFromString: "e39b00")
			range.editable = false
			selectTime()
			return range
		} else if endDate == nil {
			endDate = beginDate
			self.range.endDate = endDate
			selectTime()
			return range
		} else {
			startDate = beginDate
			calendarView.removeRange(range)
			//range = nil
			endDate = nil
			range = GLCalendarDateRange(begin: startDate, end: startDate)
			
			range.backgroundColor = UIColor(hexFromString: "e39b00")
			range.editable = false
			selectTime()
			return range
		}
	}
	
	func calenderView(_ calendarView: GLCalendarView!, beginToEdit range: GLCalendarDateRange!) {
		
	}
	
	func calenderView(_ calendarView: GLCalendarView!, finishEdit range: GLCalendarDateRange!, continueEditing: Bool) {
		
	}
	
	func calenderView(_ calendarView: GLCalendarView!, canUpdate range: GLCalendarDateRange!, toBegin beginDate: Date!, end endDate: Date!) -> Bool {
		return true
	}
	
	func calenderView(_ calendarView: GLCalendarView!, didUpdate range: GLCalendarDateRange!, toBegin beginDate: Date!, end endDate: Date!) {
		print("didUpdate")
	}
	
	
}

//MARK: APi Call
extension RentalAndBookingCalendarViewController {
	
	private func createRentalRequestApi(start: String, end: String) {
		self.startLoading()
		API.shared.getRentalsForCalendarByOwnerId(
			fromDate: start,
			toDate: end,
			result: { status, message, listing in
				self.stopLoading()
				self.startDate = nil
				self.endDate = nil
				if status {
					self.dataList = listing
					self.tabelView.reloadData()
				} else {
					self.showAlertView(message: message, title: ALERT_TITLE_APP_NAME)
				}
			})
	}
	
	private func createbookingRequestApi(start: String, end: String) {
		self.startLoading()
		API.shared.getBookingsForCalendarByUserId(
			fromDate: start,
			toDate: end,
			result: { (status, message, listing) in
				self.stopLoading()
				self.startDate = nil
				self.endDate = nil
				if status {
					self.dataList = listing
					self.tabelView.reloadData()
				} else {
					self.showAlertView(message: message, title: ALERT_TITLE_APP_NAME)
				}
			})
	}
}
