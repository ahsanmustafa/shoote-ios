//
//  UseOfVehicleViewController.swift
//  Shoote
//
//  Created by Faheem on 19/07/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit
import DLRadioButton
import SkyFloatingLabelTextField
import UPCarouselFlowLayout
import GooglePlaces

class UseOfVehicleViewController: BaseViewController {
	
	//MARK: Properties
	let typeOfBooking = DropDown()
	var data : CarListing!
	var createBooking = CreateBookingModel()
	private var currentPage: Int = 0
	fileprivate var pageSize: CGSize {
		let layout = self.collectionView.collectionViewLayout as! UPCarouselFlowLayout
		var pageSize = layout.itemSize
		if layout.scrollDirection == .horizontal {
			pageSize.width += layout.minimumLineSpacing
		} else {
			pageSize.height += layout.minimumLineSpacing
		}
		return pageSize
	}
	
	//MARK: Outlets
	@IBOutlet weak var btnTypeOfbooking : UIButton!
	@IBOutlet weak var radioDrivenYes : DLRadioButton!
	@IBOutlet weak var radioDrivenNo : DLRadioButton!
	@IBOutlet weak var collectionView: UICollectionView!
	@IBOutlet weak var txtUseOfVehicle : SkyFloatingLabelTextField!
	@IBOutlet weak var txtLocation : SkyFloatingLabelTextField!
	@IBOutlet weak var txtAddress : SkyFloatingLabelTextField!
	@IBOutlet weak var txtCity : SkyFloatingLabelTextField!
	@IBOutlet weak var txtState : SkyFloatingLabelTextField!
	@IBOutlet weak var txtZip : SkyFloatingLabelTextField!
	@IBOutlet weak var lblCarName : UILabel!
	@IBOutlet weak var imgCar : UIImageView!
    @IBOutlet weak private var search: UITextField!
	
	//MARK: Override Functions
    override func viewDidLoad() {
        super.viewDidLoad()
		GMSPlacesClient.provideAPIKey(APIKeys.GoogleMapsKey)
		initializeUI()
        search.delegate = self
		lblCarName.text = data.name
		if let sd = data.listingImages.first?.imagePath
		{
			let path = EndPoints.BASE_URL_IMG + sd
			setImageWithUrl(imageView: imgCar, url: path,placeholderImage: "img_car_placeholder")
		}
    }
    
	//MARK: Private Methods
	private func initializeUI() {
		typeOfBooking.anchorView = btnTypeOfbooking
		radioDrivenNo.isSelected = true
		let list = data.carHostActivity.components(separatedBy: ",")
		btnTypeOfbooking.setTitle(list[0], for: .normal)
		typeOfBooking.dataSource = list
		createBooking.typeOfBooking = list[0]
		typeOfBooking.selectionAction = {(index, item) in
			self.btnTypeOfbooking.setTitle(item, for: .normal)
			self.createBooking.typeOfBooking = item
		}
		createBooking.numberOfPersons = 0
		txtUseOfVehicle.titleFormatter = { $0 }
		txtLocation.titleFormatter = { $0 }
		txtAddress.titleFormatter = { $0 }
		txtCity.titleFormatter = { $0 }
		txtState.titleFormatter = { $0 }
		txtZip.titleFormatter = { $0 }
	}
	
	private func validate() -> Bool {
		var isValid = true
		
		if !radioDrivenYes.isSelected && !radioDrivenNo.isSelected {
			
			showAlertView(message: "Please select will the car be driven", title: ALERT_TITLE_APP_NAME)
			isValid = false
			
		} else if txtUseOfVehicle.text!.isEmpty {
			
			showAlertView(message: "Enter use of vehicle", title: ALERT_TITLE_APP_NAME)
			isValid = false
			
		} else if txtLocation.text!.isEmpty {
			
			showAlertView(message: "Enter location of the project", title: ALERT_TITLE_APP_NAME)
			isValid = false
			
		} else if txtAddress.text!.isEmpty {
			
			showAlertView(message: "Enter address of the project", title: ALERT_TITLE_APP_NAME)
			isValid = false
			
		} else if txtCity.text!.isEmpty {
			
			showAlertView(message: "Enter city of the project", title: ALERT_TITLE_APP_NAME)
			isValid = false
			
		} else if txtState.text!.isEmpty {
			
			showAlertView(message: "Enter state of the project", title: ALERT_TITLE_APP_NAME)
			isValid = false
			
		} else if txtZip.text!.isEmpty {
			
			showAlertView(message: "Enter zip of the project", title: ALERT_TITLE_APP_NAME)
			isValid = false
			
		}
		
		return isValid
	}

	//MARK: Actions
    @IBAction
	func actionBack(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
	
	@IBAction
	func actionShowBooking(_ sender : UIButton){
		typeOfBooking.show()
	}
	
	@IBAction
	func actionNext(_ sender : UIButton){
		if validate() {
			createBooking.willCarDriven = radioDrivenNo.isSelected || radioDrivenYes.isSelected
			createBooking.numberOfPersons = currentPage
			createBooking.useOfVehicle = txtUseOfVehicle.text!
			createBooking.location = txtLocation.text!
			createBooking.address = txtAddress.text!
			createBooking.city = txtCity.text!
			createBooking.state = txtState.text!
			createBooking.zip = txtZip.text!
			let vc = UIViewController.instantiate(BookingConfirmationViewController.self, fromStoryboard: .Main)
			vc.createBooking = createBooking
			vc.data = data
			self.navigationController?.pushViewController(vc, animated: true)
		}
	}

}
//MARK: UICollectionViewDelegate and UICollectionViewDataSource Methods
extension UseOfVehicleViewController : UICollectionViewDelegate, UICollectionViewDataSource {
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		guard let maxCapacity = Int(data.maxCapacity ) else {
			return 0
		}
		return maxCapacity + 1
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NumberOfPersonsCollectionViewCell", for: indexPath) as! NumberOfPersonsCollectionViewCell
		cell.lblCount.text = String(indexPath.row)
		return cell
	}
	
	// MARK: - UIScrollViewDelegate
	
	func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
		let layout = self.collectionView.collectionViewLayout as! UPCarouselFlowLayout
		let pageSide = (layout.scrollDirection == .horizontal) ? self.pageSize.width : self.pageSize.height
		let offset = (layout.scrollDirection == .horizontal) ? scrollView.contentOffset.x : scrollView.contentOffset.y
		currentPage = Int(floor((offset - pageSide / 2) / pageSide) + 1)
	}
	
}

//MARK:- UITextFieldDelegate Methods
extension UseOfVehicleViewController: UITextFieldDelegate {
	
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.endEditing(true)
        let acController = GMSAutocompleteViewController()
        acController.delegate = self
        let filter = GMSAutocompleteFilter()
        filter.country = "us"
        acController.autocompleteFilter = filter
        present(acController, animated: true, completion: nil)
    }
}

//MARK:- GMSAutocompleteViewControllerDelegate Methods
extension UseOfVehicleViewController : GMSAutocompleteViewControllerDelegate {
	
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        // Get the place name from 'GMSAutocompleteViewController'
        // Then display the name in textField
        search.text = place.name
        txtAddress.text = place.name
        if let address = place.addressComponents {
            for comp in address {
                for type in comp.types {
                    if type == "locality" {
                        txtCity.text = comp.name
                    } else if type == "administrative_area_level_1" {
                        txtState.text = comp.name
                    } else if type == "postal_code" {
                        txtZip.text = comp.name
                    }
                }
            }
        }
        // Dismiss the GMSAutocompleteViewController when something is selected
        dismiss(animated: true, completion: nil)
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // Handle the error
        print("Error: ", error.localizedDescription)
    }
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        // Dismiss when the user canceled the action
        dismiss(animated: true, completion: nil)
    }
}
