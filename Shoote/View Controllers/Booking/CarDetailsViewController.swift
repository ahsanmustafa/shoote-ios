//
//  CarDetailsViewController.swift
//  Shoote
//
//  Created by Faheem on 19/07/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit
import ImageSlideshow
import Kingfisher
import SDWebImage
import Cosmos

class CarDetailsViewController: BaseViewController {
    
	//MARK: Properties
	var carListing = CarListing()
	var reviewList = [Review]()
	var userProfile: BusinessProfile?
	private var services = [String]()
	
	//MARK: Outlets
    @IBOutlet weak private var slideShow : ImageSlideshow!
    
	@IBOutlet weak private var btnBookNow : UIButton!
    @IBOutlet weak private var carTitle : UILabel!
    @IBOutlet weak private var carTitlee : UILabel!
    @IBOutlet weak private var location : UILabel!
    @IBOutlet weak private var rate : UILabel!
    @IBOutlet weak private var ratee : UILabel!
    @IBOutlet weak private var capacity : UILabel!
    @IBOutlet weak private var minTime : UILabel!
    @IBOutlet weak private var desc : UILabel!
	@IBOutlet weak private var ratingView : CosmosView!
	@IBOutlet weak private var ratingOwnerView : CosmosView!
    @IBOutlet weak private var lblMilesRadius : UILabel!
    
    
    @IBOutlet weak private var servicesTable : UITableView!
    @IBOutlet weak private var daysTable : UITableView!
	@IBOutlet weak private var reviewsTable : UITableView!
	@IBOutlet weak private var reviewsTableHeightConstraint : NSLayoutConstraint!
	@IBOutlet weak private var lblReviewsCount : UILabel!
    
    @IBOutlet weak private var cancelHeading : UILabel!
    @IBOutlet weak private var cancelText : UILabel!
    
    @IBOutlet weak private var hostImage : UIImageView!
    @IBOutlet weak private var hostName : UILabel!
	
	//MARK: Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
		getHostData()
        
        carTitle.text = carListing.name
        carTitlee.text = carListing.name
        capacity.text = carListing.maxCapacity
		ratingView.rating = Double(carListing.averageRating )
		let rateText = NSNumber(value: carListing.hourlyRate).intValue
		rate.text = "$\(rateText)/hr"
		ratee.text = "$\(rateText)/hr"
		minTime.text = "\(carListing.minHours.toInt() ) Hr"
        desc.text = carListing.description
		lblMilesRadius.text = (carListing.maxDeliveryRadius ).toString()
		location.text = carListing.location.city
		if carListing.cancellation == 0 {
            cancelHeading.text = "Flexible"
            cancelText.text =  Flexible
		} else if carListing.cancellation == 1 {
            cancelHeading.text = "Strict"
            cancelText.text = Strict
        } else {
            cancelHeading.text = "Very Strict"
            cancelText.text = VeryStrict
        }
        
		services = carListing.carHostActivity.components(separatedBy: ",")
        
        var aar = [InputSource]()
		for src in carListing.listingImages {
			aar.append(KingfisherSource(urlString: EndPoints.BASE_URL_IMG + src.imagePath.replacingOccurrences(of: "\\", with: "/"))!)
        }
        
        slideShow.setImageInputs(aar)
		slideShow.slideshowInterval = 3.0
		
		let tap = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped(_:)))
		hostImage.addGestureRecognizer(tap)
		
		reviewList = carListing.reviews
		if reviewList.count != 0 {
			lblReviewsCount.textColor = UIColor(named: "yellowNewColor")
			lblReviewsCount.text = "\(reviewList.count) Reviews"
		}
		reviewsTable.reloadData()
		
		if carListing.businessProfileId == Global.shared.user.businessProfileId {
			btnBookNow.isHidden = true
		}
    }
    
	override func viewWillLayoutSubviews() {
		super.updateViewConstraints()
		DispatchQueue.main.async {
			self.reviewsTableHeightConstraint.constant = self.reviewsTable.contentSize.height
			self.view.layoutIfNeeded()
		}
	}
	
	//MARK: Private Methods
    private func getHostData() {
		API.shared.getBusinessProfileById(id: carListing.businessProfileId) { (status, msg, profile) in
			self.userProfile = profile
            if let dt = profile {
				self.hostName.text = dt.title
				self.ratingView.rating = dt.ratingAvg.toDouble()
				let url = EndPoints.BASE_URL_IMG + dt.profileImage
				self.setImageWithUrl(imageView: self.hostImage, url: url,placeholderImage: "user")
            }
        }
    }
    
	//MARK: Actions
	@objc
	func imageTapped(_ tap: UITapGestureRecognizer) {
		guard let profile = userProfile else {
			showAlertView(message: "Loading Profile in Background.", title: ALERT_TITLE_APP_NAME)
			return
		}
		let vc = UIViewController.instantiate(ViewUserProfileViewController.self, fromStoryboard: .CreateService)
		vc.host = profile
		vc.userType = "host"
		vc.id = profile.id
		self.navigationController?.pushViewController(vc, animated: true)
		
	}
	
	@IBAction
	func actionContactHost(_ sender : UIButton){
		guard let profile = userProfile else {
			showAlertView(message: "Loading Profile in Background.", title: ALERT_TITLE_APP_NAME)
			return
		}
		self.startLoading()
		API.shared.openConversation(id: profile.id) { status, message, conversation in
			self.stopLoading()
			if status {
				let vc = UIViewController.instantiate(ChatDetailsViewController.self, fromStoryboard: .Main)
				vc.conversation = conversation
				self.navigationController?.pushViewController(vc, animated: true)
			} else {
				self.showAlertView(message: message, title: ALERT_TITLE_APP_NAME)
			}
		}
	}
	
    @IBAction
	func actionBack(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
	
	@IBAction
	func actionBookNow(_ sender : UIButton){
		let vc = UIViewController.instantiate(CarAvailabilityViewController.self, fromStoryboard: .Main)
		vc.data = carListing
		self.navigationController?.pushViewController(vc, animated: true)
	}
    
}

//MARK: UITableViewDataSource Methods
extension CarDetailsViewController : UITableViewDataSource {
	
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == servicesTable {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            
            (cell.contentView.viewWithTag(1) as! UIButton).setTitle(services[indexPath.row], for: .normal)
            return cell
			
		} else if tableView == reviewsTable {
			
			let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewItemTableViewCell") as! ReviewItemTableViewCell
			cell.setData(reviewList[indexPath.row])
			return cell
			
		} else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            let day = (cell.contentView.viewWithTag(1) as! UILabel)
            let time = (cell.contentView.viewWithTag(2) as! UILabel)
            
            let row = indexPath.row
			if row == 0 {// Mon
                day.text = "Monday"
                time.text = carListing.mondayTiming
            } else if row == 1 {
                day.text = "Tuesday"
                time.text = carListing.tuesdayTiming
            } else if row == 2{
                day.text = "Wednesday"
                time.text = carListing.wednesdayTiming
                
            } else if row == 3 {
                day.text = "Thursday"
                time.text = carListing.thursdayTiming
                
            } else if row == 4 {
                day.text = "Friday"
                time.text = carListing.fridayTiming
                
            } else if row == 5 {
                day.text = "Saturday"
                time.text = carListing.saturdayTiming
                
             }else {
                day.text = "Sunday"
                time.text = carListing.sundayTiming
                
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == servicesTable {
            return services.count
		} else if tableView == reviewsTable {
			return reviewList.count
		} else {
            return 7
        }
    }
}
