//
//  BookingConfirmationViewController.swift
//  Shoote
//
//  Created by BrainX IOS 3 on 16/12/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit

class BookingConfirmationViewController: BaseViewController {

	//MARK: Properties
	var isPhoneNumberSelected = true
	var createBooking = CreateBookingModel()
	var data : CarListing!
	var localEmail = ""
	var localPhone = ""
	
	//MARK: Outlets
	@IBOutlet weak var btnEmail : UIButton!
	@IBOutlet weak var btnPhone : UIButton!
	@IBOutlet weak var btnSend : UIButton!
	@IBOutlet weak var lblMsg : UILabel!
	@IBOutlet weak var txtPhone : UITextField!
	
	//MARK: Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
		txtPhone.text = Global.shared.user.phoneNo
    }
	
	//MARK: Private Methods
	
	//MARK: Actions
	@IBAction
	func actionNext(_ sender : UIButton){
		let vc = UIViewController.instantiate(ReviewBookingViewController.self, fromStoryboard: .Main)
		vc.createBooking = createBooking
		if localPhone.isEmpty {
			createBooking.phoneNumber = Global.shared.user.phoneNo
		} else {
			createBooking.phoneNumber = localPhone
		}
		if localEmail.isEmpty {
			createBooking.email = Global.shared.user.email
		} else {
			createBooking.email = localEmail
		}
		vc.data = data
		self.navigationController?.pushViewController(vc, animated: true)
	}
	
	@IBAction
	func actionUpdate(_ sender : UIButton){
		if isPhoneNumberSelected {
			localPhone = txtPhone.text!
		} else {
			localEmail = txtPhone.text!
		}
		let msg = isPhoneNumberSelected ? "Phone number updated" : "Email updated"
		self.showAlertView(message: msg, title: msg)
	}
	
	@IBAction
	func actionPhoneConfirmation(_ sender : UIButton){
		self.btnPhone.backgroundColor = UIColor.init(hexFromString: "EAA215")
		self.btnEmail.backgroundColor = UIColor.init(hexFromString: "EAA215", alpha: 0.23)
		self.txtPhone.placeholder = "+923011234567"
		self.lblMsg.text = "A confirmation of your booking will be sent to your number. Please enter the number you wish to receive the confirmation on."
		isPhoneNumberSelected = !isPhoneNumberSelected
		txtPhone.text = Global.shared.user.phoneNo
	}
	
	@IBAction
	func actionEmailConfirmation(_ sender : UIButton){
		self.btnEmail.backgroundColor = UIColor.init(hexFromString: "EAA215")
		self.btnPhone.backgroundColor = UIColor.init(hexFromString: "EAA215", alpha: 0.23)
		self.txtPhone.placeholder = "shote@gmail.com"
		self.lblMsg.text = "A confirmation of your booking will be sent to your email. Please enter the phone you wish to receive the confirmation on."
		isPhoneNumberSelected = !isPhoneNumberSelected
		txtPhone.text = Global.shared.user.email
	}
	
	@IBAction
	func actionBack(_ sender : UIButton){
		self.navigationController?.popViewController(animated: true)
	}
}
