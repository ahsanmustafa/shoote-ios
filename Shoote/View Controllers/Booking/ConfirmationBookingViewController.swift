//
//  ConfirmationBookingViewController.swift
//  Shoote
//
//  Created by Faheem on 19/07/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit
import Cosmos

class ConfirmationBookingViewController: BaseViewController {

	//MARK: Outlets
	@IBOutlet weak var lblBookingNumber : UILabel!
	@IBOutlet weak var lblCarName : UILabel!
	@IBOutlet weak var imgCar : UIImageView!
	@IBOutlet weak var imgProfile : UIImageView!
	@IBOutlet weak var lblHostName : UILabel!
	@IBOutlet weak var lblTypeOfBookig : UILabel!
	@IBOutlet weak var lblDate : UILabel!
	@IBOutlet weak var lblDiscountHourlyTotal : UILabel!
	@IBOutlet weak var lblDeliveryFee : UILabel!
	@IBOutlet weak var lblProcessing : UILabel!
	@IBOutlet weak var lblGrandTotal : UILabel!
	@IBOutlet weak var viewRating : CosmosView!
	
	//MARK: Properties
	var createBooking = CreateBookingModel()
	var data : CarListing!
	var confirmationNumber = "12345"
	var userProfile: BusinessProfile?
	
	//MARK: Override Functions
    override func viewDidLoad() {
        super.viewDidLoad()
		populateData()
    }
	
	//MARK: Private Methods
	private func populateData() {
		getUserData()
		lblBookingNumber.text = "Confirmation Number # \(confirmationNumber)"
		lblCarName.text = data.name
		if let sd = data.listingImages.first?.imagePath
		{
			let path = EndPoints.BASE_URL_IMG + sd
			setImageWithUrl(imageView: imgCar, url: path,placeholderImage: "img_car_placeholder")
		}
		lblTypeOfBookig.text = createBooking.typeOfBooking
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy hh:mm a"
        createBooking.startDateString = dateFormatter.string(from: createBooking.startDate)
        createBooking.endDateString = dateFormatter.string(from: createBooking.endDate)
		lblDate.text = createBooking.startDateString + "\n" + createBooking.endDateString
		lblDiscountHourlyTotal.text = "$" + createBooking.hourlyTotal.withCommas()
		let delieveryFee = createBooking.additionalFees.first(where: {$0.type == "Delivery"})?.amount ?? 0.0
		lblDeliveryFee.text = String(format: "$%0.2f", delieveryFee)
		lblProcessing.text = "$" + createBooking.processingFee.withCommas()
		lblGrandTotal.text = "$" + createBooking.totalFee.withCommas()
		
	}
	
	//MARK: Actions
    @IBAction
	func actionBack(_ sender : UIButton){
		self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction
	func actionHome(_ sender : UIButton){
        self.navigationController?.popToRootViewController(animated: true)
    }
	
	@IBAction
	func actionContactHost(_ sender : UIButton){
		guard let profile = userProfile else {
			showAlertView(message: "Loading Profile in Background.", title: ALERT_TITLE_APP_NAME)
			return
		}
		self.startLoading()
		API.shared.openConversation(id: profile.id) { status, message, conversation in
			self.stopLoading()
			if status {
				let vc = UIViewController.instantiate(ChatDetailsViewController.self, fromStoryboard: .Main)
				vc.conversation = conversation
				self.navigationController?.pushViewController(vc, animated: true)
			} else {
				self.showAlertView(message: message, title: ALERT_TITLE_APP_NAME)
			}
		}
	}

	//MARK: API Call
	private func getUserData() {
		API.shared.getBusinessProfileById(id: data.businessProfileId) { (status, msg, dt) in
			if let dt = dt {
				self.userProfile = dt
				self.lblHostName.text = dt.title
				let url = EndPoints.BASE_URL_IMG + dt.profileImage
				self.viewRating.rating = dt.ratingAvg.toDouble()
				self.setImageWithUrl(imageView: self.imgProfile, url: url)
			}
		}
	}
}
