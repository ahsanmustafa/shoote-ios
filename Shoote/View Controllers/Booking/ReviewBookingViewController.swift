//
//  ReviewBookingViewController.swift
//  Shoote
//
//  Created by Faheem on 19/07/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit

class ReviewBookingViewController: BaseViewController {

	//MARK: Properties
	var createBooking = CreateBookingModel()
	var data : CarListing!
	
	//MARK: Outlets
	@IBOutlet weak var lblTypeOfBookig : UILabel!
	@IBOutlet weak var lblDurationDate : UILabel!
	@IBOutlet weak var lblPricePerHour : UILabel!
	@IBOutlet weak var lblHoursTotal : UILabel!
	@IBOutlet weak var lblTotalPrice : UILabel!
	@IBOutlet weak var lblProcessingFee : UILabel!
	@IBOutlet weak var lblDeliveryFee : UILabel!
	@IBOutlet weak var lblCompleteTotal : UILabel!
	@IBOutlet weak var lblDiscount : UILabel!
	@IBOutlet weak var lblPriceWithDiscount : UILabel!
	@IBOutlet weak var lblCarName : UILabel!
	@IBOutlet weak var imgCar : UIImageView!
	
	//MARK: Override Functions
    override func viewDidLoad() {
        super.viewDidLoad()
		setView()
    }
	
	//MARK: Private Methods
	private func setView() {
		lblTypeOfBookig.text = createBooking.typeOfBooking
		let formatter = DateFormatter()
		formatter.dateFormat = "MMM dd, yyyy hh:mm a"
		lblDurationDate.text = formatter.string(from: createBooking.startDate) + "\n" + formatter.string(from: createBooking.endDate)
		lblPricePerHour.text = "$\((data.hourlyRate ).toInt())/hr"
		let diffComponents = Calendar.current.dateComponents([.hour, .minute], from: createBooking.startDate, to: createBooking.endDate)
		var hours = diffComponents.hour ?? 0
		let minutes = diffComponents.minute ?? 0
		if minutes > 0 {
			hours += 1
		}
		let hourRate = data.hourlyRate 
		lblHoursTotal.text = "\(hours) Hour(s) Total"
		var totalWithoutDiscount = hourRate * Float(hours)
		lblTotalPrice.text = "$\(totalWithoutDiscount.toInt())"
		lblDiscount.text = "\(data.discountPercentage )% for booking \(data.discountTime ) or more hours"
		if hours >= data.discountTime {
			let discount = totalWithoutDiscount * Float(data.discountPercentage ) * 0.01
			totalWithoutDiscount = totalWithoutDiscount - discount
		}
		lblPriceWithDiscount.text = String(format: "$%0.2f", totalWithoutDiscount)
		let delieveryFee = data.additionalFees.first(where: {$0.type == "Delivery"})?.amount ?? 0.0
		lblDeliveryFee.text = String(format: "$%0.2f", delieveryFee)
		let processFee = (totalWithoutDiscount + delieveryFee) * 0.25
		lblProcessingFee.text = "$\(processFee.withCommas())"
		lblCompleteTotal.text = "$\((totalWithoutDiscount + delieveryFee + processFee).withCommas())"
		
		createBooking.hourlyTotal = totalWithoutDiscount
		createBooking.processingFee = processFee
		createBooking.totalFee = totalWithoutDiscount + delieveryFee + processFee
		if delieveryFee != 0 {
			createBooking.additionalFees = data.additionalFees
		}
		createBooking.appUserId = data.businessProfileId
		createBooking.listingId = data.id 
		
		lblCarName.text = data.name
		if let sd = data.listingImages.first?.imagePath
		{
			let path = EndPoints.BASE_URL_IMG + sd
			setImageWithUrl(imageView: imgCar, url: path,placeholderImage: "img_car_placeholder")
		}
	}
	
	
	//MARK: Actions
    @IBAction
	func actionBack(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
	
	@IBAction
	func actionNext(_ sender : UIButton){
		if checkInternetConnection() {
			createBookingReuest()
		}
	}
	
    @IBAction
	func actionCancellation(_ sender : UIButton){
        let story = UIStoryboard(name: "Setting", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "CancellationRulesViewController") as! CancellationRulesViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
	
    @IBAction
	func actionPaymentsMethods(_ sender : UIButton){
        let story = UIStoryboard(name: "Setting", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "PaymentsPayoutsViewController") as! PaymentsPayoutsViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
	
    @IBAction
	func actionTermsAndConditions(_ sender : UIButton){
        let story = UIStoryboard(name: "Setting", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "TermsOfServicesViewController") as! TermsOfServicesViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
	
	//MARK: API Call
	private func createBookingReuest() {
		
		self.startLoading()
		API.shared.createCarBooking(booking: createBooking) { (stats, message) in
			self.stopLoading()
			if stats {
				let vc = UIViewController.instantiate(ConfirmationBookingViewController.self, fromStoryboard: .Main)
				vc.data = self.data
				vc.createBooking = self.createBooking
				self.navigationController?.pushViewController(vc, animated: true)
			} else {
				self.showAlertView(message: message, title: ALERT_TITLE_APP_NAME)
			}
		}
	}
}
