//
//  CarsNearViewController.swift
//  Shoote
//
//  Created by Faheem on 13/07/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit
import SVProgressHUD
import SDWebImage

class CarsNearViewController: BaseViewController,UITextFieldDelegate {
    
	//MARK: Outlets
    @IBOutlet weak var carsCollectionView : UICollectionView!
    @IBOutlet weak var filterAlertMainView: UIView!

    @IBOutlet weak private var name: UITextField!
    @IBOutlet weak private var year: UITextField!
    @IBOutlet weak private var make: UITextField!
    @IBOutlet weak private var model: UITextField!
    
    private var exteriorColorDD : DropDown!
    @IBOutlet weak private var exteriorColor: UITextField!
    @IBOutlet weak private var exteriorColorViewDD: UIView!
	
	private var interiorColorDD : DropDown!
	@IBOutlet weak private var interiorColor: UITextField!
	@IBOutlet weak private var interiorColorViewDD: UIView!
    
    private var bodyStyleDD : DropDown!
    @IBOutlet weak private var bodyStyle: UITextField!
    @IBOutlet weak private var bodyStyleViewDD: UIView!
    
    private var typeOfCarDD : DropDown!
    @IBOutlet weak private var typeOfCar: UITextField!
    @IBOutlet weak private var typeOfCarViewDD: UIView!
    
    @IBOutlet weak private var city: UITextField!
    @IBOutlet weak private var state: UITextField!
    @IBOutlet weak private var maxHourlyRate: UITextField!
    
    private var typeOfBookingDD : DropDown!
    @IBOutlet weak private var typeOfBooking: UITextField!
    @IBOutlet weak private var typeOfBookingViewDD: UIView!
    
    @IBOutlet weak private var minHours: UITextField!
	
	//MARK: Properties
    var carListing = [CarListing]()
    
	//MARK: Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
		
        let verticalFlowLayout = UICollectionViewFlowLayout()
        verticalFlowLayout.scrollDirection = .vertical
        verticalFlowLayout.minimumInteritemSpacing = 0
        verticalFlowLayout.minimumLineSpacing = 0
        carsCollectionView.collectionViewLayout = verticalFlowLayout
        
        self.filterAlertMainView.superview?.isHidden = true
        filterAlertMainView.superview?.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        name.delegate = self
        year.delegate = self
        make.delegate = self
        model.delegate = self
		exteriorColor.delegate = self
		interiorColor.delegate = self
        bodyStyle.delegate = self
        typeOfCar.delegate = self
        city.delegate = self
        state.delegate = self
        maxHourlyRate.delegate = self
        typeOfBooking.delegate = self
        minHours.delegate = self
        
        let paddingViewName = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: self.name.frame.height))
        name.leftView = paddingViewName
        name.leftViewMode = UITextField.ViewMode.always
        
        let paddingViewYear = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: self.year.frame.height))
        year.leftView = paddingViewYear
        year.leftViewMode = UITextField.ViewMode.always
        
        let paddingViewMake = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: self.make.frame.height))
        make.leftView = paddingViewMake
        make.leftViewMode = UITextField.ViewMode.always
        
        let paddingViewmodel = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: self.model.frame.height))
        model.leftView = paddingViewmodel
        model.leftViewMode = UITextField.ViewMode.always
        
        let paddingViewExteriorColor = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: self.exteriorColor.frame.height))
		exteriorColor.leftView = paddingViewExteriorColor
		exteriorColor.leftViewMode = UITextField.ViewMode.always
		
		let paddingViewInteriorColor = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: self.interiorColor.frame.height))
		interiorColor.leftView = paddingViewInteriorColor
		interiorColor.leftViewMode = UITextField.ViewMode.always
        
        let paddingViewbodyStyle = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: self.bodyStyle.frame.height))
        bodyStyle.leftView = paddingViewbodyStyle
        bodyStyle.leftViewMode = UITextField.ViewMode.always
        
        let paddingViewtypeOfCar = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: self.typeOfCar.frame.height))
        typeOfCar.leftView = paddingViewtypeOfCar
        typeOfCar.leftViewMode = UITextField.ViewMode.always
        
        let paddingViewcity = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: self.city.frame.height))
        city.leftView = paddingViewcity
        city.leftViewMode = UITextField.ViewMode.always
        
        let paddingViewstate = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: self.state.frame.height))
        state.leftView = paddingViewstate
        state.leftViewMode = UITextField.ViewMode.always
        
        let paddingViewmaxHourlyRate = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: self.maxHourlyRate.frame.height))
        maxHourlyRate.leftView = paddingViewmaxHourlyRate
        maxHourlyRate.leftViewMode = UITextField.ViewMode.always
        
        let paddingViewtypeOfBooking = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: self.typeOfBooking.frame.height))
        typeOfBooking.leftView = paddingViewtypeOfBooking
        typeOfBooking.leftViewMode = UITextField.ViewMode.always
        
        let paddingViewminHours = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: self.minHours.frame.height))
        minHours.leftView = paddingViewminHours
        minHours.leftViewMode = UITextField.ViewMode.always
        
		exteriorColor.attributedPlaceholder = NSAttributedString(string: "Not Selected", attributes: [NSAttributedString.Key.foregroundColor : UIColor.whiteColor])
		interiorColor.attributedPlaceholder = NSAttributedString(string: "Not Selected", attributes: [NSAttributedString.Key.foregroundColor : UIColor.whiteColor])
        bodyStyle.attributedPlaceholder = NSAttributedString(string: "Not Selected", attributes: [NSAttributedString.Key.foregroundColor : UIColor.whiteColor])
        typeOfCar.attributedPlaceholder = NSAttributedString(string: "Not Selected", attributes: [NSAttributedString.Key.foregroundColor : UIColor.whiteColor])
        typeOfBooking.attributedPlaceholder = NSAttributedString(string: "Not Selected", attributes: [NSAttributedString.Key.foregroundColor : UIColor.whiteColor])
        
		exteriorColorDD = DropDown(anchorView: exteriorColorViewDD, selectionAction: { (index, item) in
            self.exteriorColor.text = item
		}, dataSource: Utilities.getColorList(), topOffset: nil, bottomOffset: nil, cellConfiguration: nil, cancelAction: nil)
        view.bringSubviewToFront(exteriorColor)
		
		interiorColorDD = DropDown(anchorView: interiorColorViewDD, selectionAction: { (index, item) in
			self.interiorColor.text = item
		}, dataSource: Utilities.getColorList(), topOffset: nil, bottomOffset: nil, cellConfiguration: nil, cancelAction: nil)
		view.bringSubviewToFront(interiorColor)
        
        bodyStyleDD = DropDown(anchorView: bodyStyleViewDD, selectionAction: { (index, item) in
            self.bodyStyle.text = item
		}, dataSource: Utilities.getBodyStyleList(), topOffset: nil, bottomOffset: nil, cellConfiguration: nil, cancelAction: nil)
        view.bringSubviewToFront(bodyStyle)
        
        typeOfCarDD = DropDown(anchorView: typeOfCarViewDD, selectionAction: { (index, item) in
            self.typeOfCar.text = item
		}, dataSource: Utilities.getTypeOfCarList(), topOffset: nil, bottomOffset: nil, cellConfiguration: nil, cancelAction: nil)
        view.bringSubviewToFront(typeOfCar)
        
        typeOfBookingDD = DropDown(anchorView: typeOfBookingViewDD, selectionAction: { (index, item) in
            self.typeOfBooking.text = item
		}, dataSource: Utilities.getTypeOfBookingList(), topOffset: nil, bottomOffset: nil, cellConfiguration: nil, cancelAction: nil)
        view.bringSubviewToFront(typeOfBooking)
        
		if carListing.count == 0 {
			getListingsAPI()
		}
        
    }
    
	//MARK: Private Functions
	private func toggleExteriorColorListDD() {
		if exteriorColorDD.isHidden{
			exteriorColorDD.show()
		}
	}
	
	private func toggleInteriorColorListDD() {
		if interiorColorDD.isHidden{
			interiorColorDD.show()
		}
	}
    
    private func toggleBodyStyleListDD() {
        if bodyStyleDD.isHidden {
            bodyStyleDD.show()
        }
    }
    
    private func toggleTypeOfCarListDD() {
        if typeOfCarDD.isHidden {
            typeOfCarDD.show()
        }
    }
	
    private func toggleTypeOfBookingListDD() {
        if typeOfBookingDD.isHidden {
            typeOfBookingDD.show()
        }
    }
    
	//MARK: UITextField Delegates
	func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
		
		if textField == exteriorColor {
			toggleExteriorColorListDD()
			return false
		} else if textField == interiorColor {
			toggleInteriorColorListDD()
			return false
		} else if textField == bodyStyle {
			toggleBodyStyleListDD()
			return false
		} else if textField == typeOfCar {
			toggleTypeOfCarListDD()
			return false
		} else if textField == typeOfBooking {
			toggleTypeOfBookingListDD()
			return false
		}
		return true
	}
    
	//MARK: Actions
    @IBAction
	func actionBack(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction
	func filterBtnAction(_ sender: Any) {
        filterAlertMainView.superview?.isHidden = false
    }
    
    @IBAction
	func searchFilterBtnAction(_ sender: Any) {
        if !(!name.text!.isEmpty || !year.text!.isEmpty || !make.text!.isEmpty || !model.text!.isEmpty || !city.text!.isEmpty || !state.text!.isEmpty || !maxHourlyRate.text!.isEmpty || !minHours.text!.isEmpty || !exteriorColor.text!.isEmpty || !bodyStyle.text!.isEmpty || !typeOfCar.text!.isEmpty || !typeOfBooking.text!.isEmpty || !interiorColor.text!.isEmpty)
        {
            self.showAlertView(message: "Apply at least 1 filter to search",title: ALERT_TITLE_APP_NAME)
        }
        else{
            getListingByFilterAPI()
        }
    }
    
    @IBAction
	func cancelBtn(_ sender: Any) {
        self.filterAlertMainView.superview?.isHidden = true
    }
    
}

//MARK: UICollectionViewDelegate and UICollectionViewDataSource Methods
extension CarsNearViewController : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return carListing.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifier.CarsCollectionViewCell, for: indexPath) as! CarsCollectionViewCell
		cell.setData(carListing[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: (collectionView.frame.width/2) - 10, height: 230)
    }
	
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		let vc = UIViewController.instantiate(CarDetailsViewController.self, fromStoryboard: .CarDetails)
		vc.carListing = carListing[indexPath.row]
		self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

//MARK:  API Calls
extension CarsNearViewController {
    private func getListingsAPI() {
        self.startLoading()
        API.shared.getCarListings() { (status, message, list) in
			self.stopLoading()
			if let info = list {
				self.carListing = info.data
				self.carsCollectionView.reloadData()
			} else {
				self.showAlertView(message: message, title: ALERT_TITLE_APP_NAME)
			}
        }
    }
    
    // getListingByFilter
    func getListingByFilterAPI() {
            self.startLoading()
		API.shared.getListingByFilter(name: name.text, make: make.text, model: model.text, year: year.text, city: city.text, state: state.text, exteriorColor: exteriorColorDD.selectedItem, interiorColor: interiorColorDD.selectedItem , typeOfCar: typeOfCarDD.selectedItem, bodyType: bodyStyleDD.selectedItem, minHours: minHours.text, maxHourlyRate: maxHourlyRate.text, bookingType: typeOfBookingDD.selectedItem) { (status, messsage, list) in
				
				self.stopLoading()
				if let info = list {
					self.carListing = info.data
					self.carsCollectionView.reloadData()
					self.filterAlertMainView.superview?.isHidden = true
					return
				}
				self.carListing = []
				self.carsCollectionView.reloadData()
				self.showAlertView(message: messsage, title: ALERT_TITLE_APP_NAME)
            }
        }
}
