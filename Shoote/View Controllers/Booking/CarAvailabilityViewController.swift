//
//  CarAvailabilityViewController.swift
//  Shoote
//
//  Created by Faheem on 19/07/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit

class CarAvailabilityViewController: BaseViewController {
	
	//MARK: Properties
	var startDate : Date?
	var endDate : Date?
	var range = GLCalendarDateRange()
	var data : CarListing!
	var createBooking = CreateBookingModel()
	
	//MARK: Outlets
	@IBOutlet weak var calendarView : GLCalendarView!
	@IBOutlet weak var lblCarName : UILabel!
	@IBOutlet weak var imgCar : UIImageView!
	
	//MARK: Lazy Variables
	private lazy var datePicker : UIDatePicker = {
		let datePicker = UIDatePicker()
		datePicker.translatesAutoresizingMaskIntoConstraints = false
		datePicker.autoresizingMask = .flexibleWidth
		if #available(iOS 13.4, *) {
			datePicker.preferredDatePickerStyle = .wheels
			datePicker.backgroundColor = .white
		} else {
			datePicker.backgroundColor = .white
		}
		datePicker.datePickerMode = .time
		
		return datePicker
	}()
	private lazy var toolBar : UIToolbar = {
		let toolBar = UIToolbar()
		toolBar.translatesAutoresizingMaskIntoConstraints = false
		toolBar.barStyle = .default
		let done = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.onDoneButtonClick))
		let cancel = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.oncancelButtonClick))
		let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
		toolBar.setItems([cancel, flexible, done], animated: false)
		toolBar.sizeToFit()
		return toolBar
	}()
	
	//MARK: Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
		self.calendarView.delegate = self
		self.calendarView.showMagnifier = false
		self.calendarView.backgroundColor = .clear
		self.calendarView.firstDate = GLDateUtils.date(byAddingDays: 0, to: Date())
		lblCarName.text = data.name
		if let sd = data.listingImages.first?.imagePath
		{
			let path = EndPoints.BASE_URL_IMG + sd
			setImageWithUrl(imageView: imgCar, url: path,placeholderImage: "img_car_placeholder")
		}
		
		
	}
	
	//MARK: Private Methods
	private func moveNext() {
		let vc = UIViewController.instantiate(UseOfVehicleViewController.self, fromStoryboard: .Main)
		vc.data = data
		vc.createBooking = createBooking
		self.navigationController?.pushViewController(vc, animated: true)
	}
	
	private func createAvailabilityRequest() {
		let id = self.data.id
		guard let startDate = self.startDate, let endDate = self.endDate else {
			self.showAlertView(message: PopupMessages.SelectDateRange, title: ALERT_TITLE_APP_NAME)
			return
		}
		self.startLoading()
		createBooking.startDate = startDate
		createBooking.endDate = endDate
		let dateFormatter = DateFormatter()
        if let timezone = TimeZone(abbreviation: "UTC") {
            dateFormatter.timeZone = timezone
        }
		dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss a"
		createBooking.startDateString = dateFormatter.string(from: startDate)
		createBooking.endDateString = dateFormatter.string(from: endDate)
		API.shared.getCarListingAvailability(listId: id, startDate: createBooking.startDateString, endDate: createBooking.endDateString) { (stats, message) in
			self.stopLoading()
			if stats {
				self.moveNext()
			} else {
				self.showAlertView(message: message, title: ALERT_TITLE_APP_NAME)
			}
		}
	}
	
	private func selectTime() {
		self.view.addSubview(self.datePicker)
		self.view.addSubview(self.toolBar)
		
		NSLayoutConstraint.activate([
			self.datePicker.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
			self.datePicker.leftAnchor.constraint(equalTo: self.view.leftAnchor),
			self.datePicker.rightAnchor.constraint(equalTo: self.view.rightAnchor),
			self.datePicker.heightAnchor.constraint(equalToConstant: 200)
		])
		
		NSLayoutConstraint.activate([
			self.toolBar.bottomAnchor.constraint(equalTo: self.datePicker.topAnchor),
			self.toolBar.leftAnchor.constraint(equalTo: self.view.leftAnchor),
			self.toolBar.rightAnchor.constraint(equalTo: self.view.rightAnchor),
			self.toolBar.heightAnchor.constraint(equalToConstant: 50)
		])
	}
	
	//MARK: Actions
	@IBAction
	func actionBack(_ sender : UIButton){
		self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction
	func actionNext(_ sender : UIButton){
		if checkInternetConnection() {
			createAvailabilityRequest()
		}
	}
	
	@objc
	func onDoneButtonClick() {
		toolBar.removeFromSuperview()
		datePicker.removeFromSuperview()
		let date = datePicker.date
		let calendar = Calendar.current
		let hour = calendar.component(.hour, from: date)
		let minute = calendar.component(.minute, from: date)
		let second = calendar.component(.second, from: date)
		if endDate == nil {
			startDate = Calendar.current.date(bySettingHour: hour, minute: minute, second: second, of: startDate!)!
		} else {
			endDate = Calendar.current.date(bySettingHour: hour, minute: minute, second: second, of: endDate!)!
		}
	}
	
	@objc
	func oncancelButtonClick() {
		toolBar.removeFromSuperview()
		datePicker.removeFromSuperview()
	}
	
}

//MARK: GLCalendarViewDelegate Methods
extension CarAvailabilityViewController : GLCalendarViewDelegate {
	func calenderView(_ calendarView: GLCalendarView!, canAddRangeWithBegin beginDate: Date!) -> Bool {
		return true
	}
	
	func calenderView(_ calendarView: GLCalendarView!, rangeToAddWithBegin beginDate: Date!) -> GLCalendarDateRange! {
        if beginDate < Calendar.current.startOfDay(for: Date()) {
			range.beginDate = GLDateUtils.date(byAddingDays: -30, to: beginDate)
			range.endDate = GLDateUtils.date(byAddingDays: -30, to: beginDate)
			return range
		}
		if startDate == nil {
			startDate = beginDate
			range = GLCalendarDateRange(begin: startDate, end: startDate)
			range.beginDate = startDate
			range.endDate = startDate
			range.backgroundColor = UIColor(hexFromString: "e39b00")
			range.editable = false
			selectTime()
			return range
		} else if endDate == nil {
			endDate = beginDate
			self.range.endDate = endDate
			selectTime()
			return range
		} else {
			startDate = beginDate
			calendarView.removeRange(range)
			//range = nil
			endDate = nil
			range = GLCalendarDateRange(begin: startDate, end: startDate)
			
			range.backgroundColor = UIColor(hexFromString: "e39b00")
			range.editable = false
			selectTime()
			return range
		}
	}
	
	func calenderView(_ calendarView: GLCalendarView!, beginToEdit range: GLCalendarDateRange!) {
		
	}
	
	func calenderView(_ calendarView: GLCalendarView!, finishEdit range: GLCalendarDateRange!, continueEditing: Bool) {
		
	}
	
	func calenderView(_ calendarView: GLCalendarView!, canUpdate range: GLCalendarDateRange!, toBegin beginDate: Date!, end endDate: Date!) -> Bool {
		return true
	}
	
	func calenderView(_ calendarView: GLCalendarView!, didUpdate range: GLCalendarDateRange!, toBegin beginDate: Date!, end endDate: Date!) {
		print("didUpdate")
	}
	
	
}
