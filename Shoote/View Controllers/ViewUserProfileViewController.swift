//
//  ViewUserProfileViewController.swift
//  Shoote
//
//  Created by BrainX IOS 3 on 21/04/2021.
//  Copyright © 2021 Digi Dev. All rights reserved.
//

import UIKit
import Cosmos

class ViewUserProfileViewController: BaseViewController {
	
	//MARK: Properties
	var host: BusinessProfile?
	var renter: ShooteUser?
	var reviews: [Review]?
	var userType = ""
	var id = 0

	//MARK: Outlets
	@IBOutlet weak var lblTitle : UILabel!
	@IBOutlet weak var lblName : UILabel!
	@IBOutlet weak var viewRating : CosmosView!
	@IBOutlet weak var lblMemberSince : UILabel!
	@IBOutlet weak var lblLocation : UILabel!
	@IBOutlet weak var imgProfile : UIImageView!
	@IBOutlet weak var lblAbout : UILabel!
	@IBOutlet weak var lblBio : UILabel!
	@IBOutlet weak var btnContact : UIButton!
	@IBOutlet weak var btnViewListing : UIButton!
	@IBOutlet weak var stackButtons : UIStackView!
	@IBOutlet weak var stackcheckBoxes : UIStackView!
	@IBOutlet weak var viewIdentityVerified : UIView!
	@IBOutlet weak var viewProvidedServices : UIView!
	@IBOutlet weak var viewMultipleListings : UIView!
	@IBOutlet weak var viewPositiveReviews : UIView!
	@IBOutlet weak var viewCompletedRentals : UIView!
	@IBOutlet weak var lblCompletedRentals : UILabel!
	@IBOutlet weak var lblTotalReviews : UILabel!
	@IBOutlet weak var tblReviews : UITableView!
	@IBOutlet weak var viewMain : UIView!
	@IBOutlet weak var stackCheckBoxedHeightConstraint : NSLayoutConstraint!
	
	//MARK: Override Methods
	override func viewDidLoad() {
		super.viewDidLoad()
		viewMain.isHidden = true
		if userType == "renter" {
			lblTitle.text = "Renter Profile"
			stackcheckBoxes.isHidden = true
			btnViewListing.isHidden = true
			btnContact.isHidden = true
			btnContact.setTitle("Contact Renter", for: .normal)
			
		} else {
			lblTitle.text = "Host Profile"
		}
		if host != nil {
			setupDataForHost()
		} else {
			if userType == "renter" {
				getRenterData()
			} else {
				getHostData()
			}
		}
	}
	
	//MARK: Private Methods
	private func setupDataForRenter() {
		
		guard let user = self.renter else {
			return
		}
		
		lblName.text = user.fullName
		if userType == "renter" {
			viewRating.rating = user.ratingAvg.toDouble()
		} else {
			viewRating.rating = user.ratingAvg.toDouble()
		}
		lblMemberSince.text = "Member Since \(Utilities.utcToLocal(dateString: user.createdAt))"
		lblLocation.text = user.physicalAddress.city + ", " + user.physicalAddress.state + ", " + user.physicalAddress.zip
		let url = EndPoints.BASE_URL_IMG + user.profileImage
		setImageWithUrl(imageView: imgProfile, url: url,placeholderImage: "user-large")
		
		lblAbout.text = "About \(user.fullName.capitalized)"
		lblBio.text = user.bio
		//viewIdentityVerified.isHidden = !user.isVerified
		//viewProvidedServices.isHidden = !user.providedService
		//viewMultipleListings.isHidden = !user.multipleListings
		//viewPositiveReviews.isHidden = user.ratingAvg < 4
		//viewCompletedRentals.isHidden = !user.tenCompletedRentals
		
		if userType == "renter" {
			reviews = user.reviews
		} else {
			reviews = user.reviews
		}
		
		tblReviews.reloadData()
		
		lblTotalReviews.text = "\(reviews?.count ?? 0) Reviews"
		viewMain.isHidden = false
		stackCheckBoxedHeightConstraint.constant = getCheckBoxHeight()
	}
	
	private func setupDataForHost() {
		
		guard let user = self.host else {
			return
		}
		
		lblName.text = user.title
		if userType == "renter" {
			viewRating.rating = user.ratingAvg.toDouble()
		} else {
			viewRating.rating = user.ratingAvg.toDouble()
		}
		lblMemberSince.text = "Member Since \(Utilities.utcToLocal(dateString: user.createdAt))"
		lblLocation.text = user.physicalAddress.city + ", " + user.physicalAddress.state + ", " + user.physicalAddress.zip
		let url = EndPoints.BASE_URL_IMG + user.profileImage
		setImageWithUrl(imageView: imgProfile, url: url,placeholderImage: "user-large")
		
		lblAbout.text = "About \(user.title.capitalized)"
		lblBio.text = user.description
		viewIdentityVerified.isHidden = !user.isVerified
		viewProvidedServices.isHidden = !user.providedService
		viewMultipleListings.isHidden = !user.multipleListings
		viewPositiveReviews.isHidden = user.ratingAvg < 4
		viewCompletedRentals.isHidden = !user.tenCompletedRentals
		
		if userType == "renter" {
			reviews = user.reviews
		} else {
			reviews = user.reviews
		}
		
		tblReviews.reloadData()
		
		lblTotalReviews.text = "\(reviews?.count ?? 0) Reviews"
		viewMain.isHidden = false
		stackCheckBoxedHeightConstraint.constant = getCheckBoxHeight()
	}
	
	private func getHostData(){
		self.startLoading()
		API.shared.getBusinessProfileById(id: id) { (status, msg, profile) in
			self.stopLoading()
			self.host = profile
			self.setupDataForHost()
		}
	}
	
	private func getRenterData(){
		self.startLoading()
		API.shared.getShooteUserById(id: id) { (status, msg, profile) in
			self.stopLoading()
			self.renter = profile
			self.setupDataForRenter()
		}
	}
	
	private func getCheckBoxHeight() ->CGFloat {
		var height: CGFloat = 0
		guard let user = self.host, userType != "renter"  else {
			return height
		}
		if user.isVerified {
			height += 38
		}
		if user.ratingAvg > 4 {
			height += 38
		}
		if user.providedService {
			height += 38
		}
		if user.multipleListings {
			height += 38
		}
		if user.tenCompletedRentals {
			height += 38
		}
		
		return height
	}
	
	private func openConversation(with id: Int) {
		self.startLoading()
		API.shared.openConversation(id: id) { status, message, conversation in
			self.stopLoading()
			if status {
				let vc = UIViewController.instantiate(ChatDetailsViewController.self, fromStoryboard: .Main)
				vc.conversation = conversation
				self.navigationController?.pushViewController(vc, animated: true)
			} else {
				self.showAlertView(message: message, title: ALERT_TITLE_APP_NAME)
			}
		}
	}

	//MARK: Actions
	@IBAction
	func actionBack(_ sender : UIButton){
		self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction
	func actionContactHost(_ sender : UIButton){
		let userId = userType == "renter" ? renter?.businessProfileId : host?.id
		guard let id = userId else {
			showAlertView(message: "Loading Profile in Background.", title: ALERT_TITLE_APP_NAME)
			return
		}
		openConversation(with: id)
		
	}
	
	@IBAction
	func actionViewListing(_ sender : UIButton){
		guard let listing = host?.carListings, listing.count != 0 else {
			self.showAlertView(message: ALERT_TITLE_APP_NAME, title: "Listigs not found")
			return
		}
		let vc = UIViewController.instantiate(CarsNearViewController.self, fromStoryboard: .Main)
		vc.carListing = listing
		self.navigationController?.pushViewController(vc, animated: true)
	}
}

//MARK: UITableViewDataSource and UITableViewDelegate Delegate Methods
extension ViewUserProfileViewController : UITableViewDataSource, UITableViewDelegate {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return reviews?.count ?? 0
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewItemTableViewCell") as! ReviewItemTableViewCell
		if let review = reviews?[indexPath.row] {
			cell.setData(review)
		}
		return cell
	}
	
//	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//		return 118
//	}
}
