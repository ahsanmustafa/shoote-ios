//
//  SelectTimingsViewController.swift
//  Shoote
//
//  Created by BrainX IOS 3 on 31/12/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit

class SelectTimingsViewController: BaseViewController {

	//MARK: Outlets
	@IBOutlet weak var alertSelectTiming: UIView!
	private var startTimingDD : DropDown!
	@IBOutlet weak var startTiming: UITextField!
	@IBOutlet weak var startTimingViewDD: UIView!
	private var endTimingDD : DropDown!
	@IBOutlet weak var endTiming: UITextField!
	@IBOutlet weak var endTimingViewDD: UIView!
	
	//MARK: Properties
	private var hoursOfOperations = [Int:[String]]()
	private var notAvailable = [Int:Bool]()
	var selectTimeCurrentDay : Int = 0
	var selectedButton : UIButton?
	
	//MARK: Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
		updateViews()
    }
	
	//MARK: Private Methods
	private func updateViews() {
		startTiming.delegate = self
		endTiming.delegate = self
		let paddingViewstartTiming = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: self.startTiming.frame.height))
		startTiming.leftView = paddingViewstartTiming
		startTiming.leftViewMode = UITextField.ViewMode.always
		
		let paddingViewendTiming = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: self.endTiming.frame.height))
		endTiming.leftView = paddingViewendTiming
		endTiming.leftViewMode = UITextField.ViewMode.always
		
		startTimingDD = DropDown(anchorView: startTimingViewDD, selectionAction: { (index, item) in
			self.startTiming.text = item
		}, dataSource: Utilities.getStartTimingList(), topOffset: nil, bottomOffset: nil, cellConfiguration: nil, cancelAction: nil)
		view.bringSubviewToFront(startTiming)
		
		endTimingDD = DropDown(anchorView: endTimingViewDD, selectionAction: { (index, item) in
			self.endTiming.text = item
		}, dataSource: Utilities.getEndTimingList(), topOffset: nil, bottomOffset: nil, cellConfiguration: nil, cancelAction: nil)
		view.bringSubviewToFront(endTiming)
		GCD.async(.Main, delay: 0.5) {
			
			self.endTimingDD.selectRow(at: 0)
			self.endTimingDD.selectionAction?(0, (Utilities.getEndTimingList()[0]))
			
			self.startTimingDD.selectRow(at: 0)
			self.startTimingDD.selectionAction?(0, (Utilities.getStartTimingList()[0]))
			
		}
	}
	
	private func toggleStartTimingListDD() {
		if startTimingDD.isHidden {
			startTimingDD.show()
		}
	}
	
	private func toggleEndTimingListDD() {
		if endTimingDD.isHidden {
			endTimingDD.show()
		}
	}
	
	//MARK: Actions
	@IBAction func notAvailableTiming(_ sender : UIButton){
		alertSelectTiming.superview?.isHidden = true
		hoursOfOperations.removeValue(forKey: selectTimeCurrentDay)
		notAvailable[selectTimeCurrentDay] = true
		selectedButton?.setTitle("Not Available", for: .normal)
		selectTimeCurrentDay = 0
		self.dismiss(animated: false, completion: nil)
	}
	
	@IBAction func confirmTiming(_ sender : UIButton) {
		if startTimingDD.selectedItem == nil
		{
			self.showAlertView(message: "Select Start Time",title: ALERT_TITLE_APP_NAME)
			return
		}
		
		if endTimingDD.selectedItem == nil
		{
			self.showAlertView(message: "Select Close Time",title: ALERT_TITLE_APP_NAME)
			return
		}
		
		alertSelectTiming.superview?.isHidden = true
		hoursOfOperations[selectTimeCurrentDay] = [startTiming.text!, endTiming.text!]
		notAvailable[selectTimeCurrentDay] = false
		selectedButton?.setTitle("\(startTiming.text!)  -  \(endTiming.text!)", for: .normal)

		selectTimeCurrentDay = 0
		self.dismiss(animated: false, completion: nil)
	}
}

extension SelectTimingsViewController : UITextFieldDelegate {
	func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
		if textField == startTiming
		{
			toggleStartTimingListDD()
			return false
		}
		else if textField == endTiming
		{
			toggleEndTimingListDD()
			return false
		}
		return true
	}
}
