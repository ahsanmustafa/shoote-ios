//
//  RatingReviewDialogueViewController.swift
//  Shoote
//
//  Created by Faheem on 21/02/2021.
//  Copyright © 2021 Digi Dev. All rights reserved.
//

import UIKit
import Cosmos
import KMPlaceholderTextView

protocol RatingReviewDialogueDelegate:AnyObject {
    func didSelectRating(review: Review)
}

class RatingReviewDialogueViewController: BaseViewController {
	
	//MARK: Properties
	weak var delegate: RatingReviewDialogueDelegate?
	var text = ""
	var acuracyTitle = ""
	
	//MARK: Outlets
    @IBOutlet weak var lblHeading : UILabel!
	@IBOutlet weak var lblListingAccuracy : UILabel!
    @IBOutlet weak var ratingCommunication : CosmosView!
    @IBOutlet weak var ratingCleanliness : CosmosView!
    @IBOutlet weak var ratingListingAccuracy : CosmosView!
    @IBOutlet weak var ratingPunctuality : CosmosView!
    @IBOutlet weak var ratingRecommend : CosmosView!
    @IBOutlet weak var txtReview : KMPlaceholderTextView!
    @IBOutlet weak var tapTopView : UIView!
    @IBOutlet weak var tapBootomView : UIView!
    
	//MARK: Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        updateViews()
    }
	
	//MARK: Private Methods
	private func updateViews() {
		lblHeading.text = text
		lblListingAccuracy.text = acuracyTitle
		let tap = UITapGestureRecognizer(target: self, action: #selector(tapToDisiss(_:)))
		let tapBottom = UITapGestureRecognizer(target: self, action: #selector(tapToDisiss(_:)))
		tapTopView.addGestureRecognizer(tap)
		tapBootomView.addGestureRecognizer(tapBottom)
	}
    
	//MARK: Actions
    @IBAction
	func actionConfirm(_ sender : UIButton){
		let review = Review()
		review.reviewDescription = txtReview.text
		review.communicationRating = Float(ratingCommunication.rating)
		review.cleanlinessRating = Float(ratingCleanliness.rating)
		review.accuracyRating = Float(ratingListingAccuracy.rating)
		review.recommendRating = Float(ratingRecommend.rating)
		review.timingRating = Float(ratingPunctuality.rating)
        dismiss(animated: false) { [self] in
            delegate?.didSelectRating(review: review)
        }
    }
    
    @objc
	func tapToDisiss(_ tap: UITapGestureRecognizer) {
        dismiss(animated: false, completion: nil)
    }
}
