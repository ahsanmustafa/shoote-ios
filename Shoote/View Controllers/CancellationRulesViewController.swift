//
//  CancellationRulesViewController.swift
//  Shoote
//
//  Created by Faheem on 25/07/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit

class CancellationRulesViewController: BaseViewController {

	//MARK: Properties
	
	//MARK: Outlets
    @IBOutlet weak var imgFlexible : UIImageView!
    @IBOutlet weak var imgStrict : UIImageView!
    @IBOutlet weak var imgVeryStrict : UIImageView!
    
	//MARK: Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.flexibleTapped(_:)))
        imgFlexible.isUserInteractionEnabled = true
        imgFlexible.addGestureRecognizer(tap)
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.strictTapped(_:)))
        imgStrict.isUserInteractionEnabled = true
        imgStrict.addGestureRecognizer(tap1)
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.varyStrictTapped(_:)))
        imgVeryStrict.isUserInteractionEnabled = true
        imgVeryStrict.addGestureRecognizer(tap2)
    }
	
	//MARK: Private Methods
    private func resetAll(){
        imgFlexible.image = UIImage(named: "circle-empty")!
        imgStrict.image = UIImage(named: "circle-empty")!
        imgVeryStrict.image = UIImage(named: "circle-empty")!
    }
	
	//MARK: Actions
    @objc
	func flexibleTapped(_ sender : UITapGestureRecognizer){
        resetAll()
        imgFlexible.image = UIImage(named: "circle-fill")!
    }
	
    @objc
	func strictTapped(_ sender : UITapGestureRecognizer){
        resetAll()
        imgStrict.image = UIImage(named: "circle-fill")!
    }
	
    @objc
	func varyStrictTapped(_ sender : UITapGestureRecognizer){
        resetAll()
        imgVeryStrict.image = UIImage(named: "circle-fill")!
    }
	
    @IBAction
	func actionBack(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }

}
