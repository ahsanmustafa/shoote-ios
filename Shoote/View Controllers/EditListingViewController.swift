//
//  EditListingViewController.swift
//  Shoote
//
//  Created by BrainX IOS 3 on 27/12/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import DLRadioButton
import SimpleCheckbox
import DKImagePickerController
import ImageSlideshow

class EditListingViewController: BaseViewController, UITextFieldDelegate {

	//MARK: Properties
	private var hoursOfOperations = [Int:[String]]()
	private var notAvailable = [Int:Bool]()
	var createListingRequest = [String: Any]()
	var createListingImages = [UIImage]()
	private var radiusDD : DropDown!
	private var milesIndex = 0
	var aar = [InputSource]()
	lazy var imagePicker : DKImagePickerController = {
		return DKImagePickerController()
	}()
	var data : CarListing!
	
	//MARK: Section 1 Outlets
	@IBOutlet weak var slideShow : ImageSlideshow!
	@IBOutlet weak var imgCar: UIImageView!
	@IBOutlet weak var btnInstant: DLRadioButton!
	@IBOutlet weak var btnManual: DLRadioButton!
	@IBOutlet weak var txtNameOrTitle: SkyFloatingLabelTextField!
	@IBOutlet weak var txtCity: SkyFloatingLabelTextField!
	@IBOutlet weak var txtState: SkyFloatingLabelTextField!
	@IBOutlet weak var txtZip: SkyFloatingLabelTextField!
	@IBOutlet weak var txtDescription: UITextView!
	@IBOutlet weak var viewSection1: UIView!
    @IBOutlet weak var viewRadiusDropDown: UIView!
    @IBOutlet weak var lblRaiusValue: UILabel!
	
	//MARK: Section 3 Outlets
	@IBOutlet weak var txtPolicies: UITextView!
	@IBOutlet weak var btnOwnerPresenseYes: DLRadioButton!
	@IBOutlet weak var btnOwnerPresenseNo: DLRadioButton!
	@IBOutlet weak var btnOwnerPresenseCoHost: DLRadioButton!
	@IBOutlet weak var btnDriveYes: DLRadioButton!
	@IBOutlet weak var btnDriveNo: DLRadioButton!
	
	@IBOutlet weak var videoCheck : Checkbox!
	@IBOutlet weak var filmCheck : Checkbox!
	@IBOutlet weak var photoCheck : Checkbox!
	@IBOutlet weak var commercialCheck : Checkbox!
	@IBOutlet weak var movieCheck : Checkbox!
	
	@IBOutlet weak var flexibleRadio : DLRadioButton!
	@IBOutlet weak var strictRadio : DLRadioButton!
	@IBOutlet weak var veryStrictRadio : DLRadioButton!
	
	//MARK: Section 4 Outlets
	@IBOutlet weak var hourlyRate : UITextField!

	private var minHoursDD : DropDown!
	@IBOutlet weak private var minHours: UITextField!
	@IBOutlet weak private var minHoursViewDD: UIView!
	
	private var discountDD : DropDown!
	@IBOutlet weak private var discount: UITextField!
	@IBOutlet weak private var discountViewDD: UIView!
	
	@IBOutlet weak private var discountPercentage: UITextField!
	
	private var milesDD : DropDown!
	@IBOutlet weak private var miles: UITextField!
	@IBOutlet weak private var milesViewDD: UIView!
	
	
	@IBOutlet weak private var deliveryFee: UITextField!
	@IBOutlet weak private var cleaning: UITextField!
	@IBOutlet weak private var late: UITextField!
	@IBOutlet weak private var paint: UITextField!
	@IBOutlet weak private var smoke: UITextField!
	@IBOutlet weak private var damage: UITextField!
	
	
	private var conditionDD : DropDown!
	@IBOutlet weak private var condition: UITextField!
	@IBOutlet weak private var conditionViewDD: UIView!

	private var exteriorColorDD : DropDown!
	@IBOutlet weak private var exteriorColor: UITextField!
	@IBOutlet weak private var exteriorColorViewDD: UIView!
	
	private var interiorColorDD : DropDown!
	@IBOutlet weak private var interiorColor: UITextField!
	@IBOutlet weak private var interiorColorViewDD: UIView!
	
	private var typeOfCarDD : DropDown!
	@IBOutlet weak private var typeOfCar: UITextField!
	@IBOutlet weak private var typeOfCarViewDD: UIView!
	
	@IBOutlet weak private var year: UITextField!
	@IBOutlet weak private var make: SkyFloatingLabelTextField!
	@IBOutlet weak private var model: SkyFloatingLabelTextField!
	
	private var transmissionDD : DropDown!
	@IBOutlet weak private var transmission: UITextField!
	@IBOutlet weak private var transmissionViewDD: UIView!
	
	private var bodyStyleDD : DropDown!
	@IBOutlet weak private var bodyStyle: UITextField!
	@IBOutlet weak private var bodyStyleViewDD: UIView!
	@IBOutlet weak private var maxCapacity: UITextField!
	
	@IBOutlet weak private var monday: UIButton!
	@IBOutlet weak private var tuesday: UIButton!
	@IBOutlet weak private var wednesday: UIButton!
	@IBOutlet weak private var thrusday: UIButton!
	@IBOutlet weak private var friday: UIButton!
	@IBOutlet weak private var saturday: UIButton!
	@IBOutlet weak private var sunday: UIButton!
	
	@IBOutlet weak var viewSection2: UIView!
	@IBOutlet weak var viewSection3: UIView!
	@IBOutlet weak var viewSection4: UIView!
	
	//MARK: Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
		
		viewSection1.roundCorners([.bottomRight,.topRight], radius: 20)
		viewSection2.roundCorners([.bottomRight,.topRight], radius: 20)
		viewSection3.roundCorners([.bottomRight,.topRight], radius: 20)
		viewSection4.roundCorners([.bottomRight,.topRight], radius: 20)
		condition.delegate = self
		exteriorColor.delegate = self
		interiorColor.delegate = self
		typeOfCar.delegate = self
		year.delegate = self
		make.delegate = self
		model.delegate = self
		transmission.delegate = self
		bodyStyle.delegate = self
		maxCapacity.delegate = self
		videoCheck.borderStyle = .square
		videoCheck.checkmarkStyle = .tick
		videoCheck.checkmarkColor = .black
		videoCheck.checkedBorderColor = .clear
		videoCheck.checkboxFillColor = .checkColor
		
		photoCheck.borderStyle = .square
		photoCheck.checkmarkStyle = .tick
		photoCheck.checkmarkColor = .black
		photoCheck.checkedBorderColor = .clear
		photoCheck.checkboxFillColor = .checkColor
		
		commercialCheck.borderStyle = .square
		commercialCheck.checkmarkStyle = .tick
		commercialCheck.checkmarkColor = .black
		commercialCheck.checkedBorderColor = .clear
		commercialCheck.checkboxFillColor = .checkColor
		
		filmCheck.borderStyle = .square
		filmCheck.checkmarkStyle = .tick
		filmCheck.checkmarkColor = .black
		filmCheck.checkedBorderColor = .clear
		filmCheck.checkboxFillColor = .checkColor
		
		movieCheck.borderStyle = .square
		movieCheck.checkmarkStyle = .tick
		movieCheck.checkmarkColor = .black
		movieCheck.checkedBorderColor = .clear
		movieCheck.checkboxFillColor = .checkColor
		let paddingViewCondition = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: self.condition.frame.height))
		condition.leftView = paddingViewCondition
		condition.leftViewMode = UITextField.ViewMode.always
		
		let paddingViewexteriorColor = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: self.exteriorColor.frame.height))
		exteriorColor.leftView = paddingViewexteriorColor
		exteriorColor.leftViewMode = UITextField.ViewMode.always
		
		let paddingViewInteriorColor = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: self.interiorColor.frame.height))
		interiorColor.leftView = paddingViewInteriorColor
		interiorColor.leftViewMode = UITextField.ViewMode.always
		
		let paddingViewtypeOfCar = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: self.typeOfCar.frame.height))
		typeOfCar.leftView = paddingViewtypeOfCar
		typeOfCar.leftViewMode = UITextField.ViewMode.always
		
		let paddingViewYear = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: self.year.frame.height))
		year.leftView = paddingViewYear
		year.leftViewMode = UITextField.ViewMode.always
		
		let paddingViewTransmission = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: self.transmission.frame.height))
		transmission.leftView = paddingViewTransmission
		transmission.leftViewMode = UITextField.ViewMode.always
		
		let paddingViewbodyStyle = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: self.bodyStyle.frame.height))
		bodyStyle.leftView = paddingViewbodyStyle
		bodyStyle.leftViewMode = UITextField.ViewMode.always
		
		let paddingViewMaxCapacity = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: self.maxCapacity.frame.height))
		maxCapacity.leftView = paddingViewMaxCapacity
		maxCapacity.leftViewMode = UITextField.ViewMode.always
		
		condition.attributedPlaceholder = NSAttributedString(string: "Not Selected", attributes: [NSAttributedString.Key.foregroundColor : UIColor.whiteColor])
		exteriorColor.attributedPlaceholder = NSAttributedString(string: "Not Selected", attributes: [NSAttributedString.Key.foregroundColor : UIColor.whiteColor])
		interiorColor.attributedPlaceholder = NSAttributedString(string: "Not Selected", attributes: [NSAttributedString.Key.foregroundColor : UIColor.whiteColor])
		typeOfCar.attributedPlaceholder = NSAttributedString(string: "Not Selected", attributes: [NSAttributedString.Key.foregroundColor : UIColor.whiteColor])
		transmission.attributedPlaceholder = NSAttributedString(string: "Not Selected", attributes: [NSAttributedString.Key.foregroundColor : UIColor.whiteColor])
		bodyStyle.attributedPlaceholder = NSAttributedString(string: "Not Selected", attributes: [NSAttributedString.Key.foregroundColor : UIColor.whiteColor])
		
		conditionDD = DropDown(anchorView: conditionViewDD, selectionAction: { (index, item) in
			self.condition.text = item
		}, dataSource: Utilities.getConditionList(), topOffset: nil, bottomOffset: nil, cellConfiguration: nil, cancelAction: nil)
		view.bringSubviewToFront(condition)
		
		exteriorColorDD = DropDown(anchorView: exteriorColorViewDD, selectionAction: { (index, item) in
			self.exteriorColor.text = item
		}, dataSource: Utilities.getColorList(), topOffset: nil, bottomOffset: nil, cellConfiguration: nil, cancelAction: nil)
		view.bringSubviewToFront(exteriorColor)
		
		interiorColorDD = DropDown(anchorView: interiorColorViewDD, selectionAction: { (index, item) in
			self.interiorColor.text = item
		}, dataSource: Utilities.getColorList(), topOffset: nil, bottomOffset: nil, cellConfiguration: nil, cancelAction: nil)
		view.bringSubviewToFront(interiorColor)
		
		typeOfCarDD = DropDown(anchorView: typeOfCarViewDD, selectionAction: { (index, item) in
			self.typeOfCar.text = item
		}, dataSource: Utilities.getTypeOfCarList(), topOffset: nil, bottomOffset: nil, cellConfiguration: nil, cancelAction: nil)
		view.bringSubviewToFront(typeOfCar)
		
		transmissionDD = DropDown(anchorView: transmissionViewDD, selectionAction: { (index, item) in
			self.transmission.text = item
		}, dataSource: Utilities.getTransmissionList(), topOffset: nil, bottomOffset: nil, cellConfiguration: nil, cancelAction: nil)
		view.bringSubviewToFront(transmission)
		
		bodyStyleDD = DropDown(anchorView: bodyStyleViewDD, selectionAction: { (index, item) in
			self.bodyStyle.text = item
		}, dataSource: Utilities.getBodyStyleList(), topOffset: nil, bottomOffset: nil, cellConfiguration: nil, cancelAction: nil)
		view.bringSubviewToFront(bodyStyle)
		
		minHoursDD = DropDown(anchorView: minHoursViewDD, selectionAction: { (index, item) in
			self.minHours.text = item
		}, dataSource: Utilities.getHoursList(), topOffset: nil, bottomOffset: nil, cellConfiguration: nil, cancelAction: nil)
		
		discountDD = DropDown(anchorView: discountViewDD, selectionAction: { (index, item) in
			self.discount.text = item
		}, dataSource: Utilities.getDiscountList(), topOffset: nil, bottomOffset: nil, cellConfiguration: nil, cancelAction: nil)
		
		milesDD = DropDown(anchorView: milesViewDD, selectionAction: { (index, item) in
			self.miles.text = item
		}, dataSource:Utilities.getMilesList(), topOffset: nil, bottomOffset: nil, cellConfiguration: nil, cancelAction: nil)
		
		minHours.delegate = self
		discount.delegate = self
		miles.delegate = self
		[txtNameOrTitle, txtCity, txtState, txtZip, make, model].forEach({$0.titleFormatter = { $0 }})
		populateData()
        
        radiusDD = DropDown(anchorView: viewRadiusDropDown, selectionAction: { (index, item) in
            self.milesIndex = index
            self.lblRaiusValue.text = item
		}, dataSource: Utilities.getRaduisMilesList(), topOffset: nil, bottomOffset: nil, cellConfiguration: nil, cancelAction: nil)
        
        if data.maxDeliveryRadius != 0 {
            milesIndex = (data.maxDeliveryRadius / 5) - 1
			self.lblRaiusValue.text = Utilities.getRaduisMilesList()[milesIndex]
        }
    }
	
	//MARK: Private Methods
	private func populateData() {
		guard let info = data else {
			return
		}
		//Section 1
		
		for src in info.listingImages {
			aar.append(KingfisherSource(urlString: EndPoints.BASE_URL_IMG + src.imagePath.replacingOccurrences(of: "\\", with: "/"))!)
		}
		
		slideShow.setImageInputs(aar)
		slideShow.slideshowInterval = 3.0
		
		if info.isManual {
			btnManual.isSelected = true
		} else {
			btnInstant.isSelected = true
		}
		txtNameOrTitle.text = info.name
		txtCity.text = info.location.city
		txtState.text = info.location.state
		txtZip.text = info.location.zip
		txtDescription.text = info.description
		if let short = info.listingImages.first?.imagePath {
			let path = EndPoints.BASE_URL_IMG + short
			setImageWithUrl(imageView: imgCar, url: path,placeholderImage: "img_car_placeholder")
		}
		
		//Sections 2
		let conIndex = Utilities.getConditionList().firstIndex(of: info.condition ) ?? 0
		conditionDD.selectRow(at: conIndex)
		conditionDD.selectionAction?(conIndex, (Utilities.getConditionList()[conIndex]))
		
		let exteriorColorIndex = Utilities.getColorList().firstIndex(of: info.exteriorColor ) ?? 0
		exteriorColorDD.selectRow(at: exteriorColorIndex)
		exteriorColorDD.selectionAction?(exteriorColorIndex, (Utilities.getColorList()[exteriorColorIndex]))
		
		let interiorColorIndex = Utilities.getColorList().firstIndex(of: info.interiorColor ) ?? 0
		interiorColorDD.selectRow(at: interiorColorIndex)
		interiorColorDD.selectionAction?(interiorColorIndex, (Utilities.getColorList()[interiorColorIndex]))
		
		let typeIndex = Utilities.getTypeOfCarList().firstIndex(of: info.typeOfCar ) ?? 0
		typeOfCarDD.selectRow(at: typeIndex)
		typeOfCarDD.selectionAction?(typeIndex, (Utilities.getTypeOfCarList()[typeIndex]))
		
		year.text = String(info.year )
		make.text = info.make
		model.text = info.model
		
		let transIndex = Utilities.getTransmissionList().firstIndex(of: info.transmission ) ?? 0
		transmissionDD.selectRow(at: transIndex)
		transmissionDD.selectionAction?(transIndex, (Utilities.getTransmissionList()[transIndex]))
		
		let bodyIndex = Utilities.getBodyStyleList().firstIndex(of: info.bodyType ) ?? 0
		bodyStyleDD.selectRow(at: bodyIndex)
		bodyStyleDD.selectionAction?(bodyIndex, (Utilities.getBodyStyleList()[bodyIndex]))
		
		maxCapacity.text = info.maxCapacity
		monday.setTitle(info.mondayTiming , for: .normal)
		tuesday.setTitle(info.tuesdayTiming , for: .normal)
		wednesday.setTitle(info.wednesdayTiming , for: .normal)
		thrusday.setTitle(info.thursdayTiming , for: .normal)
		friday.setTitle(info.fridayTiming , for: .normal)
		saturday.setTitle(info.saturdayTiming , for: .normal)
		sunday.setTitle(info.sundayTiming , for: .normal)
		
		//Sections 3
		txtPolicies.text = info.policy
		
		// 0 = No, 1 = Yes, 2 = Co Host
		switch info.ownerPresence {
		case 0:
			btnOwnerPresenseNo.isSelected = true
		case 1:
			btnOwnerPresenseYes.isSelected = true
		case 2:
			btnOwnerPresenseCoHost.isSelected = true
		default:
			break
		}
		
		// 0 = No, 1 = Yes
		switch info.userDrive {
		case 0:
			btnDriveNo.isSelected = true
		case 1:
			btnDriveYes.isSelected = true
		default:
			break
		}
		
		// 0 = Flexible, 1 = Strict, 2 = Very Strict
		switch info.cancellation {
		case 0:
			flexibleRadio.isSelected = true
		case 1:
			strictRadio.isSelected = true
		case 2:
			veryStrictRadio.isSelected = true
		default:
			break
		}
		
		if info.carHostActivity.contains("Video Shoot"){
			videoCheck.isChecked = true
		}
		if info.carHostActivity.contains("Film Shoot"){
			filmCheck.isChecked = true
		}
		if info.carHostActivity.contains("Photo Shoot"){
			photoCheck.isChecked = true
		}
		if info.carHostActivity.contains("Commercials"){
			commercialCheck.isChecked = true
		}
		if info.carHostActivity.contains("Movie Scene"){
			movieCheck.isChecked = true
		}
		
		//section 4
		hourlyRate.text = data.hourlyRate.toString()
		let minHours = Utilities.getHoursList().firstIndex(of: "\(info.minHours ) hr") ?? 0
		self.minHoursDD.selectRow(at: minHours)
		self.minHoursDD.selectionAction?(minHours, (Utilities.getHoursList()[minHours]))
		
		let dicHours = Utilities.getHoursList().firstIndex(of: "\(info.discountTime ) hr") ?? 0
		self.discountDD.selectRow(at: dicHours)
		self.discountDD.selectionAction?(dicHours, (Utilities.getDiscountList()[dicHours]))
		
		let addFee = info.additionalFees
		
		for fee in addFee {
			let type = fee.type
			switch type {
			case "Delivery":
				let miles = Utilities.getMilesList().firstIndex(of: "\(fee.durationOrDistance ) hr") ?? 0
				self.milesDD.selectRow(at: miles)
				self.milesDD.selectionAction?(miles, (Utilities.getMilesList()[miles]))
				self.deliveryFee.text = String(fee.amount)
			case "Cleaning Fee":
				cleaning.text = fee.amount.toString()
			case "Late Penalty Fee":
				late.text = fee.amount.toString()
			case "Smoking Fee":
				smoke.text = fee.amount.toString()
			case "Paint Damage":
				paint.text = fee.amount.toString()
			case "Interior Tear Damage":
				damage.text = fee.amount.toString()
			default:
				break
			}
		}
		
	}
	
	private func checkValidations() -> Bool {
		var isValid = true
		
		if txtNameOrTitle.text!.isEmpty {
			isValid = false
		} else if txtCity.text!.isEmpty {
			isValid = false
		} else if txtState.text!.isEmpty {
			isValid = false
		} else if txtZip.text!.isEmpty {
			isValid = false
		} else if txtDescription.text!.isEmpty {
			isValid = false
		} else if year.text!.isEmpty {
			isValid = false
		} else if make.text!.isEmpty {
			isValid = false
		} else if model.text!.isEmpty {
			isValid = false
		} else if maxCapacity.text!.isEmpty {
			isValid = false
		} else if txtPolicies.text!.isEmpty {
			isValid = false
		} else if hourlyRate.text!.isEmpty {
			isValid = false
		} else if discountPercentage.text!.isEmpty {
			isValid = false
		}
		if !isValid {
			self.showAlertView(message: "Input all mandatory fields to continue.", title: ALERT_TITLE_APP_NAME)
		}
		return isValid
	}
	
	//MARK: DropDown code
	
	private func toggleConditionListDD() {
		if conditionDD.isHidden {
			conditionDD.show()
		}
	}
	
	private func toggleExteriorColorListDD() {
		if exteriorColorDD.isHidden{
			exteriorColorDD.show()
		}
	}
	
	private func toggleInteriorColorListDD() {
		if interiorColorDD.isHidden{
			interiorColorDD.show()
		}
	}
	
	private func toggleTypeOfCarListDD() {
		if typeOfCarDD.isHidden {
			typeOfCarDD.show()
		}
	}
	
	private func toggleTransmissionListDD() {
		if transmissionDD.isHidden {
			transmissionDD.show()
		}
	}
	
	private func toggleBodyStyleListDD() {
		if bodyStyleDD.isHidden
		{
			bodyStyleDD.show()
		}
	}
	
	private func toggleHoursDD(){
		if minHoursDD.isHidden
		{
			minHoursDD.show()
		}
	}
	
	private func toggleDiscountDD()
	{
		if discountDD.isHidden
		{
			discountDD.show()
		}
	}
	
	private func toggleMilesDD()
	{
		if milesDD.isHidden
		{
			milesDD.show()
		}
	}
	
	//MARK: Actions
	@IBAction
	func pickImage(_ sender : UIButton){
		imagePicker.allowSwipeToSelect = true
		imagePicker.maxSelectableCount = 10
		imagePicker.delegate = self
		imagePicker.didSelectAssets = { (assets: [DKAsset]) in
			self.createListingImages = [UIImage]()
			for ass in assets
			{
				
				ass.fetchOriginalImage(completeBlock: { (ui, info) in
					if let ui = ui {
						self.createListingImages.append(ui)
						self.aar.append(ImageSource(image: ui))
						self.slideShow.setImageInputs(self.aar)
					}
				})
			}
			
		}
		
		self.present(imagePicker, animated: true, completion: nil)
	}
	
	@IBAction
	func actionSelectTime(_ sender : UIButton){
		let vc = UIViewController.instantiate(SelectTimingsViewController.self, fromStoryboard: .CarDetails)
		vc.modalPresentationStyle = .overFullScreen
		vc.selectedButton = sender
		present(vc, animated: false, completion: nil)
	}
	
    @IBAction
	func actionShowDropDown(_ sender : UIButton){
        radiusDD.show()
    }
    
	@IBAction
	func submit(_ sender : UIButton){
		if checkValidations(){
			// section 1
			createListingRequest["Id"] = data.id 
			createListingRequest["Name"] = txtNameOrTitle.text ?? kBlankString
			let location = ["City": txtCity.text ?? kBlankString,
							"State": txtState.text ?? kBlankString,
							"Zip": txtZip.text ?? kBlankString]
			createListingRequest["Location"] = location
			createListingRequest["Description"] = txtDescription.text ?? kBlankString
			createListingRequest["IsManual"] = btnManual.isSelected
            createListingRequest["MaxDeliveryRadius"] = (milesIndex + 1) * 5
			
			//section 2
			createListingRequest["Make"] = make.text
			createListingRequest["Model"] = model.text
			createListingRequest["Condition"] = condition.text ?? kBlankString
			createListingRequest["ExteriorColor"] = exteriorColor.text ?? kBlankString
			createListingRequest["InteriorColor"] = interiorColor.text ?? kBlankString
			createListingRequest["TypeOfCar"] = typeOfCar.text ?? kBlankString
			createListingRequest["Year"] = year.text ?? kBlankString
			createListingRequest["Transmission"] = transmission.text ?? kBlankString
			createListingRequest["BodyType"] = bodyStyle.text ?? kBlankString
			createListingRequest["MaxCapacity"] = maxCapacity.text ?? kBlankString
			
			//Timing
			createListingRequest["MondayTiming"] = monday.titleLabel?.text ?? kBlankString
			createListingRequest["TuesdayTiming"] = tuesday.titleLabel?.text ?? kBlankString
			createListingRequest["WednesdayTiming"] = wednesday.titleLabel?.text ?? kBlankString
			createListingRequest["ThursdayTiming"] = thrusday.titleLabel?.text ?? kBlankString
			createListingRequest["FridayTiming"] = friday.titleLabel?.text ?? kBlankString
			createListingRequest["SaturdayTiming"] = saturday.titleLabel?.text ?? kBlankString
			createListingRequest["SundayTiming"] = sunday.titleLabel?.text ?? kBlankString
			
			//section 3
			createListingRequest["AdditionalRules"] = txtPolicies.text ?? kBlankString
			if btnOwnerPresenseNo.isSelected
			{
				createListingRequest["OwnerPresence"] = 0
			}else if btnOwnerPresenseYes.isSelected
			{
				createListingRequest["OwnerPresence"] = 1
			}else{
				createListingRequest["OwnerPresence"] = 2
			}
			
			if btnDriveNo.isSelected
			{
				createListingRequest["UserDrive"] = 0
			}else{
				createListingRequest["UserDrive"] = 1
			}
			
			if flexibleRadio.isSelected
			{
				createListingRequest["Cancellation"] = 0
			}else if strictRadio.isSelected{
				createListingRequest["Cancellation"] = 1
			}else{
				createListingRequest["Cancellation"] = 2
			}
			
			var acts = [String]()
			if videoCheck.isChecked
			{
				acts.append("Video Shoot")
			}
			if movieCheck.isChecked
			{
				acts.append("Movie Scene")
			}
			if commercialCheck.isChecked
			{
				acts.append("Commercials")
			}
			if filmCheck.isChecked
			{
				acts.append("Film Shoot")
			}
			if photoCheck.isChecked
			{
				acts.append("Photo Shoot")
			}
			
			createListingRequest["CarHostActivity"] = acts.joined(separator: ",")
			
			//section 4
			
			
			var jsonData = createListingRequest
			jsonData["HourlyRate"] = hourlyRate.text!
			jsonData["MinHours"] = minHours.text!.components(separatedBy: " ")[0]
			
			jsonData["DiscountTime"] = discount.text!.components(separatedBy: " ")[0]
			jsonData["DiscountPercentage"] = discountPercentage.text!
			
			createListingRequest["MinHours"] = minHours.text!.components(separatedBy: " ")[0]
			createListingRequest["DiscountTime"] = discount.text!.components(separatedBy: " ")[0]
			createListingRequest["HourlyRate"] = hourlyRate.text!
			
			jsonData["ServiceTypeId"] = 1
			jsonData["AppUserId"] = 1003
			
			jsonData["MaxHours"] = 24
			
			var fees = [[String:Any]]()
			
			if NSString(string: deliveryFee.text!).integerValue != 0{
				let mi = Utilities.getMilesList()[milesDD.indexForSelectedRow!].components(separatedBy: " ")[0]
				let fee1 = ["ShooteServiceListId" : 1003, "Type" : "Delivery", "DurationOrDistance" : mi, "Amount" : deliveryFee.text!] as [String : Any]
				
				fees.append(fee1)
			}
			
			if NSString(string: cleaning.text!).integerValue != 0
			{
				let fee2 = ["ShooteServiceListId" : 1003, "Type" : "Cleaning Fee", "Amount" : cleaning.text!] as [String : Any]
				fees.append(fee2)
			}
			
			if NSString(string: late.text!).integerValue != 0
			{
				let fee3 = ["ShooteServiceListId" : 1003, "Type" : "Late Penalty Fee", "Amount" : late.text!] as [String : Any]
				fees.append(fee3)
				
			}
			
			if NSString(string: smoke.text!).integerValue != 0
			{
				let fee4 = ["ShooteServiceListId" : 1003, "Type" : "Smoking Fee", "Amount" : smoke.text!] as [String : Any]
				fees.append(fee4)
				
			}
			
			if NSString(string: paint.text!).integerValue != 0
			{
				let fee5 = ["ShooteServiceListId" : 1003, "Type" : "Paint Damage", "Amount" : paint.text!] as [String : Any]
				fees.append(fee5)
				
			}
			
			if NSString(string: damage.text!).integerValue != 0
			{
				let fee6 = ["ShooteServiceListId" : 1003, "Type" : "Interior Tear Damage", "Amount" : damage.text!] as [String : Any]
				fees.append(fee6)
				
			}
			
			jsonData["AdditionalFees"] = fees
			var json : Data!
			do{
				json = try JSONSerialization.data(withJSONObject: jsonData, options: .prettyPrinted)
			}catch{
				showAlertView(message: "Error parsing request values: \(error.localizedDescription)")
				return
			}
			
			startLoading()
			API.shared.updateListing(jsonData: json, images: createListingImages) { (msg, status) in
				self.stopLoading()
				if status
				{
					self.showAlertView(message: msg, title: ALERT_TITLE_APP_NAME, doneButtonTitle: "OK") { (ac) in
						self.navigationController?.popViewController(animated: true)
					}
				}else{
					self.showAlertView(message: msg)
				}
			}
		}
	}
	
	
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		if textField == year || textField == maxCapacity || textField == hourlyRate || textField == discountPercentage || textField == deliveryFee || textField == cleaning || textField == late || textField == paint || textField == smoke || textField == damage{
			guard CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string)) else {
				return false
			}
		}
		return true
	}
	
	
	func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
		if textField == condition
		{
			toggleConditionListDD()
			return false
		}
		else if textField == exteriorColor
		{
			toggleExteriorColorListDD()
			return false
		}
		else if textField == interiorColor
		{
			toggleInteriorColorListDD()
			return false
		}
		else if textField == typeOfCar
		{
			toggleTypeOfCarListDD()
			return false
		}
		else if textField == transmission
		{
			toggleTransmissionListDD()
			return false
		}
		else if textField == bodyStyle
		{
			toggleBodyStyleListDD()
			return false
		} else if textField == miles
		{
			toggleMilesDD()
			return false
		}
		else if textField == discount
		{
			toggleDiscountDD()
			return false
		}
		else if textField == minHours
		{
			toggleHoursDD()
			return false
		}
		return true
	}
	@IBAction func actionBack(_ sender: UIButton) {
		self.navigationController?.popViewController(animated: true)
	}

}

extension EditListingViewController
{
	func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
		picker.dismiss(animated: true, completion: nil)
	}
	
	func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
		let chosenImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
		createListingImages.append(chosenImage)
		picker.dismiss(animated: true, completion: nil)
	}
}
