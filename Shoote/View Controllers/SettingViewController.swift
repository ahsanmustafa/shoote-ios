//
//  SettingViewController.swift
//  Shoote
//
//  Created by Faheem on 23/07/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit

class SettingViewController: BaseViewController {

	//MARK: Outlets
    @IBOutlet weak var imgHome : UIImageView!
	@IBOutlet weak var viewCohost : UIView!
	@IBOutlet weak var lblRenterMenu : UILabel!
	@IBOutlet weak var lblRenterMenuHeight : NSLayoutConstraint!
	@IBOutlet weak var lblRenterMenuTopConstraint : NSLayoutConstraint!
	@IBOutlet weak var viewRenterMenu : UIView!
	@IBOutlet weak var viewRenterMenuHeight : NSLayoutConstraint!
	@IBOutlet weak var viewRenterMenuTopConstraint : NSLayoutConstraint!
    
	//MARK: Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.backToHome(_:)))
        imgHome.isUserInteractionEnabled = true
        imgHome.addGestureRecognizer(tap)
		if Global.shared.user.userRole != "Admin" {
			viewCohost.isHidden = true
			lblRenterMenu.isHidden = true
			lblRenterMenuHeight.constant = 0
			lblRenterMenuTopConstraint.constant = 0
			viewRenterMenu.isHidden = true
			viewRenterMenuHeight.constant = 0
			viewRenterMenuTopConstraint.constant = 0
		}
    }
    
	//MARK: Actions
    @objc
	func backToHome(_ sender : UITapGestureRecognizer){
        self.navigationController?.popViewController(animated: true)
    }
	
	@IBAction
	func actionViewCohost(_ sender : UIButton){
		let vc = UIViewController.instantiate(ViewCoHostViewController.self, fromStoryboard: .Setting)
		self.navigationController?.pushViewController(vc, animated: true)
	}
	
    @IBAction
	func actionCreateList(_ sender : UIButton){
		let vc = UIViewController.instantiate(ListServicesViewController.self, fromStoryboard: .CreateService)
        self.navigationController?.pushViewController(vc, animated: true)
    }
	
    @IBAction
	func actionMessages(_ sender : UIButton){
		let vc = UIViewController.instantiate(InboxViewController.self, fromStoryboard: .Main)
        self.navigationController?.pushViewController(vc, animated: true)
    }
	
    @IBAction
	func actionLogout(_ sender : UIButton){
        UserDefaultsManager.shared.clearUserData()
        UserDefaultsManager.shared.isUserLoggedIn = false
        UserDefaults.standard.synchronize()
		let vc = UIViewController.instantiate(LoginViewController.self, fromStoryboard: .Registration)
        self.navigationController?.setViewControllers([vc], animated: true)
    }
	
    @IBAction
	func actionBookingCalendar(_ sender : UIButton){
		let vc = UIViewController.instantiate(RentalAndBookingCalendarViewController.self, fromStoryboard: .CreateService)
		vc.type = "Booking"
		self.navigationController?.pushViewController(vc, animated: true)
    }
	
    @IBAction
	func actionCurrentBookings(_ sender : UIButton){
        let vc = UIViewController.instantiate(ViewBookingsAndRentalsViewController.self, fromStoryboard: .CreateService)
        vc.selectedType = .currentBookings
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction
	func actionIncomingBookings(_ sender : UIButton){
        let vc = UIViewController.instantiate(ViewBookingsAndRentalsViewController.self, fromStoryboard: .CreateService)
        vc.selectedType = .incommingBookings
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction
	func actionPastBooking(_ sender : UIButton){
        let vc = UIViewController.instantiate(ViewBookingsAndRentalsViewController.self, fromStoryboard: .CreateService)
        vc.selectedType = .pastBookings
        self.navigationController?.pushViewController(vc, animated: true)
    }
	
    @IBAction
	func actionRentalcalendar(_ sender : UIButton){
		let vc = UIViewController.instantiate(RentalAndBookingCalendarViewController.self, fromStoryboard: .CreateService)
		vc.type = "Rental"
        self.navigationController?.pushViewController(vc, animated: true)
    }
	
    @IBAction
	func actionCurrentRentals(_ sender : UIButton){
        let vc = UIViewController.instantiate(ViewBookingsAndRentalsViewController.self, fromStoryboard: .CreateService)
        vc.selectedType = .currentRentals
        self.navigationController?.pushViewController(vc, animated: true)
    }
	
    @IBAction
	func actionIncomingsRentals(_ sender : UIButton){
        let vc = UIViewController.instantiate(ViewBookingsAndRentalsViewController.self, fromStoryboard: .CreateService)
        vc.selectedType = .incomingRentals
        self.navigationController?.pushViewController(vc, animated: true)
    }
	
    @IBAction
	func actionPastRentals(_ sender : UIButton){
        let vc = UIViewController.instantiate(ViewBookingsAndRentalsViewController.self, fromStoryboard: .CreateService)
        vc.selectedType = .pastRentals
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction
	func backBarBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
	
	@IBAction
	func actionRequestSent(_ sender: Any) {
		let vc = UIViewController.instantiate(RequestSentViewController.self, fromStoryboard: .CarDetails)
		 self.navigationController?.pushViewController(vc, animated: true)
	}
	
	@IBAction
	func actionRequestReceived(_ sender: Any) {
		let vc = UIViewController.instantiate(RequestReceivedViewController.self, fromStoryboard: .CarDetails)
		 self.navigationController?.pushViewController(vc, animated: true)
	}
	
	@IBAction
	func actionViewMyListing(_ sender: Any) {
		let vc = UIViewController.instantiate(ViewMyListingViewController.self, fromStoryboard: .CarDetails)
		 self.navigationController?.pushViewController(vc, animated: true)
	}
    
    @IBAction
    func actionPaymentsPayouts(_ sender: Any) {
        let vc = UIViewController.instantiate(PaymentsPayoutsViewController.self, fromStoryboard: .Setting)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
