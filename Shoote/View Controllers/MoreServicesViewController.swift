//
//  MoreServicesViewController.swift
//  Shoote
//
//  Created by Faheem on 19/07/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit

class MoreServicesViewController: BaseViewController {

	//MARK: Override Functions
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    

	//MARK: Actions
    @IBAction
	func actionBack(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }

}
