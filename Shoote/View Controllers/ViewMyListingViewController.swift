//
//  ViewMyListingViewController.swift
//  Shoote
//
//  Created by BrainX IOS 3 on 27/12/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit

class ViewMyListingViewController: BaseViewController {

	//MARK: Properties
	var dataArr = [CarListing]()
	var dataArrForCurrent = [CarListing]()
	var currentActiveListing = 2
	
	//MARK: Outlets
	@IBOutlet weak var btnDeleted : UIButton!
	@IBOutlet weak var btnActive : UIButton!
	@IBOutlet weak var btnDisabled : UIButton!
	@IBOutlet weak var tableView : UITableView!
	
	//MARK: Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
    }
	
	override func viewWillAppear(_ animated: Bool) {
		getListingsByUser()
	}
	
	//MARK: Private Methods
	private func resetAllButtion(){
		btnDeleted.backgroundColor = .clear
		btnActive.backgroundColor = .clear
		btnDisabled.backgroundColor = .clear
	}
	
	//MARK: Actions Methods
	@IBAction
	func actionBack(_ sender: UIButton) {
		self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction
	func actionDeleteListing(_ sender: UIButton) {
		let id = dataArrForCurrent[sender.tag].id
		deleteListing(with: id)
	}
	
	@IBAction
	func actionEditListing(_ sender: UIButton) {
		
		let vc = UIViewController.instantiate(EditListingViewController.self, fromStoryboard: .CarDetails)
		vc.data = dataArrForCurrent[sender.tag]
		self.navigationController?.pushViewController(vc, animated: true)
	}
	
	@IBAction
	func actionChangeStaus(_ sender: UIButton) {
		
		let id = dataArrForCurrent[sender.tag].id
		let status = dataArrForCurrent[sender.tag].isActive
		ChangeStatus(with: id, and: status)
	}
	
	@IBAction
	func actionViewChangeListing(_ sender: UIButton) {
		currentActiveListing = sender.tag
		switch sender.tag {
		case 1:
			//Deleted
			self.dataArrForCurrent = self.dataArr.filter({$0.isDeleted})
		case 2:
			//Active
			self.dataArrForCurrent = self.dataArr.filter({$0.isActive && !$0.isDeleted})
		case 3:
			//Disabled
			self.dataArrForCurrent = self.dataArr.filter({!$0.isActive && !$0.isDeleted})
		default:
			break
		}
		self.tableView.reloadData()
		resetAllButtion()
		sender.backgroundColor = UIColor(named: "yellowNewColor")
	}
}

//MARK: UITableViewDelegate and UITableViewDataSource Methods
extension ViewMyListingViewController : UITableViewDelegate, UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return dataArrForCurrent.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "ViewMyListTableViewCell") as! ViewMyListTableViewCell
		cell.setData(data: dataArrForCurrent[indexPath.row], status: currentActiveListing)
		cell.btnDeleted.tag = indexPath.row
		cell.btnEdit.tag = indexPath.row
		cell.btnStatus.tag = indexPath.row
		cell.btnDeleted.addTarget(self, action: #selector(self.actionDeleteListing(_:)), for: .touchUpInside)
		cell.btnStatus.isEnabled = currentActiveListing == 1 ? false : true
		cell.btnStatus.addTarget(self, action: #selector(self.actionChangeStaus(_:)), for: .touchUpInside)
		cell.btnEdit.addTarget(self, action: #selector(self.actionEditListing(_:)), for: .touchUpInside)
		return cell
	}
	
}

//MARK: Api Call
extension ViewMyListingViewController {
	
	private func getListingsByUser() {
		self.startLoading()
		API.shared.getListingsByUser() { (status, message, list) in
			DispatchQueue.main.async
			{
				self.stopLoading()
				if let info = list {
					self.dataArr = info.data
					self.dataArrForCurrent = self.dataArr.filter({$0.isActive && !$0.isDeleted})
					self.tableView.reloadData()
				} else {
					self.showAlertView(message: message, title: ALERT_TITLE_APP_NAME)
				}
			}
		}
	}
	
	private func deleteListing(with id: Int) {
		
		self.startLoading()
		API.shared.deleteCarListing(listId: id) { (stats, message) in
			self.stopLoading()
			if stats {
				self.showAlertView(message: message, title: ALERT_TITLE_APP_NAME, doneButtonTitle: "OK") { (alert) in
					self.navigationController?.popViewController(animated: true)
				}
			} else {
				self.showAlertView(message: message, title: ALERT_TITLE_APP_NAME)
			}
		}
	}
	
	private func ChangeStatus(with id: Int, and status: Bool) {
		
		self.startLoading()
		API.shared.activeOrInActiveListing(id: id, status: !status) { (status, message) in
			self.stopLoading()
			if status {
				self.showAlertView(message: message, title: ALERT_TITLE_APP_NAME, doneButtonTitle: "OK") { (alert) in
					self.navigationController?.popViewController(animated: true)
				}
			} else {
				self.showAlertView(message: message, title: ALERT_TITLE_APP_NAME)
			}
		}
		
	}
}
