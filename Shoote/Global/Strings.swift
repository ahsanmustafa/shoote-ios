
import Foundation
import UIKit


typealias ParamsAny             = [String:Any]
typealias ParamsDic             = [String:Data]
typealias ParamsString          = [String:String]

let ALERT_TITLE_APP_NAME        = "Shoote"
let INTERNET_UNAVAILABLE       = "09801010"
let INVALID_CODE             = "406"
let EMAIL_REGULAR_EXPRESSION    = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
let ERROR_NO_NETWORK 			= "Connection lost. Please check your internet connection and try again."
let Flexible		 			= "Cancel within 24 hours time period to get 100% money back, 0% will be deducted from your transaction and the remaining amount will be added to your bank account within 3–5 Working Days."
let Strict						= "Cancel within 1 week time period to get 50% money back, 50% will be deducted from your transaction and the remaining amount will be added to your bank account within 3–5 Working Days."
let VeryStrict					= "Cancel within 2 week time period to get 25% money back, 75% will be deducted from your transaction and the remaining amount will be added to your bank account within 3–5 Working Days."

let welcomeMessage				= "Hi %@,\n\n\nWelcome to Shoote!\nA confirmation link ha  been sent to your email address once confirmed your account will be active.\n\nThanks,\n  Shoote"

//Default values for data types
let kBlankString = ""

let kInt0 = 0
let kIntDefault = -1

let kDouble0 = 0.0
let kFloat0 = Float(0)
let kDoubleDefault = -1.0

struct APIKeys {
    static let GoogleMapsKey = "AIzaSyAqzB_Wrg6M9LcF6SgojF8ASMnTHZjc4sk"
}

struct NotificationName {
    static let UnAuthorizedAccess    = Notification.Name(rawValue: "UnAuthorizedAccess")
}
struct ServiceMessage {
    static let LocationTitle       = "Location Service Off"
    static let LocationMessage     = "Turn on Location in Settings > Privacy to allow myLUMS to determine your Location"
    static let Settings            = "Settings"
    static let CameraTitle         = "Permission Denied"
    static let CameraMessage       = "Turn on Camera in Settings"
}

struct ControllerIdentifier {
    static let LoginViewController          = "LoginViewController"
    static let SignUpConfirmationViewController = "SignUpConfirmationViewController"
    static let HomeViewController = "HomeViewController"
    static let LoadingViewController = "LoadingViewController"
    static let ForgotPasswordVC = "forgotPasswordID"
    
}

struct ValidationMessages {
    static let loginSuccessfully            = "You are logged in"
    static let selectProfileimage           = "Select Profile Image"
    static let emptyAddress                    = "Please enter your address"
    static let emptyName               = "Please enter your name"
    static let emptyFirstName               = "Please enter your first name"
    static let emptyLastName                = "Please enter your last name"
    static let emptyCity                    = "Please enter your city name"
    static let emptyEmail                   = "Please enter your email"
    static let enterValidEmail              = "Please enter valid email"
    static let emptyPassword                = "Password field cannot be empty"
    static let shortPassword                = "Password must be atleast 6 characters"
    static let reTypePassword               = "Please re-type password"
    static let nonMatchingPassword          = "Password is not matching"
    static let invalidPhoneNumber           = "Enter a valid phone number"
    static let configurationUrl             = "Please enter configuration url"
    static let validUrl                     = "Please enter valid url"
    static let emptyPhonNumber              = "Please enter phone number"
    static let emptyPincode                 = "Please enter verification code"
    static let emptyCategoryName            = "Please enter category name first"
    static let emptyProductName             = "Please enter product name first"
    static let invalidProductPrice          = "Please enter a valid price for product"
    static let emptyProductInfo             = "Please describe product briefly"
    static let noImageProduct               = "Add at least one image of product"
    static let selectWeightUnit             = "Select product weight unit first"
    static let commentsMissing              = "Comment field cannot empty"
    static let noLocationAdded              = "Location info is must in order to become a supplier"
    static let AcceptTerms                 = "Please agree with terms and conditions"
}
struct CellIdentifier {
    static let CarsCollectionViewCell             = "CarsCollectionViewCell"
}

struct PopupMessages {
    static let warning                      = "Warning"
    static let signUpSuccess                      = "Registered Successfully"
    static let sureToLogout                 = "Are you sure to logout"
    static let nothingToUpdate              = "Nothing to update"
    static let orderMarkedCompleted         = "Order marked completed successfullly"
    static let unAuthorizedAccessMessage    = "Session expired, please login again"
    static let cameraPermissionNeeded       = "Camera permission needed to scan QR Code. Goto settings to enable camera permission"
    static let SomethingWentWrong           = "Something went wrong"
    static let internetFailure = "Failure due to intenet connection, please check your internet connection or try again later!"
    static let EmailExists = "Email Already Registered"
	static let SelectDateRange = "Please Select Date Range"
}
