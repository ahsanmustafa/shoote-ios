import Foundation
import UIKit
import StoreKit

class Global {
    class var shared : Global {
        
        struct Static {
            static let instance : Global = Global()
        }
        return Static.instance
    }
    var user = LoginUser()
    var isLogedIn:Bool = false
    var fcmToken:String = "some dummy text for now"
    var systemVersion = UIDevice.current.systemVersion
    var deviceModel = UIDevice.modelName
    
  }

