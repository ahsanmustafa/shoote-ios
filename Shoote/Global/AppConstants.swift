import Foundation
import UIKit
struct ScreenSize {
    static let SCREEN_WIDTH = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType {
    static let IS_IPHONE_4_OR_LESS =  UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    
    static let IS_IPHONE_5 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPHONE_X_All = (UIDevice.current.userInterfaceIdiom == .phone && (ScreenSize.SCREEN_MAX_LENGTH == 812 || ScreenSize.SCREEN_MAX_LENGTH == 896))
    static let IS_IPHONE_X = (UIDevice.current.userInterfaceIdiom == .phone && (ScreenSize.SCREEN_MAX_LENGTH == 812))
    static let IS_IPHONE_X_MAX = (UIDevice.current.userInterfaceIdiom == .phone && (ScreenSize.SCREEN_MAX_LENGTH == 896))
    static let IS_IPAD = UIDevice.current.userInterfaceIdiom == .pad
}

struct AppFonts {
	static let PoppinsMedium           = UIFont(name: "Poppins-Medium", size: 15.0)!
}

struct EndPoints {
    static let BASE_URL                         = "https://shoote23api.azurewebsites.net/"
    static let BASE_URL_IMG                     = "https://shoote23api.azurewebsites.net"
    static let Register                         = "api/v1/Signup"
    static let Login                            = "Login"
	static let ForgotPassword					= "api/v1/ForgotPassword"
	static let UpdateCarListing					= "api/v1/UpdateCarListing"
	static let CreateCarListing					= "api/v1/CreateCarListing"
	static let GetCarListings					= "api/v1/GetCarListings"
	static let GetCarListingByBusinessProfile				= "api/v1/GetCarListingByBusinessProfile"
	static let GetCarListingsByFilter				= "api/v1/GetCarListingsByFilter"
	static let ActiveOrInActiveCarListing			= "api/v1/ActiveOrInActiveCarListing"
	static let ApproveCarBookingRequest		= "api/v1/ApproveCarBookingRequest"
	static let GetAppUserById					= "api/v1/GetAppUserById?appUserId="
	static let GetAppUserProfileById			= "api/v1/GetAppUserProfileById?appUserId="
	static let GetBusinessProfileById			= "api/v1/GetBusinessProfileById"
	static let GetShooteUserById			= "api/v1/GetShooteUserById"
	static let GetCarListingAvailability		= "api/v1/GetCarListingAvailability"
	static let CreateCarBooking					= "api/v1/CreateCarBooking"
	static let GetCarRequestsSentByRenterId		= "api/v1/GetCarRequestsSentByRenterId"
	static let GetCarRequestsReceivedByBusinessProfileId		= "api/v1/GetCarRequestsReceivedByBusinessProfileId"
	static let DeleteCarListing				= "api/v1/DeleteCarListing"
	static let GetIncomingCarBookingsByBookingUser = "api/v1/GetIncomingCarBookingsByBookingUser"
	static let GetPastCarBookingsByBookingUserId	= "api/v1/GetPastCarBookingsByBookingUserId"
	static let GetCurrentCarRidesByRenter			= "api/v1/GetCurrentCarRidesByRenter"
	static let StartCarRiding						= "api/v1/StartCarRiding"
	static let AddReview						= "api/v1/AddReview"
	static let GetIncomingCarRentalsByOwnerId		= "api/v1/GetIncomingCarRentalsByOwnerId"
	static let GetCurrentCarRidesByOwner			= "api/v1/GetCurrentCarRidesByOwner"
	static let GetPastCarRentalsByOwnerId			= "api/v1/GetPastCarRentalsByOwnerId"
	static let GetCarBookingsForCalendarByShooteUser	= "api/v1/GetCarBookingsForCalendarByShooteUser"
	static let GetCarRentalsForCalendarByBusinessProfile 	= "api/v1/GetCarRentalsForCalendarByBusinessProfile"
	static let GetConversations 				= "api/v1/getConversations"
	static let GetTwillioToken					= "api/v1/getTwillioToken"
    static let GetUserProfile                   = "api/v1/UserProfile"
    static let UpdateUserProfile                = "api/v1/UpdateUserProfile"
	static let OpenConversation					= "api/v1/openConversation"
	static let GetShooteUsersByBusinessProfileId = "api/v1/GetShooteUsersByBusinessProfileId"
	static let AddShooteUsersByBusinessProfileId = "api/v1/AddShooteUsersByBusinessProfileId"
    static let CreatePaymentSheet = "api/v1/CreatePaymentSheet"
    static let GetPayoutBalance = "api/v1/GetAccountBalance"
    static let ProcessPayout = "api/v1/ProcessPayout"
    static let GetStripePayoutStatus = "api/v1/GetStripePayoutStatus"
    
}

