import Foundation
import UIKit
private var maxLengths = [UITextField: Int]()

class Utilities {
    
   static func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    static func unArchiveObject(key: String) -> AnyObject? {
        guard let data = UserDefaults.standard.object(forKey: key) as? Data else {
            return nil
        }
        return NSKeyedUnarchiver.unarchiveObject(with: data) as AnyObject
    }

    static func isValidEmail(_ testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
	
	static func getHoursList() -> [String] {
		var arr = [String]()
		for i in (1...24) {
			arr.append("\(i) hr")
		}
		return arr
	}
	
	static func getDiscountList() -> [String] {
		var arr = [String]()
		for i in (1...24) {
			arr.append("\(i) hr")
		}
		return arr
	}
	
	static func getMilesList() -> [String] {
		var arr = [String]()
		for i in (1...5) {
			arr.append("\(i) mile")
		}
		return arr
	}
    
	static func getTransmissionList() -> [String] {
		let arr2: [String] = ["Manual","Automatic","CVT","Semi-automatic"]
		return arr2
	}
	
	static func getConditionList() -> [String] {
		let arr: [String] = ["Excellent","Good","Fair","Poor"]
		return arr
	}
	
	static func getStartTimingList() -> [String] {
		let arr2: [String] = ["00:00 AM", "01:00 AM", "02:00 AM", "03:00 AM", "04:00 AM", "05:00 AM", "06:00 AM", "07:00 AM", "08:00 AM", "09:00 AM", "10:00 AM", "11:00 AM", "12:00 PM", "01:00 PM", "02:00 PM", "03:00 PM", "04:00 PM", "05:00 PM", "06:00 PM", "07:00 PM", "08:00 PM", "09:00 PM", "10:00 PM", "11:00 PM" ]
		return arr2
	}
	
	static func getRaduisMilesList() -> [String]{
		let arr: [String] = ["1–5 Miles","1–10 Miles","1–15 Miles","1–20 Miles","1–25 Miles","1–30 Miles","1–35 Miles","1–40 Miles", "1–45 Miles", "1–50 Miles"]
		return arr
	}
	
	static func getEndTimingList() -> [String] {
		let arr2: [String] = ["00:00 AM", "01:00 AM", "02:00 AM", "03:00 AM", "04:00 AM", "05:00 AM", "06:00 AM", "07:00 AM", "08:00 AM", "09:00 AM", "10:00 AM", "11:00 AM", "12:00 PM", "01:00 PM", "02:00 PM", "03:00 PM", "04:00 PM", "05:00 PM", "06:00 PM", "07:00 PM", "08:00 PM", "09:00 PM", "10:00 PM", "11:00 PM" ]
		return arr2
	}
	
	static func getTypeOfBookingList() -> [String] {
		let arr4: [String] = ["Video Shoots", "Film Shoots", "Photo Shoots", "Commercials", "Movie Scenes"]
		return arr4
	}
	
	static func getBodyStyleList() -> [String] {
		let arr2: [String] = ["Coupe", "Sedan", "Suv", "Pick Up Truck", "Cargo Van", "BoxTruck", "Other"]
		return arr2
	}
	
	static func getTypeOfCarList() -> [String] {
		let arr3: [String] = ["Economic", "Premium", "Luxury", "Exotic", "Super Car", "Classic", "Other"]
		return arr3
	}
    
    static func isDateGreater(serverDate: Date, appDate: Date)-> Bool {
        
        let compare = appDate.compare(serverDate)
        
        if compare == .orderedAscending {
            return true
        }else{
            return false
        }
    }
    static func getDayOfWeek(_ today:String) -> String {
        let formatter  = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"
        formatter.timeZone = TimeZone.current
        let todayDate = formatter.date(from: today) ?? Date()
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: todayDate)
        switch weekDay {
        case 1:
            return "SUN"
        case 2:
            return "MON"
        case 3:
            return "TUE"
        case 4:
            return "WED"
        case 5:
            return "THU"
        case 6:
            return "FRI"
        case 7:
            return "SAT"
        default:
            return ""
        }
    }
    
	static func twillioDateToTime(date: Date?) -> String {
		let dateforamtter  = DateFormatter()
		dateforamtter.dateFormat = "hh:mm a"
		return dateforamtter.string(from: date ?? Date())
	}
	
	static func utcToLocal(dateString: String) -> String {
		let dateforamtter  = DateFormatter()
		if let timezone = TimeZone(abbreviation: "UTC") {
			dateforamtter.timeZone = timezone
		}
		dateforamtter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
		
		let dateInUTC = dateforamtter.date(from: dateString) ?? Date()
		dateforamtter.timeZone = TimeZone.current
		dateforamtter.dateFormat = "MMM dd, yyyy hh:mm a"
		
		return dateforamtter.string(from: dateInUTC)
	}
	
	static func utcToLocal(withoutSSS dateString: String) -> String {
		let dateforamtter  = DateFormatter()
		if let timezone = TimeZone(abbreviation: "UTC") {
			dateforamtter.timeZone = timezone
		}
		dateforamtter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
		
		let dateInUTC = dateforamtter.date(from: dateString) ?? Date()
		dateforamtter.timeZone = TimeZone.current
		dateforamtter.dateFormat = "MMM dd, yyyy hh:mm a"
		
		return dateforamtter.string(from: dateInUTC)
	}

	static func localToUTCDateString(with date: Date) -> String {
		let dateFormatter = DateFormatter()
		if let timezone = TimeZone(abbreviation: "UTC") {
			dateFormatter.timeZone = timezone
		}
		dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss a"
		
		return dateFormatter.string(from: date)
	}
    

    static func isCurrentDate(strDate:String) -> Bool{
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        let temp = formatter.date(from: strDate)
        let currentString = formatter.string(from: Date())
        let currentDate = formatter.date(from: currentString)
        if(currentDate == temp){
            return true
        }
        return false
    }
	
	static func getColorList() -> [String] {
		let arr: [String] = ["White", "Silver", "Black", "Yellow", "Grey", "Blue", "Red", "Brown", "Green", "Purple", "Teal", "Bronze", "Orange", "Pink", "Gold", "Other"]
		return arr
	}
	
}


extension UserDefaults {
    
    func set<T: Encodable>(codable: T, forKey key: String) {
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(codable)
            let jsonString = String(data: data, encoding: .utf8)!
            print("Saving \"\(key)\": \(jsonString)")
            self.set(jsonString, forKey: key)
        } catch {
            print("Saving \"\(key)\" failed: \(error)")
        }
    }
    
    func codable<T: Decodable>(_ codable: T.Type, forKey key: String) -> T? {
        guard let jsonString = self.string(forKey: key) else { return nil }
        guard let data = jsonString.data(using: .utf8) else { return nil }
        let decoder = JSONDecoder()
        print("Loading \"\(key)\": \(jsonString)")
        return try? decoder.decode(codable, from: data)
    }
}

