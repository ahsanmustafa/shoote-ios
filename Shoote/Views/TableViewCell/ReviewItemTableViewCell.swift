//
//  ReviewItemTableViewCell.swift
//  Shoote
//
//  Created by BrainX IOS 3 on 20/04/2021.
//  Copyright © 2021 Digi Dev. All rights reserved.
//

import UIKit
import Cosmos

class ReviewItemTableViewCell: BaseTableViewCell {

	//MARK: Outlets
	@IBOutlet weak var lblDesc : UILabel!
	@IBOutlet weak var viewRating : CosmosView!
	@IBOutlet weak var imgUser : UIImageView!
	
	//MARK: Helper Methods
	func setData(_ data: Review) {
		lblDesc.text = data.reviewDescription
		viewRating.rating = data.rating.toDouble()
		let url = EndPoints.BASE_URL_IMG + data.reviewerImage
		setImageWithUrl(imageView: imgUser, url: url,placeholder: "user-large")
		
	}

}
