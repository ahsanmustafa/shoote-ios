//
//  RentalCalendarTableViewCell.swift
//  Shoote
//
//  Created by BrainX IOS 3 on 16/09/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit

class RentalCalendarTableViewCell: BaseTableViewCell {

	//MARK: Outlets
	@IBOutlet weak var lblCarName : UILabel!
	@IBOutlet weak var lblStartTime : UILabel!
	@IBOutlet weak var lblEndTime : UILabel!
	@IBOutlet weak var lblLocation : UILabel!
	@IBOutlet weak var lblTotalPayput : UILabel!
    @IBOutlet weak var imgCar : UIImageView!
    @IBOutlet weak var btnStatus : UIButton!

	//MARK: Helper Methods
	func setData(_ data: CarBooking) {
		let path = EndPoints.BASE_URL_IMG + (data.carListing.listingImages.first?.imagePath ?? "")
		setImageWithUrl(imageView: imgCar, url: path,placeholder: "img_car_placeholder")
		lblCarName.text = data.carListing.name
		lblStartTime.text = data.startDateUserTimeZome
		lblEndTime.text = data.endDateUserTimeZone
		lblTotalPayput.text = "$\(data.totalFee.withCommas())"
		
		lblLocation.text = data.location.address
		
		switch data.bookingStatus {
		case "Current":
			btnStatus.setTitle("Active", for: .normal)
			btnStatus.backgroundColor = UIColor(hexFromString: "E39B00")
		case "Incoming":
			btnStatus.setTitle("Incoming", for: .normal)
			btnStatus.backgroundColor = UIColor(hexFromString: "8219EB")
		case "Past":
			btnStatus.setTitle("Complete", for: .normal)
			btnStatus.backgroundColor = UIColor(hexFromString: "1DBB17")
		default:
			break
		}
	}

}
