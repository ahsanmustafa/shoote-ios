//
//  InboxTableViewCell.swift
//  Shoote
//
//  Created by Faheem on 14/08/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit

class InboxTableViewCell: BaseTableViewCell {

	//MARK: Outlets
	@IBOutlet weak var lblName: UILabel!
	@IBOutlet weak var imgUser: UIImageView!
	@IBOutlet weak var imgArrow: UIImageView!
	
	//MARK: Helper Method
	func setData(_ conversation: Conversation) {
		if !(conversation.businessProfileId == Global.shared.user.businessProfileId)  {
			if !conversation.businessProfile.title.isEmpty{
				lblName.text = conversation.businessProfile.title
			} else {
				lblName.text = "Test User: \(conversation.businessProfileId)"
			}
			self.setImageWithUrl(imageView: imgUser, url: EndPoints.BASE_URL_IMG + conversation.businessProfile.profileImage, placeholder: "user")
		} else {
			if !conversation.shooteUser.firstName.isEmpty{
				lblName.text = conversation.shooteUser.fullName
			} else {
				lblName.text = "Test User: \(conversation.shooteUserId)"
			}
			self.setImageWithUrl(imageView: imgUser, url: EndPoints.BASE_URL_IMG + conversation.shooteUser.profileImage, placeholder: "user")
		}
	}
}
