//
//  ViewMyListTableViewCell.swift
//  Shoote
//
//  Created by BrainX IOS 3 on 27/12/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit

class ViewMyListTableViewCell: BaseTableViewCell {

	//MARK: Outlets
	@IBOutlet weak var lblCarName : UILabel!
	@IBOutlet weak var lblInactiveDate : UILabel!
	@IBOutlet weak var lblCeateddate : UILabel!
	@IBOutlet weak var lblAddress : UILabel!
	@IBOutlet weak var imgArrow : UIImageView!
	@IBOutlet weak var imgCar : UIImageView!
	@IBOutlet weak var btnDeleted : UIButton!
	@IBOutlet weak var btnEdit : UIButton!
	@IBOutlet weak var btnStatus : UIButton!
	
	//MARK: Helper Methods
	func setData(data : CarListing, status : Int) {
		
		lblCarName.text = data.name
		lblCeateddate.text = data.createdAt.components(separatedBy: "T")[0]
		lblAddress.text = "\(data.location.city), \(data.location.state), \(data.location.zip)"
		if let short = data.listingImages.first?.imagePath {
			let path = EndPoints.BASE_URL_IMG + short
			setImageWithUrl(imageView: imgCar, url: path,placeholder: "img_car_placeholder")
		}
		let inactiveDate = data.inActiveDate.components(separatedBy: "T")[0] 
		switch status {
		case 1:
		//Deleted
			btnStatus.backgroundColor = UIColor(hexFromString: "FF1D1D")
			btnStatus.setTitle("Deleted", for: .normal)
			lblInactiveDate.isHidden = true
			btnDeleted.isHidden = true
			btnEdit.isHidden = true
		case 2:
		//Active
			btnStatus.backgroundColor = UIColor(hexFromString: "00C127")
			btnStatus.setTitle("Active", for: .normal)
			lblInactiveDate.isHidden = true
			btnDeleted.isHidden = false
			btnEdit.isHidden = false
		case 3:
		//Disabled
			btnStatus.backgroundColor = UIColor(hexFromString: "E39b00")
			btnStatus.setTitle("Inactive", for: .normal)
			lblInactiveDate.isHidden = false
			lblInactiveDate.text = "Inactive Date: \(inactiveDate)"
			btnDeleted.isHidden = false
			btnEdit.isHidden = false
			
		default:
			break
		}
	}

}
