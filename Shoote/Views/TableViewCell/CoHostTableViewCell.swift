//
//  CoHostTableViewCell.swift
//  Shoote
//
//  Created by BrainX IOS 3 on 05/09/2021.
//  Copyright © 2021 Digi Dev. All rights reserved.
//

import UIKit

class CoHostTableViewCell: BaseTableViewCell {

	//MARK: Outlets
	@IBOutlet weak var lblCount: UILabel!
	@IBOutlet weak var lblName: UILabel!
	@IBOutlet weak var lblConfirmationStatus: UILabel!
	@IBOutlet weak var lblType: UILabel!
	
	//MARK: Helper Methods
	func setData(user: ShooteUser, index: Int) {
		lblCount.text = (index + 1).toString()
		lblName.text = user.fullName
		if user.isActive {
			lblConfirmationStatus.text = "Confirmed"
		} else {
			lblConfirmationStatus.text = "Pending"
		}
		if user.role == 1 {
			lblType.text = "Admin"
		} else {
			lblType.text = "Co-Host"
		}
		
	}

}
