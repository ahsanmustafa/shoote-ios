//
//  RequestSentTableViewCell.swift
//  Shoote
//
//  Created by BrainX IOS 3 on 20/12/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit
import Cosmos

class RequestSentTableViewCell: BaseTableViewCell {

	//MARK: Outlets
	@IBOutlet weak var lblOwnerName : UILabel!
	@IBOutlet weak var ratingOwner : CosmosView!
	@IBOutlet weak var imgOwner : UIImageView!
	@IBOutlet weak var lblCarName : UILabel!
	@IBOutlet weak var imgCar : UIImageView!
	@IBOutlet weak var lblStartTime : UILabel!
	@IBOutlet weak var lblEndTime : UILabel!
	@IBOutlet weak var lblBookingPayment : UILabel!
	@IBOutlet weak var lblBookingType : UILabel!
	@IBOutlet weak var lblLocation : UILabel!
	
	//MARK: Helper Methods
	func setData(_ data : CarBooking) {
		getUserData(id: data.ownerId )
		lblCarName.text = data.carListing.name
		let path = EndPoints.BASE_URL_IMG + (data.carListing.listingImages.first?.imagePath ?? kBlankString)
		setImageWithUrl(imageView: imgCar, url: path,placeholder: "img_car_placeholder")
		lblStartTime.text = data.startDateUserTimeZome
		lblEndTime.text = data.endDateUserTimeZone
        lblBookingPayment.text = "$\(data.totalFee.withCommas())"
		lblBookingType.text = data.typeOfBooking
		lblLocation.text = data.location.city + ", " + data.location.state + ", " + data.location.zip
	}
	
	//MARK: Private Methods
	private func getUserData(id : Int) {
		API.shared.getBusinessProfileById(id: id) { (status, msg, dt) in
			if let dt = dt {
				GCD.async(.Main) {
					self.lblOwnerName.text = dt.title
					let url = EndPoints.BASE_URL_IMG + dt.profileImage
					self.setImageWithUrl(imageView: self.imgOwner, url: url,placeholder: "img_car_placeholder")
				}
			}
		}
	}

}
