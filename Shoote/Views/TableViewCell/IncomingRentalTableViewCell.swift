//
//  RentalTableViewCell.swift
//  Shoote
//
//  Created by BrainX IOS 3 on 15/09/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit
import Cosmos
import MagicTimer

class IncomingRentalTableViewCell: BaseTableViewCell {

	//MARK: Outlets
    //Host views
    @IBOutlet weak var imgHost : UIImageView!
    @IBOutlet weak var lblHostName : UILabel!
    @IBOutlet weak var viewHostRating : CosmosView!
    @IBOutlet weak var lblLocation : UILabel!
    @IBOutlet weak var imgHostCar : UIImageView!
    //Renter views
    @IBOutlet weak var imgRenter : UIImageView!
    @IBOutlet weak var lblRenterName : UILabel!
    @IBOutlet weak var viewRenterRating : CosmosView!
    
    @IBOutlet weak var lblStartTime : UILabel!
    @IBOutlet weak var lblEndTime : UILabel!
    @IBOutlet weak var lblDuration : UILabel!
    @IBOutlet weak var btnBookingStatus : UIButton!
    @IBOutlet weak var lblBookingPayment : UILabel!
    @IBOutlet weak var lblBookingType : UILabel!
    
    @IBOutlet weak var btnViewBooking : UIButton!
    @IBOutlet weak var btnViewBookingSingle : UIButton!
    @IBOutlet weak var btnStartBooking : UIButton!
    @IBOutlet weak var timer : MagicTimerView!
    
	//MARK: Helper Methods
    func setData(_ data : CarBooking, type: ViewBookingsAndRentals) {
        getHotsData(id: data.ownerId )
        getRenterData(id: data.bookingUserId)
        let path = EndPoints.BASE_URL_IMG + (data.carListing.listingImages.first?.imagePath ?? kBlankString)
        setImageWithUrl(imageView: imgHostCar, url: path,placeholder: "img_car_placeholder")
        lblStartTime.text = data.startDateUserTimeZome
        lblEndTime.text = data.endDateUserTimeZone
        
        lblBookingPayment.text = "$\(data.totalFee.withCommas())"
        lblBookingType.text = data.typeOfBooking
		lblLocation.text = data.carListing.location.city
        //lblBookingId.text = "Conf # \(data.id)"
        switch type {
        case .incomingRentals:
            btnBookingStatus.setTitle("Pending", for: .normal)
            btnBookingStatus.backgroundColor = UIColor(hexFromString: "8219EB")
            
            btnStartBooking.isHidden = true
            btnViewBooking.isHidden = true
            btnViewBookingSingle.isHidden = false
            
        case .currentRentals:
            btnBookingStatus.setTitle("Active", for: .normal)
            btnBookingStatus.backgroundColor = UIColor(hexFromString: "E39B00")
            btnStartBooking.isHidden = true
            btnViewBookingSingle.isHidden = true
            timer.isHidden = false
            setUpTimer(with: data.currentTimeSpan)
        case .pastRentals:
            // Check whether expired or completed
            if data.rideDateTime.isEmpty {
                btnBookingStatus.setTitle("Expired", for: .normal)
                btnBookingStatus.backgroundColor = UIColor(hexFromString: "FF1D1D")
            } else {
                btnBookingStatus.setTitle("Complete", for: .normal)
                btnBookingStatus.backgroundColor = UIColor(hexFromString: "1DBB17")
            }
            btnStartBooking.setTitle("Review Renter", for: .normal)
            btnStartBooking.backgroundColor = UIColor(hexFromString: "E39B00")
            btnStartBooking.isEnabled = true
            btnStartBooking.isHidden = false
            btnViewBooking.isHidden = false
            btnViewBookingSingle.isHidden = true
        default:
            break
        }
		let dateforamtter  = DateFormatter()
		dateforamtter.dateFormat = "MMM dd, yyyy hh:mm a"
		let startDate = dateforamtter.date(from: data.startDateUserTimeZome) ?? Date()
		let endDate = dateforamtter.date(from: data.endDateUserTimeZone) ?? Date()
        let diffComponents = Calendar.current.dateComponents([.hour, .minute], from: startDate, to: endDate)
        var hours = diffComponents.hour ?? 0
        let minutes = diffComponents.minute ?? 0
        if minutes > 0 {
            hours += 1
        }
        lblDuration.text = "\(hours) hours"
        
        //check review info
        // If No Reviews Submitted
        if data.reviews.count > 0 {
            var flag = false
            for review in data.reviews {
				if review.givenTo == .USER {
                    flag = true
                    break
                }
                
            }
            
            if flag {
                btnStartBooking.setTitle("Review Submitted", for: .normal)
                btnStartBooking.backgroundColor = UIColor(hexFromString: "1DBB17")
                btnStartBooking.isEnabled = false
            }
        }
    }
    
	//MARK: Private Methods
    private func getHotsData(id : Int){
        
        API.shared.getBusinessProfileById(id: id) { (status, msg, dt) in
            if let dt = dt {
                GCD.async(.Main) {
                    self.lblHostName.text = dt.title
					self.viewHostRating.rating = dt.ratingAvg.toDouble()
					let url = EndPoints.BASE_URL_IMG + dt.profileImage
					self.setImageWithUrl(imageView: self.imgHost, url: url,placeholder: "img_car_placeholder")
                }
            }
        }
    }
    
    private func getRenterData(id : Int){
        
        API.shared.getBusinessProfileById(id: id) { (status, msg, dt) in
            if let dt = dt {
                GCD.async(.Main) {
                    self.lblRenterName.text = dt.title
					self.viewRenterRating.rating = dt.ratingAvg.toDouble()
					let url = EndPoints.BASE_URL_IMG + dt.profileImage
					self.setImageWithUrl(imageView: self.imgRenter, url: url,placeholder: "img_car_placeholder")
                }
            }
        }
    }
    
    private func setUpTimer(with timeLeft: CurrentTimeSpan) {
        let timeInMili = (timeLeft.days * 24 * 60 * 60 ) + (timeLeft.hours * 60 * 60 ) + (timeLeft.minutes * 60) + (timeLeft.seconds)
        timer.isActiveInBackground = false
        timer.font = UIFont(name: "Poppins-Medium", size: 14.0)
        timer.formatter = CustomTimeFormatter()
        timer.mode = .countDown(fromSeconds: TimeInterval(timeInMili))
        timer.effectiveValue = 1
        timer.timeInterval = 1
        timer.startCounting()
    }

}
