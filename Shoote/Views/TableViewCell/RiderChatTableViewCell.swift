//
//  RiderChatTableViewCell.swift
//  Shoote
//
//  Created by Faheem on 15/08/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit

class SendChatTableViewCell: BaseTableViewCell {

	//MARK: Outlets
    @IBOutlet weak var textTimestamp: UILabel!
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var textMsg: UILabel!

}
