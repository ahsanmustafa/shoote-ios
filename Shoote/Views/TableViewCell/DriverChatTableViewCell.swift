//
//  DriverChatTableViewCell.swift
//  Shoote
//
//  Created by Faheem on 15/08/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit

class DriverChatTableViewCell: BaseTableViewCell {

	//MARK: Outlets
    @IBOutlet weak var textTimestamp: UILabel!
    @IBOutlet weak var textMsg: UILabel!
    @IBOutlet weak var viewBack: UIView!

}
