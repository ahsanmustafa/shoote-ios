//
//  NumberOfPersonsCollectionViewCell.swift
//  Shoote
//
//  Created by BrainX IOS 3 on 15/12/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit

class NumberOfPersonsCollectionViewCell: BaseCollectionViewCell {
 
	//MARK: Outlets
	@IBOutlet weak var lblCount : UILabel!
	
}
