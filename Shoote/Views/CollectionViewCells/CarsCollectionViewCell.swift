//
//  CarsCollectionViewCell.swift
//  Shoote
//
//  Created by Faheem on 13/07/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit
import Cosmos
import SDWebImage

class CarsCollectionViewCell: BaseCollectionViewCell {
    
	//MARK: Outlets
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var name: UILabel!
	@IBOutlet weak var lblCityState: UILabel!
    @IBOutlet weak var amount: UILabel!
	@IBOutlet weak var ratingView: CosmosView!
    
	//MARK: Helper Methods
	func setData(_ carInfo : CarListing) {
		name.text = carInfo.name
		amount.text = "$"+"\((carInfo.hourlyRate ).toInt())"+"/hr"
		lblCityState.text = (carInfo.location.city ) + ", " + (carInfo.location.state )
		ratingView.rating = Double(carInfo.averageRating)
		
		if let sd = carInfo.listingImages.first?.imagePath {
			let path = EndPoints.BASE_URL_IMG + sd
			setImageWithUrl(imageView: img, url: path, placeholder: "img_car_placeholder")
		} else {
			img.image = UIImage(named: "img_car_placeholder")
		}
	}
}
