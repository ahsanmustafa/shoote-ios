//
//  ListServicesCollectionViewCell.swift
//  Shoote
//
//  Created by Faheem on 18/07/2020.
//  Copyright © 2020 Digi Dev. All rights reserved.
//

import UIKit

class ListServicesCollectionViewCell: BaseCollectionViewCell {
	
	//MARK: Outlets
    @IBOutlet weak var viewMain : UIView!
    @IBOutlet weak var name: UILabel!
	
}
