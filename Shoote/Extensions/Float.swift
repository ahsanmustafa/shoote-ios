//
//  Float.swift
//  Shoote
//
//  Created by BrainX IOS 3 on 25/04/2021.
//  Copyright © 2021 Digi Dev. All rights reserved.
//

import Foundation

extension Float {
	func toString() -> String{
		return String(self)
	}
	
	func toDouble() -> Double{
		return Double(self)
	}
	
	func toInt() -> Int{
		return Int(self)
	}
	
	func withCommas() -> String {
		let numberFormatter = NumberFormatter()
		numberFormatter.numberStyle = .decimal
		numberFormatter.minimumFractionDigits = 2
		return numberFormatter.string(from: NSNumber(value:self))!
	}
}
