//
//  UINavigationController.swift
//  Shoote
//
//  Created by BrainX IOS 3 on 25/04/2021.
//  Copyright © 2021 Digi Dev. All rights reserved.
//

import Foundation

extension UINavigationController {
	
	open override var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	}
}
