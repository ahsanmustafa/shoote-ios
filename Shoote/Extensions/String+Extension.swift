//
//  String+Extension.swift
//  Student Diary
//
//  Created by Faheem on 22/04/2020.
//  Copyright © 2020 Rapidzz Solutions. All rights reserved.
//

import UIKit
extension String {
	
    func trim() -> String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    var doubleValue: Double {
        return (self as NSString).doubleValue
    }
    
    func size(OfFont font: UIFont) -> CGSize {
        return (self as NSString).size(withAttributes: [.font:font])
    }
    
    func boldString(fontSize : CGFloat ,font : UIFont?) -> NSMutableAttributedString {
        let attrs = [NSAttributedString.Key.font : font ?? UIFont.systemFont(ofSize: 8)]
        return NSMutableAttributedString(string:self, attributes:attrs)
    }
}
