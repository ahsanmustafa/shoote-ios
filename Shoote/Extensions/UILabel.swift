//
//  UILabel.swift
//  Shoote
//
//  Created by BrainX IOS 3 on 25/04/2021.
//  Copyright © 2021 Digi Dev. All rights reserved.
//

import Foundation

extension UILabel {
	
	func colorString(text: String, coloredText: String, color: UIColor) {
		
		let attributedString = NSMutableAttributedString(string: text)
		let range = (text as NSString).range(of: coloredText)
		attributedString.setAttributes([NSAttributedString.Key.foregroundColor: color],
									   range: range)
		self.attributedText = attributedString
	}
}
