//
//  UIColor.swift
//  Shoote
//
//  Created by BrainX IOS 3 on 25/04/2021.
//  Copyright © 2021 Digi Dev. All rights reserved.
//

import Foundation

extension UIColor {
	
	convenience init(rgbValues red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat = 1) {
		self.init(red: red / 255, green: green / 255, blue: blue / 255, alpha: alpha)
	}
	
	convenience init(hexFromString value: String, alpha: CGFloat = 1) {
		var hexValue = value.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
		
		if hexValue.hasPrefix("#") {
			hexValue.remove(at: hexValue.startIndex)
		}
		
		if hexValue.hasPrefix("0x".uppercased()) {
			hexValue.removeSubrange(hexValue.range(of: "0x".uppercased())!)
		}
		
		if hexValue.count != 6 {
			self.init(white: 0.5, alpha: 0.8)
			return
		}
		
		var rgbValue: UInt32 = 0
		Scanner(string: hexValue).scanHexInt32(&rgbValue)
		
		self.init(hexFromInt: rgbValue, alpha: alpha)
	}
	
	convenience init(hexFromInt value: UInt32, alpha: CGFloat = 1) {
		self.init(
			red: CGFloat((value & 0xFF0000) >> 16) / 255,
			green: CGFloat((value & 0x00FF00) >> 8) / 255,
			blue: CGFloat(value & 0x0000FF) / 255,
			alpha: alpha)
	}
}
