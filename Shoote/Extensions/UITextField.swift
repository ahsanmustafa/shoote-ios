//
//  UITextField.swift
//  Shoote
//
//  Created by BrainX IOS 3 on 25/04/2021.
//  Copyright © 2021 Digi Dev. All rights reserved.
//

import Foundation

extension UITextField {
	
	@IBInspectable var textInsets: CGPoint {
		get {
			return CGPoint.zero
		}
		set {
			layer.sublayerTransform = CATransform3DMakeTranslation(newValue.x, newValue.y, 0);
		}
	}
	
	@IBInspectable var placeholderColor: UIColor {
		get {
			guard let currentAttributedPlaceholderColor = attributedPlaceholder?.attribute(NSAttributedString.Key.foregroundColor, at: 0, effectiveRange: nil) as? UIColor else { return UIColor.clear }
			return currentAttributedPlaceholderColor
		}
		set {
			guard let currentAttributedString = attributedPlaceholder else { return }
			let attributes = [NSAttributedString.Key.foregroundColor : newValue]

			attributedPlaceholder = NSAttributedString(string: currentAttributedString.string, attributes: attributes)
		}
	}
}

//MARK: Calendar with UITextField

extension UITextField {

	func setInputViewDatePicker(target: Any, selector: Selector) {
		// Create a UIDatePicker object and assign to inputView
		let screenWidth = UIScreen.main.bounds.width
		let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))//1
		datePicker.datePickerMode = .date //2
		if #available(iOS 13.4, *) {
			datePicker.preferredDatePickerStyle = .wheels
		}
		self.inputView = datePicker //3

		// Create a toolbar and assign it to inputAccessoryView
		let toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: screenWidth, height: 44.0)) //4
		let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil) //5
		let cancel = UIBarButtonItem(title: "Cancel", style: .plain, target: nil, action: #selector(tapCancel)) // 6
		let barButton = UIBarButtonItem(title: "Done", style: .plain, target: target, action: selector) //7
		toolBar.setItems([cancel, flexible, barButton], animated: false) //8
		self.inputAccessoryView = toolBar //9

	}

	@objc func tapCancel() {
		self.resignFirstResponder()
	}

}
