//
//  UIStoryboard.swift
//  Point Work
//
//  Created by Faheem on 07/11/2020.
//  Copyright © 2020 Crymzee Networks. All rights reserved.
//

import UIKit

extension UIStoryboard {
    
    enum Name: String {
        case Main
        case Registration
        case CreateService
		case Setting
		case BecomeHostSecond
		case BecomeHostThree
		case BecomeHostFour
		case BecomeHostFive
		case CarDetails
    }
}
