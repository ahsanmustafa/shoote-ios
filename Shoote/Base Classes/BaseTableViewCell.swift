//
//  BaseTableViewCell.swift
//  AlKadi
//
//  Created by Khurram Bilal Nawaz on 22/07/2016.
//  Copyright © 2016 Khurram Bilal Nawaz. All rights reserved.
//

import UIKit
import SDWebImage

public class BaseTableViewCell : UITableViewCell {
	
    //MARK: SetImageWithUrl
    func setImageWithUrl(imageView:UIImageView,url:String, placeholder:String? = ""){
		let finalUrl = url.replacingOccurrences(of: "\\", with: "/").replacingOccurrences(of: " ", with: "%20")
        if let imageurl =  NSURL.init(string: finalUrl){
            imageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
            imageView.sd_setImage(with: imageurl as URL?, placeholderImage: UIImage(named:placeholder!))
        }
    }
}
