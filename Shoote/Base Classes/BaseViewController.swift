//
//  BaseViewController.swift
//  AlKadi
//
//  Created by Khurram Bilal Nawaz on 22/07/2016.
//  Copyright © 2016 Khurram Bilal Nawaz. All rights reserved.
//

import UIKit
import SDWebImage
import MBProgressHUD
import StoreKit
import AVFoundation
import Lottie


public class BaseViewController : UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
	
	//MARK: Properties
    let animationView = AnimationView()
    lazy var profileImagePicker: UIImagePickerController = {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        return imagePicker
    }()
    
	//MARK: Override Methods
    override public func viewDidLoad() {
        super.viewDidLoad()
    }
	
	public override var preferredStatusBarStyle : UIStatusBarStyle {
		return .lightContent //.default for black style
	}
    
	//MARK: Check Internet
    internal func checkInternetConnection() -> Bool {
        if(BReachability.isConnectedToNetwork()){
            return true
        }else{
            self.showAlertVIew(message:ERROR_NO_NETWORK, title: ALERT_TITLE_APP_NAME)
            return false
        }
    }
    
	//MARK: Open Photos
	internal func fetchProfileImage(_ message:String = kBlankString){
       let actionSheet = UIAlertController.init(title: "Select Image", message: message, preferredStyle: UIAlertController.Style.actionSheet)
        actionSheet.addAction(UIAlertAction.init(title: "Camera", style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
            if(UIImagePickerController.isSourceTypeAvailable(.camera)){
                self.profileImagePicker.sourceType = .camera
                self.present(self.profileImagePicker, animated: true, completion:  nil)
            }else{
                print("Camera is not available")
            }
        }))
        actionSheet.addAction(UIAlertAction.init(title: "Photo Gallary", style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
            self.profileImagePicker.sourceType = .photoLibrary
            self.present(self.profileImagePicker, animated: true, completion: nil)
        }))
        actionSheet.addAction(UIAlertAction.init(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { (UIAlertAction) in
        }))
        self.present(actionSheet, animated: true, completion: nil)
    }
	
    public func checkCameraPermission() -> Bool {
        if AVCaptureDevice.authorizationStatus(for: AVMediaType.video) == .authorized {
            return true
        }else {
            return false
        }
    }
    
	//MARK: Loader Methods
	internal func stopLoading() {
        self.animationView.removeFromSuperview()
    }
    
	internal func startLoading() {
        let animation = Animation.named("anim_loading2", subdirectory: nil)
        animationView.animation = animation
        animationView.contentMode = .scaleAspectFit
        view.addSubview(animationView)
        animationView.backgroundBehavior = .pauseAndRestore
        animationView.translatesAutoresizingMaskIntoConstraints = false
        animationView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        animationView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        
        animationView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        animationView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        animationView.setContentCompressionResistancePriority(.fittingSizeLevel, for: .horizontal)
        let redValueProvider = ColorValueProvider(Color(r: 1, g: 0.2, b: 0.3, a: 1))
        animationView.setValueProvider(redValueProvider, keypath: AnimationKeypath(keypath: "Switch Outline Outlines.**.Fill 1.Color"))
        animationView.setValueProvider(redValueProvider, keypath: AnimationKeypath(keypath: "Checkmark Outlines 2.**.Stroke 1.Color"))
        animationView.play(fromProgress: 0,
                           toProgress: 1,
                           loopMode: LottieLoopMode.loop,
                           completion: { (finished) in
                            if finished {
                              print("Animation Complete")
                            } else {
                              print("Animation cancelled")
                            }
        })
        self.view.bringSubviewToFront(self.animationView)
        
    }
    
 
  
    //MARK: SetImageWithUrl
	internal func setImageWithUrl(imageView:UIImageView,url:String, placeholderImage:String? = ""){
		let finalUrl = url.replacingOccurrences(of: "\\", with: "/").replacingOccurrences(of: " ", with: "%20")
        if let imageurl =  NSURL.init(string: finalUrl){
            imageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
            imageView.sd_setImage(with: imageurl as URL?, placeholderImage: UIImage(named:placeholderImage!))
        }
    }
    
	//MARK: Show Alert Methods
	internal func showAlertVIew(message:String, title:String) {
		let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
		let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
			
		}
		alertController.addAction(okAction)
		self.present(alertController, animated: true, completion: nil)
		
	}
   
	internal func showAlertView(message:String) {
        showAlertView(message: message, title: ALERT_TITLE_APP_NAME)
    }
    
	internal func showAlertView(message:String, title:String, doneButtonTitle:String, doneButtonCompletion: ((UIAlertAction) -> Void)?) {
        showAlertView(message: message, title: title, doneButtonTitle: doneButtonTitle, doneButtonCompletion: doneButtonCompletion, cancelButtonTitle: nil, cancelButtonCompletion: nil)
    }
    
	internal func showAlertView(message:String, title:String, doneButtonTitle:String = "OK", doneButtonCompletion: ((UIAlertAction) -> Void)? = nil, cancelButtonTitle:String? = nil, cancelButtonCompletion:((UIAlertAction) -> Void)? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: doneButtonTitle, style: .default, handler: doneButtonCompletion)
        if let cancelTitle = cancelButtonTitle {
            let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel, handler: cancelButtonCompletion)
            alertController.addAction(cancelAction)
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
	internal func buildAlertSheet(with title:String? = nil, message:String? = nil, options:[String], completion: @escaping (Int,String)->Void) {
        let controller = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        
        for index in 0..<options.count {
            let action = UIAlertAction(title: options[index], style: .default) { (_) in
                completion(index,options[index])
            }
            
            controller.addAction(action)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        controller.addAction(cancelAction)
        self.present(controller, animated: true, completion: nil)
    }

}
